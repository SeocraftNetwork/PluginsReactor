package net.seocraft.api.bukkit.util;

import net.seocraft.api.bukkit.BukkitAPI;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

public class RedisConnection {

    public static boolean addString(String key, String value) {
        Jedis jedis = BukkitAPI.pool.getResource();
        try {
            jedis.set(key, value);
            return jedis.exists(key);
        } catch(JedisException e) {
            if (null != jedis) {
                BukkitAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BukkitAPI.pool.returnResource(jedis);
        }
        return false;
    }

    public static boolean hasString(String key) {
        Jedis jedis = BukkitAPI.pool.getResource();
        try {
            return jedis.exists(key);
        } catch(JedisException e) {
            if (null != jedis) {
                BukkitAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BukkitAPI.pool.returnResource(jedis);
        }
        return false;
    }

    public static String getRedisString(String key) {
        Jedis jedis = BukkitAPI.pool.getResource();
        try {
            if(!jedis.exists(key)) {
                return "false";
            } else {
                return jedis.get(key);
            }
        } catch (JedisException ex) {
            ex.printStackTrace();
            if (null != jedis) {
                BukkitAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BukkitAPI.pool.returnResource(jedis);
        }
        return null;
    }

    public static String deleteString(String key) {
        Jedis jedis = BukkitAPI.pool.getResource();
        try {
            if(!jedis.exists(key)) {
                return "false";
            } else {
                jedis.del(key);
                return "true";
            }
        } catch(JedisException e) {
            if (null != jedis) {
                BukkitAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BukkitAPI.pool.returnResource(jedis);
        }
        return "error";
    }
}