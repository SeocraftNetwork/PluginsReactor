package net.seocraft.api.bukkit.requests.disguises;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.bukkit.models.Disguise;
import net.seocraft.api.shared.models.Group;
import org.bukkit.Bukkit;

public class DisguiseJoin {

    private static String realm = BukkitAPI.getInstance().getConfig().getString("realm");

    public static Disguise request(String player, String user_token) {
        JsonObject json = new JsonObject();
        json.addProperty("realm", realm);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/join-disguised", request, BukkitAPI.getInstance().token, user_token);
        Gson gson = new Gson();
        String disguise = JSONDeserializer.deserialize(response).get("disguise").getAsString();
        String disguised_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("group").getAsJsonObject());
        Group disguised_group = gson.fromJson(disguised_group_string, Group.class);
        return new Disguise(Bukkit.getPlayer(player),disguise,disguised_group);
    }
}