package net.seocraft.api.bukkit.requests.user;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class AddGroup {

    public static String[] request(String username, String group, String sender_token) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("group", group.toLowerCase());
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/add-group", request, BukkitAPI.getInstance().token, sender_token);
        String[] update = new String[2];
        String updated = JSONDeserializer.deserialize(response).get("updated").getAsString();
        if (updated.equals("true")) {
            update[0] = "true";
            update[1] = null;
            return update;
        } else {
            update[0] = "false";
            update[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
            return update;
        }
    }
}