package net.seocraft.api.bukkit.requests.user;

import com.google.gson.Gson;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.bukkit.models.AuthenticationUser;

public class LanguageUpdate {

    public static String request(String username, String language) {
        Gson model = new Gson();
        AuthenticationUser user = new AuthenticationUser(username, language);
        String serverRequest = model.toJson(user, AuthenticationUser.class);
        String response = HTTPRequests.post("user/server-update", serverRequest, BukkitAPI.getInstance().token, null);
        return JSONDeserializer.deserialize(response).get("updated").getAsString();
    }
}