package net.seocraft.api.bukkit.requests.disguises;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class DisguiseRemove {

    public static String[] request(String user_token) {
        String response = HTTPRequests.get("user/undisguise-user", BukkitAPI.getInstance().token, user_token);
        String[] removed = new String[2];
        removed[0] = JSONDeserializer.deserialize(response).get("removed").getAsString();
        removed[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
        return removed;
    }
}