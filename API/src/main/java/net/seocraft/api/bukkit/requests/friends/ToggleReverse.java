package net.seocraft.api.bukkit.requests.friends;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class ToggleReverse {

    public static Boolean request(String user_token) {
        String response = HTTPRequests.get("friend/toggle-reverse", BukkitAPI.getInstance().token, user_token);
        if(!JSONDeserializer.deserialize(response).get("query_success").getAsString().equals("true")) {
            return true;
        } else {
            return false;
        }
    }
}