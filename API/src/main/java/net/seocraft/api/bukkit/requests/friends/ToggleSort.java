package net.seocraft.api.bukkit.requests.friends;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class ToggleSort {

    public static Boolean request(String user_token, Integer actual) {
        Integer value = 777;
        if (actual == 3) {
            value = 1;
        } else if (actual == 2) {
            value = 3;
        } else if (actual == 1) {
            value = 2;
        } else {
            value = 1;
        }
        String response = HTTPRequests.get("friend/toggle-sort/" + value, BukkitAPI.getInstance().token, user_token);
        if(!JSONDeserializer.deserialize(response).get("query_success").getAsString().equals("true")) {
            return false;
        } else {
            return true;
        }
    }
}