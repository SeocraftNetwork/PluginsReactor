package net.seocraft.api.bukkit.requests.server;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.cloud.api.CloudAPI;

public class ConnectRequest {

    public static String request(String type) {
        JsonObject json = new JsonObject();
        String typeSlug;
        json.addProperty("slug", CloudAPI.getServerID());
        json.addProperty("status", "online");
        switch(type) {
            case "game": {
                typeSlug = "game";
                break;
            }
            case "lobby": {
                typeSlug = "lobby";
                break;
            }
            default: {
                typeSlug = "special";
                break;
            }
        }
        json.addProperty("type", typeSlug);
        json.addProperty("cluster", BukkitAPI.getInstance().getConfig().getString("api.cluster"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("servers/load", request, null, null);
    }
}