package net.seocraft.api.bukkit.requests.login;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class LoginRequest {

    public static String[] request(String username, String password) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("password", password);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/login-server", request, BukkitAPI.getInstance().token, null);
        String[] check = new String[3];
        String valid = JSONDeserializer.deserialize(response).get("valid").getAsString();
        if(!valid.equals("true")) {
            check[0] = valid;
            check[1] = null;
            check[2] = null;
            return check;
        } else {
            check[0] = valid;
            check[1] = JSONDeserializer.deserialize(response).get("last_game").getAsString();
            check[2] = JSONDeserializer.deserialize(response).get("token").getAsString();
            return check;
        }
    }
}