package net.seocraft.api.bukkit.packets;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenSignEditor;
import net.seocraft.api.bukkit.BukkitAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class SignGui {

    private Player player;
    private String t1;
    private String t2;
    private String t3;
    private String t4;
    private String accessor;

    public SignGui(Player player, String t1, String t2, String t3, String t4, String accessor) {
        this.player = player;
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
        this.accessor = accessor;
    }

    public Player getPlayer() {
        return player;
    }

    public String getT1() {
        return t1;
    }

    public String getT2() {
        return t2;
    }

    public String getT3() {
        return t3;
    }

    public String getT4() {
        return t4;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setT1(String t1) {
        this.t1 = t1;
    }

    public void setT2(String t2) {
        this.t2 = t2;
    }

    public void setT3(String t3) {
        this.t3 = t3;
    }

    public void setT4(String t4) {
        this.t4 = t4;
    }

    public String getAccessor() {
        return accessor;
    }

    public void open() {
        Block b = player.getLocation().getBlock();
        Material type = b.getType();
        b.setType(Material.SIGN_POST);
        Sign s = (Sign) b.getState();
        s.setLine(0, t1);
        s.setLine(1, t2);
        s.setLine(2, t3);
        s.setLine(3, t4);
        s.update(true);
        BlockPosition bp = new BlockPosition(s.getLocation().getX(), s.getLocation().getY(), s.getLocation().getZ());
        PacketPlayOutOpenSignEditor packet = new PacketPlayOutOpenSignEditor(bp);
        SignGui gui = this;
        Bukkit.getServer().getScheduler().runTaskLater(BukkitAPI.getInstance(), new Runnable() {
            @Override
            public void run() {
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
                SignInputHandler.injectNetty(player, gui);
                b.setType(type);
            }
        }, 2);
    }
}