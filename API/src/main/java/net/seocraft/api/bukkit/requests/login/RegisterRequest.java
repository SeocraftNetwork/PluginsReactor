package net.seocraft.api.bukkit.requests.login;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class RegisterRequest {

    public static String[] request(String username, String password, String ip) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("password", password);
        json.addProperty("ip", ip);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/register-server", request, BukkitAPI.getInstance().token, null);
        String[] check = new String[2];
        String registered = JSONDeserializer.deserialize(response).get("registered").getAsString();
        if (!registered.equals("true")) {
            check[0] = registered;
            check[1] = null;
        } else {
            check[0] = registered;
            check[1] = JSONDeserializer.deserialize(response).get("token").getAsString();
        }
        return check;
    }
}