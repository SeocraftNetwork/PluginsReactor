package net.seocraft.api.bukkit;

import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.bukkit.requests.server.ConnectRequest;
import net.seocraft.api.bukkit.requests.server.DisconnectRequest;
import net.seocraft.api.shared.utils.BungeeMessaging;
import net.seocraft.cloud.api.CloudAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;

public class BukkitAPI extends JavaPlugin {

    private static BukkitAPI instance;
    public static HashMap<Player, BukkitUser> bukkit_users;
    public String id;
    public String token;
    public static JedisPool pool = null;

    @Override
    public void onEnable() {
        bukkit_users = new HashMap<>();
        instance = this;
        loadConfig();
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "InternalAPI", new BungeeMessaging());
        APIConnection();
        pool = new JedisPool(getConfig().getString("redis.ip"), getConfig().getInt("redis.port"));
    }

    public void onDisable() {
        this.APIDisconnect();
    }

    public static BukkitAPI getInstance(){
        return instance;
    }

    private void APIConnection() {
        String request = ConnectRequest.request(getConfig().getString("api.type").toLowerCase());
        if (!JSONDeserializer.deserialize(request).get("authorized").getAsString().equals("true")) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[BukkitAPI] No se ha recibido autorizacion del cluster para activar el servidor");
            Bukkit.shutdown();
        } else {
            String slug = CloudAPI.getServerID();
            this.id = JSONDeserializer.deserialize(request).get("id").getAsString();
            this.token = JSONDeserializer.deserialize(request).get("token").getAsString();
            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[Bukkit API] El servidor " + slug + " se ha conectado correctamente al cluster. Servidor #" + this.id);
        }
    }

    private void APIDisconnect() {
        DisconnectRequest.request();
        String slug = CloudAPI.getServerID();
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[Bukkit API] El servidor " + slug + "se ha desconectado correctamente del cluster. Servidor #" + this.id);
    }

    public void loadConfig(){
        getConfig().options().copyDefaults(true);
        saveConfig();
    }
}