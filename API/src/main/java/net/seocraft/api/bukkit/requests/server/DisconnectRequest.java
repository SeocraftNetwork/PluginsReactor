package net.seocraft.api.bukkit.requests.server;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;

public class DisconnectRequest {
    public static String request() {
        return HTTPRequests.delete("servers/disconnect" , BukkitAPI.getInstance().token, null);
    }
}