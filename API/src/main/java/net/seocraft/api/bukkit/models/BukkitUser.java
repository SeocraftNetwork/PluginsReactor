package net.seocraft.api.bukkit.models;

import net.seocraft.api.bukkit.util.RedisConnection;
import net.seocraft.api.shared.models.Group;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

public class BukkitUser {

    private String username;
    private String language;
    private String used_ip;
    private Group main_group;
    private List<Group> secondary_groups;
    private String skin;
    private Date member_since;
    private String last_seen;
    private Integer experience;
    private Integer level;
    private Integer max_experience;
    private Boolean disguised;
    private String logged;
    private String token;

    public BukkitUser(String username, String language, String used_ip, Group main_group,
                      @Nullable List<Group> secondary_groups, String skin, Date member_since,
                      String last_seen, Integer experience, Integer level, Integer max_experience,
                      Boolean disguised, String logged) {
        this.username = username;
        this.language = language;
        this.used_ip = used_ip;
        this.main_group = main_group;
        this.secondary_groups = secondary_groups;
        this.skin = skin;
        this.member_since = member_since;
        this.last_seen = last_seen;
        this.experience = experience;
        this.level = level;
        this.max_experience = max_experience;
        this.disguised = disguised;
        this.logged = logged;
    }

    public void startToken() {
        System.out.println(RedisConnection.getRedisString(username + ".minecraft_token"));
        this.token = RedisConnection.getRedisString(username + ".minecraft_token");
        System.out.println(token);
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setLanguague(String language) { this.language = language; }

    public void setUsed_ip(String used_ip) {
        this.used_ip = used_ip;
    }

    public void setMain_group(Group main_group) {
        this.main_group = main_group;
    }

    public void setSecondary_groups(List<Group> secondary_groups) {
        this.secondary_groups = secondary_groups;
    }

    public void setSkin(String skin) { this.skin = skin; }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setMax_experience(Integer max_experience) {
        this.max_experience = max_experience;
    }

    public void setDisguised(Boolean disguised) {
        this.disguised = disguised;
    }

    public String getUsername() {
        return username;
    }

    public String getLanguage() {
        return language;
    }

    public String getUsed_ip() {
        return used_ip;
    }

    public Group getMain_group() {
        return main_group;
    }

    public List<Group> getSecondary_groups() {
        return secondary_groups;
    }

    public String getSkin() { return skin; }

    public Date getMember_since() {
        return member_since;
    }

    public String getLast_seen() {
        return last_seen;
    }

    public Integer getExperience() {
        return experience;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getMax_experience() {
        return max_experience;
    }

    public Boolean getDisguised() {
        return disguised;
    }

    public String getLogged() { return logged; }

    public String getToken() {
        return token;
    }
}