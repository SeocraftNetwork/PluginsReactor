package net.seocraft.api.bukkit.requests.disguises;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class DisguiseNameChecker {

    public static String[] request(String disguise_nick) {
        JsonObject json = new JsonObject();
        json.addProperty("disguise_nick", disguise_nick);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/check-disguise", request, BukkitAPI.getInstance().token, null);
        String[] can_use = new String[2];
        can_use[0] = JSONDeserializer.deserialize(response).get("can_use").getAsString();
        can_use[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
        return can_use;
    }
}