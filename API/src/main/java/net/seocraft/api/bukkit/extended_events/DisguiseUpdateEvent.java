package net.seocraft.api.bukkit.extended_events;

import net.seocraft.api.shared.models.Group;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class DisguiseUpdateEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Group main_group;

    public DisguiseUpdateEvent(Player player, Group main_group) {
        this.player = player;
        this.main_group = main_group;
    }

    public Player getPlayer() {
        return player;
    }

    public Group getMain_group() {
        return main_group;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}