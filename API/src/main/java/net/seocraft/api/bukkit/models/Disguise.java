package net.seocraft.api.bukkit.models;

import net.seocraft.api.shared.models.Group;
import org.bukkit.entity.Player;

public class Disguise {

    private Player player;
    private String disguise;
    private Group group;

    public Disguise(Player player, String disguise, Group group){
        this.player = player;
        this.disguise = disguise;
        this.group = group;
    }

    public Player getPlayer() {
        return player;
    }

    public String getDisguise() {
        return disguise;
    }

    public Group getGroup() {
        return group;
    }

    public void setDisguise(String disguise) {
        this.disguise = disguise;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setPlayer(Player player) { this.player = player; }
}