package net.seocraft.api.bukkit.requests.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
import org.bukkit.Bukkit;

public class GetGroupPlaceholder {

    public static Group request(String name, String realm) {
        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("realm", realm);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("group/get-server", request, BukkitAPI.getInstance().token, null);
        Gson gson = new Gson();
        Group group = gson.fromJson(response, Group.class);
        Bukkit.getConsoleSender().sendMessage(group.getName());
        return gson.fromJson(response, Group.class);
    }
}