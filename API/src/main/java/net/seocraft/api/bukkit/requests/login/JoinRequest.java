package net.seocraft.api.bukkit.requests.login;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class JoinRequest {

    public static String[] request(String user, String ip) {
        JsonObject json = new JsonObject();
        json.addProperty("username", user);
        json.addProperty("ip", ip);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/join-record", request, BukkitAPI.getInstance().token, null);
        String[] logged_user = new String[5];
        String new_user = JSONDeserializer.deserialize(response).get("new_user").getAsString();
        String multi_account = JSONDeserializer.deserialize(response).get("multi_account").getAsString();
        String language = JSONDeserializer.deserialize(response).get("language").getAsString();
        if (multi_account.equals("false")) {
            logged_user[0] = new_user;
            logged_user[1] = multi_account;
            logged_user[2] = null;
            logged_user[3] = null;
            logged_user[4] = language;
            return logged_user;
        } else if (multi_account.equals("true")) {
            logged_user[0] = new_user;
            logged_user[1] = multi_account;
            logged_user[2] = JSONDeserializer.deserialize(response).get("account_details").getAsJsonObject().getAsJsonPrimitive("created_at").getAsString();
            logged_user[3] = JSONDeserializer.deserialize(response).get("account_details").getAsJsonObject().getAsJsonPrimitive("username").getAsString();
            logged_user[4] = language;
            return logged_user;
        }
        return null;
    }
}