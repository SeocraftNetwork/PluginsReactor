package net.seocraft.api.bukkit.extended_events;

import net.seocraft.api.bukkit.packets.SignGui;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerInputEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private SignGui gui;

    public PlayerInputEvent(SignGui gui) {
        this.gui = gui;
    }

    public SignGui getGui() {
        return gui;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}