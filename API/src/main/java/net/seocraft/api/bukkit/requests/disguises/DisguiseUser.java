package net.seocraft.api.bukkit.requests.disguises;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class DisguiseUser {

    public static String[] request(String user_token, String disguise_nick, String disguise_group) {
        JsonObject json = new JsonObject();
        json.addProperty("disguise_nick", disguise_nick);
        json.addProperty("disguise_group", disguise_group);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/disguise-user", request, BukkitAPI.getInstance().token, user_token);
        String[] has_nick = new String[2];
        has_nick[0] = JSONDeserializer.deserialize(response).get("nicked").getAsString();
        has_nick[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
        return has_nick;
    }
}