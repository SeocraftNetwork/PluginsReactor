package net.seocraft.api.bukkit.requests.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

public class JoinData {

    private static String realm = BukkitAPI.getInstance().getConfig().getString("realm");

    public static BukkitUser getPlayerUserObject(String player, String ip) {
        JsonObject json = new JsonObject();
        json.addProperty("username", player);
        json.addProperty("realm", realm);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/user-login", request, BukkitAPI.getInstance().token, null);
        System.out.println(response);
        Gson gson = new Gson();
        String language = JSONDeserializer.deserialize(response).get("language").getAsString();
        String skin = JSONDeserializer.deserialize(response).get("skin").getAsString();
        Date member_since = new java.util.Date(JSONDeserializer.deserialize(response).get("member_since").getAsInt());
        String last_seen = JSONDeserializer.deserialize(response).get("last_seen").getAsString();
        Integer experience = JSONDeserializer.deserialize(response).get("experience").getAsInt();
        Integer level = JSONDeserializer.deserialize(response).get("level").getAsInt();
        Integer max_experience = JSONDeserializer.deserialize(response).get("max_experience").getAsInt();
        String main_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("main_group").getAsJsonObject());
        String secondary_groups_raw = JSONDeserializer.jsonArray(JSONDeserializer.deserialize(response).get("secondary_groups").getAsJsonArray());
        Boolean disguised = JSONDeserializer.deserialize(response).get("disguised").getAsBoolean();
        String logged = JSONDeserializer.deserialize(response).get("logged").getAsString();
        Type listType = new TypeToken<List<Group>>(){}.getType();
        List<Group> secondary_group = gson.fromJson(secondary_groups_raw, listType);
        Group main_group = gson.fromJson(main_group_string, Group.class);
        BukkitUser user = new BukkitUser(player, language, ip,
                main_group, secondary_group, skin, member_since, last_seen, experience, level, max_experience, disguised, logged);
        return user;
    }
}