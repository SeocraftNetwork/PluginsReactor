package net.seocraft.api.bukkit.extended_events;

import net.seocraft.api.shared.models.Group;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;


public class GroupsUpdateEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Group main_group;
    private List<Group> secondary_groups;

    public GroupsUpdateEvent(Player player, Group main_group, List<Group> secondary_groups) {
        this.player = player;
        this.main_group = main_group;
        this.secondary_groups = secondary_groups;
    }

    public Player getPlayer() {
        return player;
    }

    public Group getMain_group() {
        return main_group;
    }

    public List<Group> getSecondary_groups() {
        return secondary_groups;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}