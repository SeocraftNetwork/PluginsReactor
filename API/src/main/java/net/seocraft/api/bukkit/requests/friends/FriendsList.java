package net.seocraft.api.bukkit.requests.friends;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
public class FriendsList {

    public static String request(String user_token, String page, String sort) {
        JsonObject json = new JsonObject();
        json.addProperty("ipp", 27);
        json.addProperty("sorted", Integer.parseInt(sort));
        json.addProperty("realm", BukkitAPI.getInstance().getConfig().getString("realm"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("friend/list/" + page, request, BukkitAPI.getInstance().token, user_token);
    }
}