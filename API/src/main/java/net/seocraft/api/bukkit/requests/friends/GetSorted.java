package net.seocraft.api.bukkit.requests.friends;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class GetSorted {

    public static String[] request(String user_token) {
        String response = HTTPRequests.get("friend/get-sort", BukkitAPI.getInstance().token, user_token);
        String[] sort = new String[2];
        if(!JSONDeserializer.deserialize(response).get("query_success").getAsString().equals("true")) {
            sort[0] = "1";
            sort[1] = "false";
            return sort;
        } else {
            Integer sorted = JSONDeserializer.deserialize(response).get("sorted").getAsInt();
            Boolean reversed = JSONDeserializer.deserialize(response).get("reversed").getAsBoolean();
            sort[0] = sorted.toString();
            sort[1] = reversed.toString();
            return sort;
        }
    }
}