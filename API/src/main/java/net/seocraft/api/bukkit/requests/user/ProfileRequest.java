package net.seocraft.api.bukkit.requests.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;

import java.util.Date;

public class ProfileRequest {

    private static String realm = BukkitAPI.getInstance().getConfig().getString("realm");

    public static String request(String user, @Nullable String target, String sender_token) {
        JsonObject json = new JsonObject();
        json.addProperty("user", user);
        if (target != null) json.addProperty("target", target);
        json.addProperty("realm", realm);
        String request = JSONDeserializer.jsonString(json);
        @Nullable String response = HTTPRequests.post("user/profile-query", request, BukkitAPI.getInstance().token, sender_token);
        if (JSONDeserializer.deserialize(response).get("query_success").getAsString().equals("true") && !response.isEmpty()) {
            return response;
        }
        return null;
    }

    public static String[] queryOptions(String response_raw) {
        String[] status = new String[2];
        status[0] = JSONDeserializer.deserialize(response_raw).get("query_success").getAsString();
        if (status[0].equals("true")) {
            status[1] = JSONDeserializer.deserialize(response_raw).get("queried_disguised").getAsString();
        } else {
            status[1] = "false";
        }
        return status;
    }

    public static BukkitUser encodeUser(String response_raw, String type) {
        Gson gson = new Gson();
        String response = null;
        if (type.equals("own")) {
            response = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response_raw).get("query_user").getAsJsonObject());
        } else if (type.equals("other")) {
            response = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response_raw).get("query_target").getAsJsonObject());
        }
        String username = JSONDeserializer.deserialize(response).get("username").getAsString();
        String language = JSONDeserializer.deserialize(response).get("language").getAsString();
        String skin = JSONDeserializer.deserialize(response).get("skin").getAsString();
        Date member_since = new java.util.Date(JSONDeserializer.deserialize(response).get("member_since").getAsInt());
        String last_seen = JSONDeserializer.deserialize(response).get("last_seen").getAsString();
        Integer experience = JSONDeserializer.deserialize(response).get("experience").getAsInt();
        Integer level = JSONDeserializer.deserialize(response).get("level").getAsInt();
        Integer max_experience = JSONDeserializer.deserialize(response).get("max_experience").getAsInt();
        String main_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("main_group").getAsJsonObject());
        Boolean disguised = JSONDeserializer.deserialize(response).get("disguised").getAsBoolean();
        Group main_group = gson.fromJson(main_group_string, Group.class);
        BukkitUser user = new BukkitUser(username, language, "0.0.0.0",
                main_group, null, skin, member_since, last_seen, experience, level, max_experience, disguised, "false");
        return user;
    }

    public static Group encodeDisguise(String response_raw, String type) {
        if (type.equals("own")) {
            Gson gson = new Gson();
            String response = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response_raw).get("query_user").getAsJsonObject());
            String main_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("disguise_group").getAsJsonObject());
            return gson.fromJson(main_group_string, Group.class);
        } else {
            Gson gson = new Gson();
            String response = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response_raw).get("query_target").getAsJsonObject());
            String main_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("disguise_group").getAsJsonObject());
            return gson.fromJson(main_group_string, Group.class);
        }
    }
}