package net.seocraft.api.bukkit.requests.user;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class RemoveGroup {

    public static String[] request(String username, String group, String sender_token) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("group", group.toLowerCase());
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/remove-group", request, BukkitAPI.getInstance().token, sender_token);
        String[] remove = new String[2];
        String removed = JSONDeserializer.deserialize(response).get("removed").getAsString();
        if (removed.equals("true")) {
            remove[0] = "true";
            remove[1] = null;
            return remove;
        } else {
            remove[0] = "false";
            remove[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
            return remove;
        }
    }
}