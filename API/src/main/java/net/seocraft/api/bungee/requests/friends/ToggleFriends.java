package net.seocraft.api.bungee.requests.friends;

import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class ToggleFriends {

    public static String[] request(String user_token) {
        String response = HTTPRequests.get("friend/toggle-requests", BungeeAPI.getInstance().token, user_token);
        String[] status = new String[2];
        String query_success = JSONDeserializer.deserialize(response).get("query_success").getAsString();
        if (query_success.equals("false")) {
            status[0] = "false";
            status[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
            return status;
        } else {
            status[0] = "true";
            Boolean result = JSONDeserializer.deserialize(response).get("receive_requests").getAsBoolean();
            status[1] = result.toString();
            return status;
        }
    }
}