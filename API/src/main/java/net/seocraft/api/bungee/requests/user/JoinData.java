package net.seocraft.api.bungee.requests.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.models.BungeeUser;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class JoinData {

    private static String realm = BungeeAPI.getConfig().getString("realm");

    public static BungeeUser getPlayerUserObject(String player) {
        JsonObject json = new JsonObject();
        json.addProperty("username", player);
        json.addProperty("realm", realm);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/user-login", request, BungeeAPI.getInstance().token, null);
        Gson gson = new Gson();
        String language = JSONDeserializer.deserialize(response).get("language").getAsString();
        String skin = JSONDeserializer.deserialize(response).get("skin").getAsString();
        String main_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("main_group").getAsJsonObject());
        Boolean disguised = JSONDeserializer.deserialize(response).get("disguised").getAsBoolean();
        Group main_group = gson.fromJson(main_group_string, Group.class);
        BungeeUser user = new BungeeUser(player, language, main_group, skin, disguised);
        return user;
    }
}