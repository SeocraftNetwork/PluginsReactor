package net.seocraft.api.bungee.requests.user;

import com.google.gson.JsonObject;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class PreLogin {
    public static String request(String player) {
        JsonObject json = new JsonObject();
        json.addProperty("username", player);
        json.addProperty("realm",  BungeeAPI.getConfig().getString("realm"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("user/pre-join", request, BungeeAPI.getInstance().token, null);
    }
}