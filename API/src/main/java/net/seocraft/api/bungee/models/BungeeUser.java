package net.seocraft.api.bungee.models;

import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.models.Group;

public class BungeeUser {
    private String username;
    private String language;
    private Group main_group;
    private String skin;
    private Boolean disguised;
    private String token;

    public BungeeUser(String username, String language, Group main_group, String skin, Boolean disguised) {
        this.username = username;
        this.language = language;
        this.main_group = main_group;
        this.skin = skin;
        this.disguised = disguised;
    }

    public void startToken() {
        System.out.println(RedisConnection.getRedisString(username + ".minecraft_token"));
        this.token = RedisConnection.getRedisString(username + ".minecraft_token");
        System.out.println(token);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setLanguague(String language) { this.language = language; }

    public void setMain_group(Group main_group) {
        this.main_group = main_group;
    }

    public void setSkin(String skin) { this.skin = skin; }

    public void setDisguised(Boolean disguised) {
        this.disguised = disguised;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getLanguage() {
        return language;
    }

    public Group getMain_group() {
        return main_group;
    }

    public String getSkin() { return skin; }

    public Boolean getDisguised() {
        return disguised;
    }

    public String getToken() {
        return token;
    }
}