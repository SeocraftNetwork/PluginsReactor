package net.seocraft.api.bungee.requests.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

import java.lang.reflect.Type;
import java.util.List;

public class GetPermissions {

    public static String[] request(String username) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("user/get-permissions", request, BungeeAPI.getInstance().token, null);
        Gson gson = new Gson();
        String permissions = JSONDeserializer.jsonArray(JSONDeserializer.deserialize(response).get("player_permissions").getAsJsonArray());
        Type type = new TypeToken<List<String>>(){}.getType();
        List<String> list = gson.fromJson(permissions, type);
        return list.toArray(new String[list.size()]);
    }
}