package net.seocraft.api.bungee.requests.parties;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;

public class PartyKickOfflineRequest {

    public static String request(String token) {
        return HTTPRequests.get("party/kick-offline", BukkitAPI.getInstance().token, token);
    }
}
