package net.seocraft.api.bungee.requests.server;

import com.google.gson.JsonObject;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.cloud.api.CloudAPI;

public class ConnectRequest {

    public static String request() {
        JsonObject json = new JsonObject();
        json.addProperty("slug", CloudAPI.getServerID());
        json.addProperty("status", "online");
        json.addProperty("type", "BUNGEE");
        json.addProperty("cluster", BungeeAPI.getConfig().getString("api.cluster"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("servers/load", request, null, null);
    }
}