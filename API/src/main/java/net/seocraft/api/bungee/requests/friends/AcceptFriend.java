package net.seocraft.api.bungee.requests.friends;

import com.google.gson.JsonObject;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class AcceptFriend {

    public static String[] request(String user_token, String target) {
        JsonObject json = new JsonObject();
        json.addProperty("target", target);
        json.addProperty("realm", BungeeAPI.getConfig().getString("realm"));
        String request = JSONDeserializer.jsonString(json);
        String response = HTTPRequests.post("friend/accept", request, BungeeAPI.getInstance().token, user_token);
        String[] status = new String[2];
        String query_success = JSONDeserializer.deserialize(response).get("query_success").getAsString();
        if (query_success.equals("false")) {
            status[0] = "false";
            status[1] = JSONDeserializer.deserialize(response).get("message").getAsString();
            return status;
        } else {
            status[0] = "true";
            status[1] = JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("target_data").getAsJsonObject());
            return status;
        }
    }
}