package net.seocraft.api.bungee.requests.parties;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;

public class PartyLeaveRequest {

    public static String request(String token) {
        return HTTPRequests.get("party/leave", BukkitAPI.getInstance().token, token);
    }
}