package net.seocraft.api.bungee.requests.punishments;

import com.google.gson.JsonObject;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class TempPunishmentRequest {

    public static String request(String username, String target, String type, String reason, String server, String last_ip, String expires) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("target", target);
        json.addProperty("type", type);
        json.addProperty("reason", reason);
        json.addProperty("server", server);
        json.addProperty("expires", expires);
        json.addProperty("last_ip", last_ip);
        json.addProperty("realm", BungeeAPI.getConfig().getString("realm"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("punishment/server-create", request, BungeeAPI.getInstance().token, null);
    }
}