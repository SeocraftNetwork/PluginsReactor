package net.seocraft.api.bungee.requests.user;

import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;

public class QuitRequest {

    public static void request(String user_token) {
        try {
            HTTPRequests.get("user/left-record", BungeeAPI.getInstance().token, user_token);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}