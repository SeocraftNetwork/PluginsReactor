package net.seocraft.api.bungee.requests.parties;

import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class PartyPromoteRequest {

    public static String request(String token, String target) {
        JsonObject json = new JsonObject();
        json.addProperty("target", target);
        json.addProperty("realm", BukkitAPI.getInstance().getConfig().getString("realm"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("party/promote", request, BukkitAPI.getInstance().token, token);
    }
}