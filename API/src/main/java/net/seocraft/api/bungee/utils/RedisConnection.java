package net.seocraft.api.bungee.utils;

import net.seocraft.api.bungee.BungeeAPI;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

public class RedisConnection {

    public static boolean addString(String key, String value) {
        Jedis jedis = BungeeAPI.pool.getResource();
        try {
            jedis.set(key, value);
            return jedis.exists(key);
        } catch(JedisException e) {
            if (null != jedis) {
                BungeeAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BungeeAPI.pool.returnResource(jedis);
        }
        return false;
    }

    public static boolean hasString(String key) {
        Jedis jedis = BungeeAPI.pool.getResource();
        try {
            return jedis.exists(key);
        } catch(JedisException e) {
            if (null != jedis) {
                BungeeAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BungeeAPI.pool.returnResource(jedis);
        }
        return false;
    }

    public static String getRedisString(String key) {
        Jedis jedis = BungeeAPI.pool.getResource();
        try {
            if(!jedis.exists(key)) {
                return "false";
            } else {
                return jedis.get(key);
            }
        } catch(JedisException ex) {
            ex.printStackTrace();
            if (null != jedis) {
                BungeeAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BungeeAPI.pool.returnResource(jedis);
        }
        return null;
    }

    public static String deleteString(String key) {
        Jedis jedis = BungeeAPI.pool.getResource();
        try {
            if(!jedis.exists(key)) {
                return "false";
            } else {
                jedis.del(key);
                return "true";
            }
        } catch(JedisException e) {
            if (null != jedis) {
                BungeeAPI.pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (null != jedis)
                BungeeAPI.pool.returnResource(jedis);
        }
        return "error";
    }
}