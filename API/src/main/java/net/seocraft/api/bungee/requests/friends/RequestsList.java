package net.seocraft.api.bungee.requests.friends;

import com.google.gson.JsonObject;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class RequestsList {

    public static String request(String user_token, String page, String sort) {
        JsonObject json = new JsonObject();
        json.addProperty("ipp", 8);
        json.addProperty("sorted", Integer.parseInt(sort));
        json.addProperty("realm", BungeeAPI.getConfig().getString("realm"));
        String request = JSONDeserializer.jsonString(json);
        return HTTPRequests.post("friend/requests/" + page, request, BungeeAPI.getInstance().token, user_token);
    }
}