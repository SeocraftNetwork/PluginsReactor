package net.seocraft.api.bungee.requests.server;

import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;

public class DisconnectRequest {

    public static String request() {
        return HTTPRequests.delete("servers/disconnect", BungeeAPI.getInstance().token, null);
    }
}