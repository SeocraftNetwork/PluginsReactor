package net.seocraft.api.bungee.requests.friends;

import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.shared.commons.HTTPRequests;
import net.seocraft.api.shared.commons.JSONDeserializer;

public class RemoveAllFriends {

    public static String request(String user_token) {
        String response = HTTPRequests.get("friend/remove-all", BungeeAPI.getInstance().token, user_token);
        return JSONDeserializer.deserialize(response).get("query_success").getAsString();
    }
}