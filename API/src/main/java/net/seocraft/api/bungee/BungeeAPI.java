package net.seocraft.api.bungee;

import com.google.api.client.util.ByteStreams;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.seocraft.api.bungee.models.BungeeUser;
import net.seocraft.api.bungee.requests.server.ConnectRequest;
import net.seocraft.api.bungee.requests.server.DisconnectRequest;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.shared.utils.BukkitMessaging;
import net.seocraft.cloud.api.CloudAPI;
import redis.clients.jedis.JedisPool;

import javax.annotation.Nullable;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BungeeAPI extends Plugin {

    private static BungeeAPI instance;
    public static HashMap<ProxiedPlayer, BungeeUser> bungee_users;
    public static List<String> not_redirected;
    private ExecutorService exe;
    private String id;
    public String token;
    private static Configuration configuration;
    public static JedisPool pool = null;

    public static BungeeAPI getInstance() {
        return instance;
    }

    public ExecutorService getExecutor() {
        return exe;
    }

    @Override
    public void onEnable() {
        instance = this;
        getProxy().registerChannel("InternalAPI");
        getProxy().getPluginManager().registerListener(this, new BukkitMessaging());
        exe = Executors.newCachedThreadPool();
        not_redirected = new ArrayList<>();
        bungee_users = new HashMap<>();
        try {
            loadConfig();
            @Nullable String request = ConnectRequest.request();
            if (request == null || !JSONDeserializer.deserialize(request).get("authorized").getAsString().equals("true")) {
                System.out.println("[Bungee API] No se ha recibido autorizacion del cluster para activar el servidor");
                BungeeAPI.getInstance().getProxy().stop();
            } else {
                String slug = CloudAPI.getServerID();
                System.out.println(request);
                this.id = JSONDeserializer.deserialize(request).get("id").getAsString();
                this.token = JSONDeserializer.deserialize(request).get("token").getAsString();
                System.out.println("[Bungee API] El servidor " + slug + " se ha conectado correctamente al cluster. Servidor #" + this.id);
            }
            pool = new JedisPool(getConfig().getString("redis.ip"), getConfig().getInt("redis.port"));
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
    }

    @Override
    public void onDisable() {
        this.APIDisconnect();
    }

    public static Configuration getConfig() {
        return configuration;
    }

    private void APIDisconnect() {
        DisconnectRequest.request();
        String slug = CloudAPI.getServerID();
        System.out.println("[Bungee API] El servidor " + slug + " se ha desconectado correctamente del cluster. Servidor #" + this.id);
    }

    public void loadConfig() throws IOException {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File file = new File(getDataFolder(), "config.yml");
        if (!file.exists()) {
            file.createNewFile();
            try (InputStream in = getResourceAsStream("config.yml");
                 OutputStream out = new FileOutputStream(file)) {
                ByteStreams.copy(in, out);
            }
        }
        configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
    }

    public static void sendToRandomServer(ProxiedPlayer player, String group) {
        List<String> servers = new ArrayList<>();
        for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
            if (server.getName().split("-")[0].equals(group)) {
                servers.add(server.getName());
            }
        }
        Random random = new Random();
        player.connect(ProxyServer.getInstance().getServerInfo(servers.get(random.nextInt(servers.size()))));
    }
}