package net.seocraft.api.shared.utils;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.seocraft.api.bungee.BungeeAPI;

import java.io.*;

public class BukkitMessaging implements Listener {

    public static void sendWarnMessage(String id, ProxiedPlayer player, String punisher, String color, String symbol, String language, String reason) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("warn_message");
            out.writeUTF(id);
            out.writeUTF(punisher);
            out.writeUTF(color);
            out.writeUTF(symbol);
            out.writeUTF(language);
            out.writeUTF(reason);
            player.getServer().sendData("InternalAPI", b.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendWarnSuccess(String group, ProxiedPlayer punisher, String punished, String color, String symbol, String language, String reason) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("warn_success");
            out.writeUTF(group);
            out.writeUTF(punished);
            out.writeUTF(color);
            out.writeUTF(symbol);
            out.writeUTF(language);
            out.writeUTF(reason);
            punisher.getServer().sendData("InternalAPI", b.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendKickSuccess(String group, ProxiedPlayer punisher, String punished, String color, String symbol, String language, String reason) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("kick_success");
            out.writeUTF(group);
            out.writeUTF(punished);
            out.writeUTF(color);
            out.writeUTF(symbol);
            out.writeUTF(language);
            out.writeUTF(reason);
            punisher.getServer().sendData("InternalAPI", b.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendPermaBanSuccess(String group, ProxiedPlayer punisher, String punished, String color, String symbol, String language, String reason) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("pb_success");
            out.writeUTF(group);
            out.writeUTF(punished);
            out.writeUTF(color);
            out.writeUTF(symbol);
            out.writeUTF(language);
            out.writeUTF(reason);
            punisher.getServer().sendData("InternalAPI", b.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendTempBanSuccess(String group, ProxiedPlayer punisher, String punished, String color, String symbol, String language, String reason) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("tb_success");
            out.writeUTF(group);
            out.writeUTF(punished);
            out.writeUTF(color);
            out.writeUTF(symbol);
            out.writeUTF(language);
            out.writeUTF(reason);
            punisher.getServer().sendData("InternalAPI", b.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendSound(ProxiedPlayer player, String sound) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("play_sound");
            out.writeUTF(sound);
            player.getServer().sendData("InternalAPI", b.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) {
        if (event.getTag().equalsIgnoreCase("BungeeCord")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(event.getData()));
            try {
                String channel = in.readUTF();
                if (channel.equals("InternalAPI")){
                    ProxiedPlayer player = BungeeAPI.getInstance().getProxy().getPlayer(event.getReceiver().toString());
                    String input = in.readUTF();
                    System.out.println("Input: " + input);
                    if (input.equals("ExecuteCommand")) {
                        String command = in.readUTF();
                        ProxyServer.getInstance().getPluginManager().dispatchCommand(player, command);
                    }
                    if (input.equals("SendServer")) {
                        String group = in.readUTF();
                        BungeeAPI.sendToRandomServer(player, group);
                    }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}