package net.seocraft.api.shared.commons;

import net.seocraft.api.shared.models.Friend;

import java.util.*;

public class UserSort {

    public static List<Friend> friend_sort(List<Friend> list, String type, String reversed) {
        List<Friend> friends_data_backup = new ArrayList<>();
        friends_data_backup.addAll(list);
        if (type.equals("1")) {
            if (reversed.equals("true")) {
                List<Friend> fixed_list_raw = new ArrayList<>();
                for (Friend friend : friends_data_backup) {
                    if (friend.getLast_online().equals("conectado")) {
                        fixed_list_raw.add(friend);
                        list.remove(friend);
                    }
                }
                if (list.size() >= 0) {
                    Collections.sort(list, new Comparator<Friend>() {
                        @Override
                        public int compare(Friend one, Friend two) {
                            return one.getUsername().compareToIgnoreCase(two.getUsername());
                        }
                    });
                }
                List<Friend> fixed_list = sort_fixed(fixed_list_raw);
                list.addAll(0, fixed_list);
                return reverse_list(list);
            } else {
                List<Friend> fixed_list_raw = new ArrayList<>();
                for (Friend friend : friends_data_backup) {
                    if (friend.getLast_online().equals("conectado")) {
                        fixed_list_raw.add(friend);
                        list.remove(friend);
                    }
                }
                if (list.size() >= 0) {
                    Collections.sort(list, new Comparator<Friend>() {
                        @Override
                        public int compare(Friend one, Friend two) {
                            return one.getUsername().compareToIgnoreCase(two.getUsername());
                        }
                    });
                }
                List<Friend> fixed_list = sort_fixed(fixed_list_raw);
                list.addAll(0, fixed_list);
                return list;
            }
        }
        if (type.equals("2")) {
            if (reversed.equals("true")) {
                Collections.sort(list, new Comparator<Friend>() {
                    @Override
                    public int compare(Friend one, Friend two) {
                        return one.getUsername().compareToIgnoreCase(two.getUsername());
                    }
                });
                return reverse_list(list);
            } else {
                Collections.sort(list, new Comparator<Friend>() {
                    @Override
                    public int compare(Friend one, Friend two) {
                        return one.getUsername().compareToIgnoreCase(two.getUsername());
                    }
                });
                return list;
            }
        }
        if (type.equals("3")) {
            if (reversed.equals("true")) {
                List<Friend> fixed_list_raw = new ArrayList<>();
                for (Friend friend : friends_data_backup) {
                    if (friend.getLast_online().equals("conectado")) {
                        fixed_list_raw.add(friend);
                        list.remove(friend);
                    }
                }
                if (list.size() >= 0) {
                    Collections.sort(list, new Comparator<Friend>() {
                        @Override
                        public int compare(Friend one, Friend two) {
                            return Integer.parseInt(one.getLast_online())-Integer.parseInt(two.getLast_online());
                        }
                    });
                }
                List<Friend> fixed_list = sort_fixed(fixed_list_raw);
                list.addAll(0, fixed_list);
                return reverse_list(list);
            } else {
                List<Friend> fixed_list_raw = new ArrayList<>();
                for (Friend friend : friends_data_backup) {
                    if (friend.getLast_online().equals("conectado")) {
                        fixed_list_raw.add(friend);
                        list.remove(friend);
                    }
                }
                if (list.size() >= 0) {
                    Collections.sort(list, new Comparator<Friend>() {
                        @Override
                        public int compare(Friend one, Friend two) {
                            return Integer.parseInt(one.getLast_online())-Integer.parseInt(two.getLast_online());
                        }
                    });
                }
                List<Friend> fixed_list = sort_fixed(fixed_list_raw);
                list.addAll(0, fixed_list);
                return list;
            }
        }
        return list;
    }

    public static List<Friend> reverse_list(List<Friend> list) {
        Collections.reverse(list);
        return list;
    }

    public static List<Friend> sort_fixed(List<Friend> list) {
        Collections.sort(list, new Comparator<Friend>() {
            @Override
            public int compare(Friend one, Friend two) {
                return one.getUsername().compareToIgnoreCase(two.getUsername());
            }
        });
        return list;
    }
}