package net.seocraft.api.shared.processor;

import net.seocraft.api.shared.i18n.TranslatableComponent;

public class GameOnlineProcessor {
    public static String processor(String game) {
        TranslatableComponent injector = new TranslatableComponent();
        if(!game.contains("lobby")) {
            String replaced = game.replace("-", " ").replace("_", " ");
            return Character.toUpperCase(replaced.charAt(0)) + replaced.substring(1);
        } else {
            String replaced = game.split("-")[0].replace("_", " ");
            return Character.toUpperCase(replaced.charAt(0)) + replaced.substring(1);
        }
    }
}