package net.seocraft.api.shared.commons;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.Locale;

public class TimestampAgo {
    public static String convert(String timestamp, String locale) {
        Integer fixed_stamp = Integer.parseInt(timestamp);
        Date date = new Date(fixed_stamp * 1000L);
        PrettyTime ago_raw = new PrettyTime(new Locale(locale));
        return capitalize(ago_raw.format(date));
    }

    private static String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
}