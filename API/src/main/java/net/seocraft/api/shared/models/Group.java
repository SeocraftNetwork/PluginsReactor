package net.seocraft.api.shared.models;

public class Group {

    public String name;
    public String color;
    public String symbol;
    public Integer priority;

    public Group(String name, String color, String symbol, Integer priority){
        this.name = name;
        this.color = color;
        this.symbol = symbol;
        this.priority = priority;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getSymbol() {
        return symbol;
    }

    public Integer getPriority() {
        return priority;
    }
}