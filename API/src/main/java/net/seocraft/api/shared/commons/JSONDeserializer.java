package net.seocraft.api.shared.commons;

import com.google.gson.*;

public class JSONDeserializer {

    public static JsonObject deserialize(String raw) {
        JsonElement element = new JsonParser().parse(raw);
        return element.getAsJsonObject();
    }

    public static String errorMessage(String raw) {
        JsonElement message = new JsonParser().parse(raw);
        JsonObject object = message.getAsJsonObject();
        return object.get("message").getAsString();
    }

    public static String jsonString(JsonObject object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static String jsonArray(JsonArray array) {
        Gson gson = new Gson();
        return gson.toJson(array);
    }
}