package net.seocraft.api.shared.processor;

import java.util.HashMap;

public enum TimeUnit {
    SEGUNDOS("Segundos", "s", 1),
    MINUTOS("Minutos", "m", 60),
    HORAS("Horas", "h", 60 * 60),
    DIAS("Dias", "d", 60 * 60 * 24),
    SEMANAS("Semanas", "w", 60 * 60 * 24 * 7);

    private String name;
    private String shortcut;
    private long toSecond;

    private static HashMap<String, TimeUnit> ID_SHORTCUT = new HashMap<>();

    private TimeUnit(String name, String shortcut, long toSecond) {
        this.name = name;
        this.shortcut = shortcut;
        this.toSecond = toSecond;
    }

    static {
        for(TimeUnit units : values()){
            ID_SHORTCUT.put(units.shortcut, units);
        }
    }

    public static TimeUnit getFromShortcut(String shortcut){
        return ID_SHORTCUT.get(shortcut);
    }

    public String getName(){
        return name;
    }

    public String getShortcut(){
        return shortcut;
    }

    public long getToSecond() {
        return toSecond;
    }

    public static boolean existFromShortcut(String shortcut){
        return ID_SHORTCUT.containsKey(shortcut);
    }
}