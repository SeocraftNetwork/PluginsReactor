package net.seocraft.api.shared.commons;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;

import javax.annotation.Nullable;
import java.io.IOException;

public class HTTPRequests {

    private static String RestfulURI = "http://127.0.0.1:3800/api";
    private static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    public static String get(String uri, @Nullable String cluster_token, @Nullable String user_token) {
        try {
            GenericUrl url = new GenericUrl(RestfulURI + "/" + uri);
            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory();
            HttpRequest request = requestFactory.buildGetRequest(url);
            HttpHeaders headers = request.getHeaders();
            headers.setContentType("application/json");
            if (cluster_token != null) headers.setAuthorization(cluster_token);
            if (user_token != null) headers.set("game_authorization", user_token);
            HttpResponse raw_response = request.execute();
            return raw_response.parseAsString();
        } catch (IOException e) {
            System.out.println("[HTTP Error] Error en peticion '" + uri + "': " + e.getClass().getCanonicalName());
        }
        return null;
    }

    public static String post(String uri, String params, @Nullable String cluster_token, @Nullable String user_token) {
        try {
            GenericUrl url = new GenericUrl(RestfulURI + "/" + uri);
            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory();
            HttpRequest request = requestFactory.buildPostRequest(url, ByteArrayContent.fromString("application/json", params));
            HttpHeaders headers = request.getHeaders();
            if (cluster_token != null) headers.setAuthorization(cluster_token);
            if (user_token != null) headers.set("game_authorization", user_token);
            HttpResponse raw_response = request.execute();
            return raw_response.parseAsString();
        } catch (IOException e) {
            System.out.println("[HTTP Error] Error en peticion '" + uri + "': " + e.getClass().getCanonicalName());
        }
        return null;
    }

    public static String delete(String uri, @Nullable String cluster_token, @Nullable String user_token) {
        try {
            GenericUrl url = new GenericUrl(RestfulURI + "/" + uri);
            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory();
            HttpRequest request = requestFactory.buildDeleteRequest(url);
            HttpHeaders headers = request.getHeaders();
            headers.setContentType("application/json");
            if (cluster_token != null) headers.setAuthorization(cluster_token);
            if (user_token != null) headers.set("game_authorization", user_token);
            HttpResponse raw_response = request.execute();
            return raw_response.parseAsString();
        } catch (IOException e) {
            System.out.println("[HTTP Error] Error en peticion '" + uri + "': " + e.getClass().getCanonicalName());
        }
        return null;
    }
}