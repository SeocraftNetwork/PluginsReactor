package net.seocraft.api.shared.processor;

import net.seocraft.api.shared.i18n.TranslatableComponent;

public class FriendListChatProcessor {
    public static String processor(String game, String connected, String language) {
        TranslatableComponent injector = new TranslatableComponent();
        if (!connected.equals("conectado")) {
            return " %red%" + injector.getString(language, "commons_friends_list_offline");
        } else {
            if(!game.contains("lobby")) {
                String replaced = game.replace("-", " ");
                return " %yellow%" + injector.getString(language, "commons_friends_list_in_game").replace("[0]", Character.toUpperCase(replaced.charAt(0)) + replaced.substring(1));
            } else {
                String replaced = game.split("-")[0];
                return " %yellow%" + injector.getString( language,"commons_friends_list_in_lobby").replace("[0]", Character.toUpperCase(replaced.charAt(0)) + replaced.substring(1));
            }
        }
    }
}