package net.seocraft.api.shared.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.*;
import java.util.HashMap;

public class BungeeMessaging implements PluginMessageListener {

    private static HashMap<String, Integer> player_count = new HashMap<>();

    public static void applySkin(Player player, String skin) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("InternalAPI");
            out.writeUTF("SkinUpdate");
            out.writeUTF(skin);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(BukkitAPI.getInstance(), "BungeeCord", b.toByteArray());
    }

    public static void sendServer(Player player, String group) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("InternalAPI");
            out.writeUTF("SendServer");
            out.writeUTF(group);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(BukkitAPI.getInstance(), "BungeeCord", b.toByteArray());
    }

    public static void applySkin(Player player, String value, String signature) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("InternalAPI");
            out.writeUTF("SkinUpdate-Manual");
            out.writeUTF(value);
            out.writeUTF(signature);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(BukkitAPI.getInstance(), "BungeeCord", b.toByteArray());
    }

    public static void executeBungeeCommand(Player player, String command) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("InternalAPI");
            out.writeUTF("ExecuteCommand");
            out.writeUTF(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(BukkitAPI.getInstance(), "BungeeCord", b.toByteArray());
    }

    public static void sendStaffRequest(Player player) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("InternalAPI");
            out.writeUTF("StaffRequest");
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(BukkitAPI.getInstance(), "BungeeCord", b.toByteArray());
    }

    public static void askPlayerCount(String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PlayerCount");
        out.writeUTF(server);
        Player player = Bukkit.getPlayerExact("Example");
        player.sendPluginMessage(BukkitAPI.getInstance(), "BungeeCord", out.toByteArray());
    }

    public static Integer getPlayerCount(String server) {
        return player_count.get(server);
    }

    @Override
    public synchronized void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("InternalAPI") && !channel.equals("BungeeCord")) return;
        Bukkit.getScheduler().runTaskAsynchronously(BukkitAPI.getInstance(), new Runnable() {
            @Override
            public void run() {
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
                try {
                    String subchannel = in.readUTF();
                    if (subchannel.equals("PlayerCount")) {
                        String server = in.readUTF();
                        int playerCount = in.readInt();
                        player_count.put(server, playerCount);
                    }
                    if (subchannel.equalsIgnoreCase("warn_message")) {
                        TranslatableComponent injector = new TranslatableComponent();
                        String id = in.readUTF();
                        String punisher = in.readUTF();
                        String color = in.readUTF();
                        String symbol = in.readUTF();
                        String language = in.readUTF();
                        String reason = in.readUTF();
                        TextComponent punisher_placeholder = new TextComponent(GetAPIColor.getColor(color) + "" + ChatColor.BOLD + symbol + " " + punisher + " ");
                        punisher_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(color) + punisher) + " ").create()));
                        punisher_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + punisher));
                        TextComponent punishment_message = new TextComponent(injector.getString(language, "commons_admin_punishments_warned_text").replace("[1]", reason).toLowerCase() + ". ");
                        punishment_message.setBold(true);
                        punishment_message.setColor(net.md_5.bungee.api.ChatColor.RED);
                        TextComponent punishment_info = new TextComponent(injector.getString(language, "commons_admin_punishments_more_info"));
                        punishment_info.setBold(true);
                        punishment_info.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
                        punishment_info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_admin_punishments_warned_hover") + " ").create()));
                        punishment_info.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.seocraft.net/punishments/" + id));
                        TitleHandler.sendTitle("[{\"text\": \"\u26A0 \", \"color\":\"" + ChatColor.RED.name().toLowerCase() + "\"}, {\"text\": \"" + injector.getString(language, "commons_admin_punishments_warn").toUpperCase() +"\", \"color\":\"" + ChatColor.YELLOW.name().toLowerCase() + "\"}, {\"text\": \" \u26A0\", \"color\":\"" + ChatColor.RED.name().toLowerCase() + "\"}]",
                                "{\"text\": \"" + reason + "\", \"color\":\"" + ChatColor.AQUA.name().toLowerCase() + "\"}", player);
                        player.playSound(player.getLocation(), Sound.WITHER_DEATH, 1f, 1f);
                        player.spigot().sendMessage(punisher_placeholder, punishment_message, punishment_info);
                    }
                    if (subchannel.equalsIgnoreCase("warn_success")) {
                        TranslatableComponent injector = new TranslatableComponent();
                        String group = in.readUTF();
                        String punished = in.readUTF();
                        String color = in.readUTF();
                        String symbol = in.readUTF();
                        String language = in.readUTF();
                        String reason = in.readUTF();
                        TextComponent punisher_message = new TextComponent(injector.getString(language, "commons_admin_punishment_warned_success") + " ");
                        punisher_message.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        TextComponent punished_placeholder;
                        if (!group.equalsIgnoreCase("usuario")) {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) + symbol + " " + punished);
                        } else {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) +  punished);
                        }
                        punished_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(color) + punished)).create()));
                        punished_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + punished));
                        String reason_placheolder = injector.getString(language, "reason_placeholder");
                        TextComponent punisher_reason = new TextComponent(ChatColor.AQUA + ". " + (reason_placheolder.substring(0, 1).toUpperCase() + reason_placheolder.substring(1)) + ": " + ChatColor.GREEN + (reason.substring(0, 1).toUpperCase() + reason.substring(1)));
                        punisher_reason.setColor(net.md_5.bungee.api.ChatColor.GREEN);
                        player.spigot().sendMessage(punisher_message, punished_placeholder, punisher_reason);
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                    }
                    if (subchannel.equalsIgnoreCase("kick_success")) {
                        TranslatableComponent injector = new TranslatableComponent();
                        String group = in.readUTF();
                        String punished = in.readUTF();
                        String color = in.readUTF();
                        String symbol = in.readUTF();
                        String language = in.readUTF();
                        String reason = in.readUTF();
                        TextComponent punisher_message = new TextComponent(injector.getString(language, "commons_admin_punishment_kicked_success") + " ");
                        punisher_message.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        TextComponent punished_placeholder;
                        if (!group.equalsIgnoreCase("usuario")) {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) + symbol + " " + punished);
                        } else {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) +  punished);
                        }
                        punished_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(color) + punished)).create()));
                        punished_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + punished));
                        String reason_placheolder = injector.getString(language, "reason_placeholder");
                        TextComponent punisher_reason = new TextComponent(ChatColor.AQUA + ". " + (reason_placheolder.substring(0, 1).toUpperCase() + reason_placheolder.substring(1)) + ": " + ChatColor.GREEN + (reason.substring(0, 1).toUpperCase() + reason.substring(1)));
                        punisher_reason.setColor(net.md_5.bungee.api.ChatColor.GREEN);
                        player.spigot().sendMessage(punisher_message, punished_placeholder, punisher_reason);
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                    }
                    if (subchannel.equalsIgnoreCase("pb_success")) {
                        TranslatableComponent injector = new TranslatableComponent();
                        String group = in.readUTF();
                        String punished = in.readUTF();
                        String color = in.readUTF();
                        String symbol = in.readUTF();
                        String language = in.readUTF();
                        String reason = in.readUTF();
                        TextComponent punisher_message = new TextComponent(injector.getString(language, "commons_admin_punishment_permanent_ban_success") + " ");
                        punisher_message.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        TextComponent punished_placeholder;
                        if (!group.equalsIgnoreCase("usuario")) {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) + symbol + " " + punished);
                        } else {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) +  punished);
                        }
                        punished_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(color) + punished)).create()));
                        punished_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + punished));
                        String reason_placheolder = injector.getString(language, "reason_placeholder");
                        TextComponent punisher_reason = new TextComponent(ChatColor.AQUA + ". " + (reason_placheolder.substring(0, 1).toUpperCase() + reason_placheolder.substring(1)) + ": " + ChatColor.GREEN + (reason.substring(0, 1).toUpperCase() + reason.substring(1)));
                        punisher_reason.setColor(net.md_5.bungee.api.ChatColor.GREEN);
                        player.spigot().sendMessage(punisher_message, punished_placeholder, punisher_reason);
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                    }
                    if (subchannel.equalsIgnoreCase("tb_success")) {
                        TranslatableComponent injector = new TranslatableComponent();
                        String group = in.readUTF();
                        String punished = in.readUTF();
                        String color = in.readUTF();
                        String symbol = in.readUTF();
                        String language = in.readUTF();
                        String reason = in.readUTF();
                        TextComponent punisher_message = new TextComponent(injector.getString(language, "commons_admin_punishment_temporal_ban_success") + " ");
                        punisher_message.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        TextComponent punished_placeholder;
                        if (!group.equalsIgnoreCase("usuario")) {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) + symbol + " " + punished);
                        } else {
                            punished_placeholder = new TextComponent(GetAPIColor.getColor(color) +  punished);
                        }
                        punished_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(color) + punished)).create()));
                        punished_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + punished));
                        String reason_placheolder = injector.getString(language, "reason_placeholder");
                        TextComponent punisher_reason = new TextComponent(ChatColor.AQUA + ". " + (reason_placheolder.substring(0, 1).toUpperCase() + reason_placheolder.substring(1)) + ": " + ChatColor.GREEN + (reason.substring(0, 1).toUpperCase() + reason.substring(1)));
                        punisher_reason.setColor(net.md_5.bungee.api.ChatColor.GREEN);
                        player.spigot().sendMessage(punisher_message, punished_placeholder, punisher_reason);
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                    }
                    if (subchannel.equalsIgnoreCase("play_sound")) {
                        String sound = in.readUTF();
                        switch (sound) {
                            case "pling": {
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                                break;
                            }
                            case "bass": {
                                player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                                break;
                            }
                            case "harp": {
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 1f);
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}