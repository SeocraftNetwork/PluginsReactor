package net.seocraft.api.shared.utils;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class ChatProcessor {

    public static String lobbyChatProcessor(String string) {
        return string.replace("<3", ChatColor.RED + "\u2674" + ChatColor.WHITE)
                .replace(":star:", ChatColor.GOLD + "\u272D" + ChatColor.WHITE)
                .replace(":shrug:",ChatColor.YELLOW + "¯\\_(ツ)_/¯" + ChatColor.WHITE)
                .replace(":tableflip:",ChatColor.RED + "(╯°□°）╯" + ChatColor.GRAY + "︵ ┻━┻" + ChatColor.WHITE)
                .replace(":123:", ChatColor.GREEN + "1" + ChatColor.YELLOW + "2" + ChatColor.RED + "3" + ChatColor.WHITE);
    }

    public static List<String> loreProcessor(String message_input, String replace, ChatColor color) {
        String[] message_raw = message_input.split(replace);
        List<String> list = new ArrayList<>();
        for (String piece : message_raw) {
            list.add(color + piece);
        }
        return list;
    }

    public static List<String> loreProcessor(String message_input, String replace) {
        String[] message_raw = message_input.split(replace);
        List<String> list = new ArrayList<>();
        for (String piece : message_raw) {
            list.add(piece);
        }
        return list;
    }

    public static List<String> convertStringArray(String[] array) {
        List<String> list = new ArrayList<>();
        for (String piece : array) {
            list.add(piece);
        }
        return list;
    }
}