package net.seocraft.api.shared.utils;

import org.bukkit.ChatColor;

public class GetAPIColor {

    public static ChatColor getColor(String color) {
        switch (color) {
            case "black": {
                return ChatColor.BLACK;
            }
            case "dark_blue": {
                return ChatColor.DARK_BLUE;
            }
            case "dark_green": {
                return ChatColor.DARK_GREEN;
            }
            case "dark_aqua": {
                return ChatColor.DARK_AQUA;
            }
            case "dark_red": {
                return ChatColor.DARK_RED;
            }
            case "dark_purple": {
                return ChatColor.DARK_PURPLE;
            }
            case "gold": {
                return ChatColor.GOLD;
            }
            case "gray": {
                return ChatColor.GRAY;
            }
            case "dark_gray": {
                return ChatColor.DARK_GRAY;
            }
            case "blue": {
                return ChatColor.BLUE;
            }
            case "green": {
                return ChatColor.GREEN;
            }
            case "aqua": {
                return ChatColor.AQUA;
            }
            case "red": {
                return ChatColor.RED;
            }
            case "light_purple": {
                return ChatColor.LIGHT_PURPLE;
            }
            case "yellow": {
                return ChatColor.YELLOW;
            }
            default: {
                return ChatColor.WHITE;
            }
        }
    }
}