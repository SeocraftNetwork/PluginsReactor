package net.seocraft.api.shared.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TranslatableComponent {

    public String getString(String locale, String translation) {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = getClass().getResourceAsStream(locale + ".properties");
            prop.load(input);
            return prop.getProperty(translation);
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if(input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}