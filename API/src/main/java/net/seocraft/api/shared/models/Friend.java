package net.seocraft.api.shared.models;

import javax.annotation.Nullable;

public class Friend {

    private String username;
    private Integer level;
    private Group group;
    private Boolean disguised;
    private String skin;
    private String last_online;
    private String last_game;
    private String disguised_actual;
    private Group disguised_group;

    public Friend(String username, Integer level, Group group, Boolean disguised, String skin, String last_online, String last_game,  @Nullable String disguised_actual, @Nullable Group disguised_group) {
        this.username = username;
        this.level = level;
        this.group = group;
        this.disguised = disguised;
        this.skin = skin;
        this.last_online = last_online;
        this.last_game = last_game;
        this.disguised_actual = disguised_actual;
        this.disguised_group = disguised_group;
    }

    public String getUsername() {
        return username;
    }

    public Integer getLevel() {
        return level;
    }

    public Group getGroup() {
        return group;
    }

    public Boolean getDisguised() {
        return disguised;
    }

    public String getSkin() { return skin; }

    public String getLast_game() { return last_game; }

    public String getLast_online() {
        return last_online;
    }

    public String getDisguise_actual() {
        return disguised_actual;
    }

    public Group getDisguise_group() {
        return disguised_group;
    }
}