package net.seocraft.api.shared.utils;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TitleHandler {
    public static void sendTitle(String top, String bottom, Player player) {
        IChatBaseComponent title_top = IChatBaseComponent.ChatSerializer.a(top);
        IChatBaseComponent title_bottom = IChatBaseComponent.ChatSerializer.a(bottom);
        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, title_top);
        PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, title_bottom);
        PacketPlayOutTitle length = new PacketPlayOutTitle(20, 60, 20);

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(subtitle);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(length);
    }
}