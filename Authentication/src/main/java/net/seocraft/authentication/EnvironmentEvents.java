package net.seocraft.authentication;

import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldLoadEvent;

public class EnvironmentEvents implements Listener {

    private static Authentication plugin = Authentication.getInstance();

    @EventHandler
    public void onWorldLoad(WorldLoadEvent event) {

        World world = event.getWorld();
        world.setDifficulty(Difficulty.PEACEFUL);

        for(Entity e : world.getEntities()){
            e.remove();
        }

        if(plugin.getConfig().getString("special.holiday").toLowerCase().equals("halloween")) {
            world.setTime(18000);
            world.setStorm(false);
        } else if(plugin.getConfig().getString("special.holiday").toLowerCase().equals("christmas")) {
            world.setTime(0);
            world.setStorm(true);
        } else {
            world.setTime(0);
            world.setStorm(false);
        }

    }

    @EventHandler
    public void onCreateureSpawn(CreatureSpawnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(true);
    }
}