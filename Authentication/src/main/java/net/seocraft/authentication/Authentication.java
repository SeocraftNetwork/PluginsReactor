package net.seocraft.authentication;

import net.seocraft.authentication.command_framework.CommandFramework;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;

public class Authentication extends JavaPlugin {

    ArrayList<Player> registered = new ArrayList<>();
    ArrayList<Player> multi_account = new ArrayList<>();
    HashMap<Player, String> user_languages = new HashMap<>();
    private static Authentication instance;

    @Override
    public void onEnable() {
        instance = this;
        getServer().getPluginManager().registerEvents(new PlayerEvents(), this);
        getServer().getPluginManager().registerEvents(new EnvironmentEvents(), this);
        this.registerCommands();
        this.loadConfig();
    }

    public void onDisable() {
    }

    private void registerCommands() {
        CommandFramework commandFramework = new CommandFramework(this);
        commandFramework.registerCommands(new LoginCommand());
        commandFramework.registerCommands(new RegisterCommand());
    }

    static Authentication getInstance(){
        return instance;
    }

    private void loadConfig(){
        getConfig().options().copyDefaults(true);
        saveConfig();
    }
}