package net.seocraft.authentication;

import net.seocraft.api.bukkit.packets.CustomSkull;
import net.seocraft.api.bukkit.packets.NBTHandler;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class LanguageHeads {
    public static ItemStack selectHead() {
        ItemStack raw_language = CustomSkull.getSkull("http://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25");
        ItemStack language = NBTHandler.addString(raw_language, "accessor", "LANGUAGE_ACCESSOR");
        ItemMeta LanguageMeta = language.getItemMeta();
        LanguageMeta.setDisplayName(ChatColor.GREEN + "\u00BB " + ChatColor.GOLD + "Cambia tu lenguaje - Change your language" + ChatColor.GREEN + " \u00AB");
        language.setItemMeta(LanguageMeta);

        return language;
    }

    public static ItemStack spanishHead(Boolean actual) {
        ItemStack raw_spanish = CustomSkull.getSkull("http://textures.minecraft.net/texture/a9f5799dfb7de65350337e735651d4c831f1c2a827d584b02d8e875ff8eaa2");
        ItemStack spanish = NBTHandler.addString(raw_spanish, "accessor", "SPANISH_LANGUAGE");
        ItemMeta SpanishMeta = spanish.getItemMeta();
        SpanishMeta.setDisplayName(ChatColor.YELLOW + "Español");
        ArrayList<String> spanish_lore = new ArrayList<>();
        spanish_lore.add(ChatColor.GRAY + "Selecciona el idioma español");
        spanish_lore.add("");
        if(!actual) { spanish_lore.add(ChatColor.AQUA + "¡Haz click para cambiar tu lenguaje!"); } else { spanish_lore.add(ChatColor.RED + "¡Ya haz seleccionado este lenguaje!"); }
        SpanishMeta.setLore(spanish_lore);
        spanish.setItemMeta(SpanishMeta);

        return spanish;
    }

    public static ItemStack englishHead(Boolean actual) {
        ItemStack raw_english = CustomSkull.getSkull("http://textures.minecraft.net/texture/4cac9774da1217248532ce147f7831f67a12fdcca1cf0cb4b3848de6bc94b4");
        ItemStack english = NBTHandler.addString(raw_english, "accessor", "ENGLISH_LANGUAGE");
        ItemMeta EnglishMeta = english.getItemMeta();
        EnglishMeta.setDisplayName(ChatColor.YELLOW + "English");
        ArrayList<String> english_lore = new ArrayList<>();
        english_lore.add(ChatColor.GRAY + "Select english language");
        english_lore.add("");
        if(!actual) { english_lore.add(ChatColor.AQUA + "Click to change your language!"); } else { english_lore.add(ChatColor.RED + "You have already selected this language!"); }
        EnglishMeta.setLore(english_lore);
        english.setItemMeta(EnglishMeta);

        return english;
    }

    public static ItemStack francaisHead(Boolean actual) {
        ItemStack raw_francais = CustomSkull.getSkull("http://textures.minecraft.net/texture/9b3495e9dbd5a426e1446e6627bf8dd55d9612ce3b55a8596e112b28db9ea3a");
        ItemStack francais = NBTHandler.addString(raw_francais, "accessor", "FRANCAIS_LANGUAGE");
        ItemMeta FrancaisMeta = francais.getItemMeta();
        FrancaisMeta.setDisplayName(ChatColor.YELLOW + "Français");
        ArrayList<String> francais_lore = new ArrayList<>();
        francais_lore.add(ChatColor.GRAY + "Sélectionnez la langue française");
        francais_lore.add("");
        if(!actual) { francais_lore.add(ChatColor.AQUA + "Cliquez pour changer votre langue!"); } else { francais_lore.add(ChatColor.RED + "Vous avez déjà sélectionné cette langue!"); }
        FrancaisMeta.setLore(francais_lore);
        francais.setItemMeta(FrancaisMeta);

        return francais;
    }
}