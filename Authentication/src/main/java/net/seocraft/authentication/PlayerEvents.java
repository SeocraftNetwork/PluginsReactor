package net.seocraft.authentication;

import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.PacketPlayOutCamera;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.seocraft.api.bukkit.packets.NBTHandler;
import net.seocraft.api.bukkit.requests.login.JoinRequest;
import net.seocraft.api.bukkit.requests.user.LanguageUpdate;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

@Deprecated
public class PlayerEvents implements Listener {

    private static Authentication plugin = Authentication.getInstance();

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        World w = Bukkit.getServer().getWorld(plugin.getConfig().getString("spawn.world"));
        int x = plugin.getConfig().getInt("spawn.x");
        int y = plugin.getConfig().getInt("spawn.y");
        int z = plugin.getConfig().getInt("spawn.z");
        int yaw = plugin.getConfig().getInt("spawn.yaw");
        int pitch = plugin.getConfig().getInt("spawn.pitch");

        EntityArmorStand stand = new EntityArmorStand(((CraftWorld)w).getHandle());
        stand.setLocation(x, y, z, yaw, pitch);
        stand.setInvisible(true);
        stand.setGravity(false);
        ((CraftPlayer)event.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(stand));
        ((CraftPlayer)event.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutCamera(stand));
        event.setJoinMessage("");
        Player player = event.getPlayer();
        player.getInventory().clear();
        PermissionsManifest.setupPermissions(player);
        player.setGameMode(GameMode.ADVENTURE);
        player.setHealth(20);
        player.setFoodLevel(20);
        player.getInventory().setHeldItemSlot(4);
        player.teleport(new Location(w, x, y, z, yaw, pitch));
        player.getInventory().setItem(4, LanguageHeads.selectHead());
        String[] logged_user = JoinRequest.request(player.getName(), player.getAddress().toString().split(":")[0].replace("/", ""));
        Boolean new_user = Boolean.valueOf(logged_user[0]);
        Boolean multi_account = Boolean.valueOf(logged_user[1]);
        plugin.user_languages.put(player, logged_user[4]);
        String player_language = plugin.user_languages.get(player);
        TranslatableComponent injector = new TranslatableComponent();
        if(!new_user) {
            plugin.registered.add(player);
            player.sendTitle(ChatColor.AQUA + injector.getString(player_language, "welcome_back_title"), ChatColor.YELLOW + injector.getString(player_language, "login_title") + " " + ChatColor.RED + "/login <"+ injector.getString(player_language, "password_placeholder") + ">");
        } else {
            player.sendTitle(ChatColor.AQUA + injector.getString(player_language, "welcome_new_title"), ChatColor.YELLOW + injector.getString(player_language, "register_title") + ChatColor.RED + "/register <contraseña>");
        }
        if(multi_account) {
            plugin.multi_account.add(player);
            Bukkit.getServer().getPlayer(player.getName()).kickPlayer(ChatColor.RED + injector.getString(player_language, "accounts_per_ip") + "\n\n"
                    + ChatColor.AQUA + "\u00BB " + logged_user[3] + " " + ChatColor.GRAY + "(" + injector.getString(player_language, "account_created_at") + " " + logged_user[2] + ")\n\n" +
                    ChatColor.YELLOW + injector.getString(player_language, "more_info") + " " + ChatColor.GOLD + "seocraft.net/reglas");
        }
        for (Player players : Bukkit.getOnlinePlayers()) {
            player.hidePlayer(players);
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        if (plugin.registered.contains(event.getPlayer())) {
            plugin.registered.remove(event.getPlayer());
        }
        if (!plugin.multi_account.contains(event.getPlayer())) {
            plugin.multi_account.remove(event.getPlayer());
        }
        plugin.user_languages.remove(event.getPlayer());
        event.setQuitMessage("");
    }

    @EventHandler
    public void onHungerChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String player_language = plugin.user_languages.get(player);
        TranslatableComponent injector = new TranslatableComponent();
        player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
        if(!plugin.registered.contains(player)) {
            player.sendMessage(ChatColor.RED + injector.getString(player_language, "not_registered") + " " + ChatColor.YELLOW + "/register <" + injector.getString(player_language, "password_placeholder") + ">");
        } else {
            player.sendMessage(ChatColor.RED + injector.getString(player_language, "not_logged") + " " + ChatColor.YELLOW + "/login <" + injector.getString(player_language, "password_placeholder") + ">");
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String player_language = plugin.user_languages.get(player);
        TranslatableComponent injector = new TranslatableComponent();
        if((NBTHandler.getString(player.getItemInHand(), "accessor")).equals("LANGUAGE_ACCESSOR")) {
            Inventory gui = Bukkit.getServer().createInventory(null, 9, ChatColor.DARK_GRAY + injector.getString(player_language, "select_language"));
            if(player_language.equals("en")) { gui.setItem(3, LanguageHeads.englishHead(true)); } else { gui.setItem(3, LanguageHeads.englishHead(false)); }
            if(player_language.equals("es")) { gui.setItem(4, LanguageHeads.spanishHead(true)); } else { gui.setItem(4, LanguageHeads.spanishHead(false)); }
            if(player_language.equals("fr")) { gui.setItem(5, LanguageHeads.francaisHead(true)); } else { gui.setItem(5, LanguageHeads.francaisHead(false)); }
            player.openInventory(gui);
        }
    }

    @EventHandler
    public void inventoryClick(InventoryClickEvent event) {
        HumanEntity entity = event.getWhoClicked();
        if (event.getCurrentItem()== null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
            return;
        }
        event.setCancelled(true);
        if((entity instanceof Player)) {
            Player player = (Player)entity;
            String player_language = plugin.user_languages.get(player);
            TranslatableComponent injector = new TranslatableComponent();
            ItemStack clicked = event.getCurrentItem();
            switch(NBTHandler.getString(clicked, "accessor")) {
                case "SPANISH_LANGUAGE": {
                    if(!player_language.equals("es")) {
                        if(!LanguageUpdate.request(player.getName(), "es").equals("true")) {
                            player.closeInventory();
                            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                            player.sendMessage(ChatColor.RED + injector.getString(player_language, "language_switch_error") + ".");
                        } else {
                            plugin.user_languages.remove(player);
                            plugin.user_languages.put(player, "es");
                            player.closeInventory();
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                            player.sendMessage(ChatColor.AQUA + injector.getString(plugin.user_languages.get(player), "language_switch") +": " + ChatColor.YELLOW + injector.getString(plugin.user_languages.get(player), "language"));
                        }
                    } else {
                        player.closeInventory();
                        player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                        player.sendMessage(ChatColor.RED + injector.getString(player_language, "language_already_selected") + ".");
                    }
                    break;
                }
                case "ENGLISH_LANGUAGE": {
                    if(!player_language.equals("en")) {
                        if(!LanguageUpdate.request(player.getName(), "en").equals("true")) {
                            player.closeInventory();
                            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                            player.sendMessage(ChatColor.RED + injector.getString(player_language, "language_switch_error") + ".");
                        } else {
                            plugin.user_languages.remove(player);
                            plugin.user_languages.put(player, "en");
                            player.closeInventory();
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                            player.sendMessage(ChatColor.AQUA + injector.getString(plugin.user_languages.get(player), "language_switch") +": " + ChatColor.YELLOW + injector.getString(plugin.user_languages.get(player), "language"));
                        }
                    } else {
                        player.closeInventory();
                        player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                        player.sendMessage(ChatColor.RED + injector.getString(player_language, "language_already_selected") + ".");
                    }
                    break;
                }
                case "FRANCAIS_LANGUAGE": {
                    if(!player_language.equals("fr")) {
                        if(!LanguageUpdate.request(player.getName(), "fr").equals("true")) {
                            player.closeInventory();
                            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                            player.sendMessage(ChatColor.RED + injector.getString(player_language, "language_switch_error") + ".");
                        } else {
                            plugin.user_languages.remove(player);
                            plugin.user_languages.put(player, "fr");
                            player.closeInventory();
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                            player.sendMessage(ChatColor.AQUA + injector.getString(plugin.user_languages.get(player), "language_switch") +": " + ChatColor.YELLOW + injector.getString(plugin.user_languages.get(player), "language"));
                        }
                    } else {
                        player.closeInventory();
                        player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                        player.sendMessage(ChatColor.RED + injector.getString(player_language, "language_already_selected") + ".");
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onSlotSwitch(PlayerItemHeldEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerClickTab(PlayerChatTabCompleteEvent event) {
        event.getTabCompletions().clear();
        event.getChatMessage().charAt(0);
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event)  {
        Player player = event.getPlayer();
        String player_language = plugin.user_languages.get(player);
        TranslatableComponent injector = new TranslatableComponent();
        if((event.getMessage().equals("/pl")) || (event.getMessage().equals("/plugins"))) {
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
            player.sendMessage(ChatColor.AQUA + injector.getString(player_language, "plugins_message") +" " + ChatColor.YELLOW + "www.seocraft.net/developers");
            event.setCancelled(true);
        } else if(event.getMessage().contains("/register") || event.getMessage().contains("/login")) {
            event.setCancelled(false);
        } else {
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
            player.sendMessage(ChatColor.RED + injector.getString(player_language, "unavailable_command") + ".");
            event.setCancelled(true);
        }
    }
}