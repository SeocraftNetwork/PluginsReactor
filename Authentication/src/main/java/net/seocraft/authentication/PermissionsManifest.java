package net.seocraft.authentication;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

public class PermissionsManifest {
    private static Authentication plugin = Authentication.getPlugin(Authentication.class);

    public static void setupPermissions(Player player) {
        PermissionAttachment attachment = player.addAttachment(plugin);
        attachment.setPermission("commons.commands.basics", true);
    }
}