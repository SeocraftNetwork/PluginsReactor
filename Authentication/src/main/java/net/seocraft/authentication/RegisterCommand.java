package net.seocraft.authentication;

import net.seocraft.api.bukkit.requests.login.RegisterRequest;
import net.seocraft.api.bukkit.util.RedisConnection;
import net.seocraft.api.shared.commons.ServerRedirector;
import net.seocraft.api.shared.utils.BungeeMessaging;
import net.seocraft.authentication.command_framework.Command;
import net.seocraft.authentication.command_framework.CommandArgs;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class RegisterCommand {

    private static Authentication plugin = Authentication.getInstance();

    @Command(name = "register", aliases = {"r", "registro"}, description = "Comando usado para registrar al usuario en la base de datos", usage = "/register <contraseña>", inGameOnly = true, permission = "")
    public void onCommand(CommandArgs args) {
        Player player = args.getPlayer();
        String player_language = plugin.user_languages.get(player);
        TranslatableComponent injector = new TranslatableComponent();
        if (args.length() < 1) {
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
            player.sendMessage(ChatColor.RED + injector.getString(player_language, "register_incorrect") + ".");
        } else if (args.getArgs().length >= 1) {
            if(!plugin.registered.contains(player)) {
                String[] request = RegisterRequest.request(player.getName(), args.getArgs()[0], player.getAddress().toString().split(":")[0].replace("/", ""));
                if (!request[0].equals("true")) {
                    player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                    player.sendMessage(ChatColor.RED + injector.getString(player_language, "register_error") + ".");
                } else {
                    player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                    player.sendMessage(ChatColor.AQUA + injector.getString(player_language, "register_success") +", " + ChatColor.YELLOW + injector.getString(player_language, "register_welcome"));
                    String token = request[1];
                    RedisConnection.addString(player.getName() + ".minecraft_token", token);
                    ServerRedirector.redirect("main_lobby", args.getPlayer());
                }
            } else if(args.length() > 1) {
                player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                player.sendMessage(ChatColor.RED + injector.getString(player_language, "password_spaces") + ".");
            }

        }
    }
}