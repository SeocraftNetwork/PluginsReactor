package net.seocraft.authentication;

import net.seocraft.api.bukkit.requests.login.LoginRequest;
import net.seocraft.api.bukkit.util.RedisConnection;
import net.seocraft.api.shared.commons.ServerRedirector;
import net.seocraft.api.shared.utils.BungeeMessaging;
import net.seocraft.authentication.command_framework.Command;
import net.seocraft.authentication.command_framework.CommandArgs;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class LoginCommand {

    private static Authentication plugin = Authentication.getInstance();

    @Command(name = "login", aliases = {"l", "login"}, description = "Comando usado para autorizar al usuario a ingresar al servidor.", usage = "/login <contraseña>", inGameOnly = true, permission = "")
    public void onCommand(CommandArgs args) {
        Player player = args.getPlayer();
        String player_language = plugin.user_languages.get(player);
        TranslatableComponent injector = new TranslatableComponent();
        if (args.length() < 1) {
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
            player.sendMessage(ChatColor.RED + injector.getString(player_language, "login_incorrect") + ".");
        } else if (args.length() >= 1) {
            if(!plugin.registered.contains(player)) {
                player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                player.sendMessage(ChatColor.RED + injector.getString(player_language, "not_registered") + " " + ChatColor.YELLOW + "/register <" + injector.getString(player_language, "password_placeholder") + ">");
            } else {
                String[] login_info = LoginRequest.request(player.getName(), args.getArgs()[0]);
                Boolean login_valid = Boolean.valueOf(login_info[0]);
                if (!login_valid) {
                    player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                    player.sendMessage(ChatColor.RED + injector.getString(player_language, "wrong_password") + ".");
                } else {
                    String last_game = login_info[1];
                    String token = login_info[2];
                    if(!last_game.equals("registrandose")) {
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                        player.sendMessage(ChatColor.AQUA + injector.getString(player_language, "login_redirect") + ": " + ChatColor.YELLOW + last_game);
                        System.out.println(token);
                        RedisConnection.addString(player.getName() + ".minecraft_token", token);
                        ServerRedirector.redirect(last_game, player);
                    } else {
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 2f);
                        player.sendMessage(ChatColor.AQUA + injector.getString(player_language, "register_redirect") + ".");
                        BungeeMessaging.sendStaffRequest(player);
                        System.out.println(token);
                        RedisConnection.addString(player.getName() + ".minecraft_token", token);
                        ServerRedirector.redirect("main_lobby", player);
                    }
                }
            }
        } else if(args.length() > 1) {
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);
            player.sendMessage(ChatColor.RED + injector.getString(player_language, "password_spaces") + ".");
        }
    }
}