package net.seocraft.commons.bukkit.commands;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.extended_events.GroupsUpdateEvent;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.bukkit.requests.user.JoinData;
import net.seocraft.api.bukkit.requests.user.RemoveGroup;
import net.seocraft.commons.bukkit.command_framework.Command;
import net.seocraft.commons.bukkit.command_framework.CommandArgs;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RemoveGroupCommand {

    @Command(name = "removegroup", aliases = {"rg", "remg"}, description = "Comando para remover un grupo al usuario", usage = "/removegroup <usuario> <grupo>", permission = "util.admin.removegroup", inGameOnly = false)
    public void onCommand(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (args.length() == 2) {
            String[] change_event = RemoveGroup.request(args.getArgs()[0], args.getArgs()[1], BukkitAPI.bukkit_users.get(args.getPlayer()).getToken());
            if (change_event[0].equals("true")) {
                Player player = Bukkit.getPlayer(args.getArgs()[0]);
                args.getSender().sendMessage(ChatColor.GREEN + injector.getString(player_language, "command_group_modify_remove")
                        .replace("[0]", ChatColor.AQUA + args.getArgs()[1] + ChatColor.GREEN)
                        .replace("[1]", ChatColor.AQUA + args.getArgs()[0]) + ChatColor.GREEN);
                if (player == null) return;
                BukkitUser user = JoinData.getPlayerUserObject(player.getName(), player.getAddress().toString().split(":")[0].replace("/", ""));
                GroupsUpdateEvent event = new GroupsUpdateEvent(player, user.getMain_group(), user.getSecondary_groups());
                Bukkit.getServer().getPluginManager().callEvent(event);
                BukkitAPI.bukkit_users.put(player, user);
            } else if(change_event[0].equals("false")) {
                args.getSender().sendMessage(ChatColor.RED + change_event[1]);
            }
        } else {
            args.getSender().sendMessage(ChatColor.RED + "Uso correcto: /removegroup <usuario> <grupo>");
        }
    }
}