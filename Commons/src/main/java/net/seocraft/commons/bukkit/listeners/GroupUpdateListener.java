package net.seocraft.commons.bukkit.listeners;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.api.bukkit.extended_events.GroupsUpdateEvent;
import net.seocraft.commons.bukkit.users.NametagChanger;
import net.seocraft.commons.bukkit.users.PermissionsManifest;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GroupUpdateListener implements Listener {

    @EventHandler
    public void onGroupUpdate(GroupsUpdateEvent event) {
        event.getPlayer().sendMessage(event.getMain_group().getColor());
        String player_language = BukkitAPI.bukkit_users.get(event.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        PermissionsManifest.setupPermissions(event.getPlayer());
        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 1f, 2f);
        NametagChanger.changePlayerName(event.getPlayer(), "", NametagChanger.TeamAction.CREATE, event.getMain_group());
        if (event.getMain_group().getName().equals("usuario")) {
            event.getPlayer().setDisplayName(GetAPIColor.getColor(event.getMain_group().getColor()) + event.getMain_group().getSymbol() + event.getPlayer().getName());
        } else {
            event.getPlayer().setDisplayName(GetAPIColor.getColor(event.getMain_group().getColor()) + event.getMain_group().getSymbol() + " " + event.getPlayer().getName());
        }
        if (event.getMain_group().getPriority() == 900) event.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + injector.getString(player_language, "group_user")
                .replace("[0]", GetAPIColor.getColor(event.getMain_group().getColor())
                        + "" + ChatColor.BOLD + event.getMain_group().getName().toUpperCase()) + ChatColor.RED + "" + ChatColor.BOLD);
        if (event.getMain_group().getPriority() == 800) event.getPlayer().sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + injector.getString(player_language, "group_vip").replace("[0]", GetAPIColor.getColor(event.getMain_group().getColor())
                + "" + ChatColor.BOLD + event.getMain_group().getName().toUpperCase() + ChatColor.GREEN + "" + ChatColor.BOLD));
        if (event.getMain_group().getPriority() == 700) event.getPlayer().sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + injector.getString(player_language, "group_media").replace("[0]", GetAPIColor.getColor(event.getMain_group().getColor())
                + "" + ChatColor.BOLD + event.getMain_group().getName().toUpperCase() + ChatColor.GREEN + "" + ChatColor.BOLD) + ChatColor.AQUA + "" +
                ChatColor.BOLD +" https://seocraft.net/media-info");
        if (event.getMain_group().getPriority() == 600) event.getPlayer().sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + injector.getString(player_language, "group_staff_new").replace("[0]", GetAPIColor.getColor(event.getMain_group().getColor())
                + "" + ChatColor.BOLD + event.getMain_group().getName().toUpperCase() + ChatColor.GREEN + "" + ChatColor.BOLD));
        if (event.getMain_group().getPriority() == 500 || event.getMain_group().getPriority() == 400 || event.getMain_group().getPriority() == 300) event.getPlayer().sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + injector.getString(player_language, "group_staff_promote").replace("[0]", GetAPIColor.getColor(event.getMain_group().getColor())
                + "" + ChatColor.BOLD + event.getMain_group().getName().toUpperCase() + ChatColor.GREEN + "" + ChatColor.BOLD));
        if (event.getMain_group().getPriority() == 200) event.getPlayer().sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + injector.getString(player_language, "group_manager"));
    }
}