package net.seocraft.commons.bukkit.commands;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.bukkit.packets.CustomSkull;
import net.seocraft.api.bukkit.packets.NBTHandler;
import net.seocraft.api.bukkit.packets.SignGui;
import net.seocraft.api.bukkit.requests.friends.*;
import net.seocraft.api.shared.models.Group;
import net.seocraft.commons.bukkit.command_framework.Command;
import net.seocraft.commons.bukkit.command_framework.CommandArgs;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.api.bukkit.requests.user.ProfileRequest;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.shared.commons.TimestampAgo;
import net.seocraft.api.shared.commons.UserSort;
import net.seocraft.api.shared.models.Friend;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.commons.bukkit.CommonsBukkit;
import net.seocraft.commons.bukkit.utils.CustomHeads;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

public class ProfileCommand implements Listener {

    private CommonsBukkit plugin = CommonsBukkit.getPlugin(CommonsBukkit.class);

    @Command(name = "perfil", aliases = {"profile", "profil"}, description = "Comando utilizado para ver el perfil de un jugador.", usage = "/perfil <jugador>", inGameOnly = true, permission = "commons.commands.user")
    public void onCommand(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (!BukkitAPI.getInstance().getConfig().getString("api.type").equals("LOBBY")) {
            args.getSender().sendMessage(ChatColor.RED + injector.getString(player_language, "profile_not_allowed"));
        } else {
            if (args.length() >= 2) {
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                args.getSender().sendMessage(ChatColor.RED + injector.getString(player_language, "correct_usage") + ": /perfil <" + injector.getString(player_language, "user_placeholder") + ">");
                return;
            }
            if (args.length() == 0 || args.getArgs()[0].equalsIgnoreCase(args.getPlayer().getName())) {
                own_gui(args.getPlayer(), player_language);
                return;
            }
            if (plugin.disguise.containsKey(args.getPlayer()) && args.getArgs()[0].equalsIgnoreCase(plugin.disguise.get(args.getPlayer()).getDisguise())) {
                own_gui(args.getPlayer(), player_language);
                return;
            }
            if (args.length() == 1)  {
                other_gui(args.getPlayer(), args.getArgs()[0], player_language);
                return;
            }
        }
    }

    private void own_gui(Player user, String language) {
        // -- Inventory init -- //
        TranslatableComponent injector = new TranslatableComponent();
        Inventory menu = Bukkit.getServer().createInventory(null, 54, ChatColor.DARK_GRAY + injector.getString(language, "profile_main_title"));
        String query = ProfileRequest.request(user.getName(), null, BukkitAPI.bukkit_users.get(user).getToken());
        if (ProfileRequest.queryOptions(query)[0].equals("false")) {
            user.sendMessage(ChatColor.RED + injector.getString(BukkitAPI.bukkit_users.get(user).getLanguage(), JSONDeserializer.deserialize(query).get("message").getAsString()));
        } else {
            BukkitUser user_info = ProfileRequest.encodeUser(query, "own");
            String user_status;
            if (user_info.getLast_seen().equals("conectado")) {
                user_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
            } else {
                user_status = ChatColor.AQUA + TimestampAgo.convert(user_info.getLast_seen(), language);
            }
            // -- Itemstack defines -- //
            ItemStack separator = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 1);
            ItemMeta separator_meta = separator.getItemMeta();
            separator_meta.setDisplayName(" ");
            separator.setItemMeta(separator_meta);
            ItemStack player_skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta skull_meta = (SkullMeta) player_skull.getItemMeta();
            skull_meta.setOwner(user_info.getSkin());
            if (!user_info.getMain_group().getName().equals("usuario")) {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user_info.getMain_group().getSymbol() + " " + user.getName());
            } else {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user.getName());
            }
            ArrayList<String> skull_lore = new ArrayList<>();
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
            if (user_info.getDisguised()) {
                Group disguise_group = ProfileRequest.encodeDisguise(query, "own");
                String disguise_name = JSONDeserializer.deserialize(query).get("query_user").getAsJsonObject().getAsJsonPrimitive("disguised_actual").getAsString();
                if(!disguise_group.getName().equals("usuario")) {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_group.getSymbol() + " " + disguise_name);
                } else {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_name);
                }
            }
            skull_lore.add("");
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + user_status);
            skull_meta.setLore(skull_lore);
            player_skull.setItemMeta(skull_meta);
            ItemStack friends_skull = CustomHeads.profileFriends();
            ItemMeta friends_meta = friends_skull.getItemMeta();
            friends_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_friends"));
            friends_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_friends_info"), "%s", ChatColor.GRAY));
            friends_skull.setItemMeta(friends_meta);
            ItemStack parties_skull = CustomHeads.profileParties();
            ItemMeta parties_meta = parties_skull.getItemMeta();
            parties_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_parties"));
            parties_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_parties_info"), "%s", ChatColor.GRAY));
            parties_skull.setItemMeta(parties_meta);
            ItemStack recent_skull = CustomHeads.profileRecent();
            ItemMeta recent_meta = recent_skull.getItemMeta();
            recent_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_recent"));
            recent_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_recent_info"), "%s", ChatColor.GRAY));
            recent_skull.setItemMeta(recent_meta);
            // -- Slot set -- //
            for (int slot = 9; slot <= 17; slot++) {
                menu.setItem(slot, separator);
            }
            menu.setItem(2, player_skull);
            menu.setItem(3, friends_skull);
            menu.setItem(4, parties_skull);
            menu.setItem(5, recent_skull);
            user.openInventory(menu);
        }
    }

    private void other_gui(Player user, String target, String language) {
        // -- Inventory init -- //
        TranslatableComponent injector = new TranslatableComponent();
        String query = ProfileRequest.request(user.getName(), target, BukkitAPI.bukkit_users.get(user).getToken());
        if (ProfileRequest.queryOptions(query)[0].equals("false")) {
            user.playSound(user.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
            user.sendMessage(ChatColor.RED + injector.getString(BukkitAPI.bukkit_users.get(user).getLanguage(), JSONDeserializer.deserialize(query).get("message").getAsString()));
        } else {
            // -- User and target defines -- //
            BukkitUser user_info = ProfileRequest.encodeUser(query, "own");
            String user_status;
            if (user_info.getLast_seen().equals("conectado")) {
                user_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
            } else {
                user_status = ChatColor.AQUA + TimestampAgo.convert(user_info.getLast_seen(), language);
            }
            BukkitUser target_info = ProfileRequest.encodeUser(query, "other");
            @Nullable Group target_disguise = null;
            @Nullable String target_disguise_name = null;
            if (target_info.getDisguised()) {
                target_disguise = ProfileRequest.encodeDisguise(query, "other");
                target_disguise_name = JSONDeserializer.deserialize(query).get("query_target").getAsJsonObject().getAsJsonPrimitive("disguised_actual").getAsString();
            }
            String target_status;
            if (target_info.getLast_seen().equals("conectado")) {
                target_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
            } else {
                target_status = ChatColor.AQUA + TimestampAgo.convert(target_info.getLast_seen(), language);
            }
            Inventory menu;
            if (!target_info.getDisguised()) {
                menu = Bukkit.getServer().createInventory(null, 54, ChatColor.DARK_GRAY + target_info.getUsername());
            } else {
                if (!ProfileRequest.queryOptions(query)[1].equals("true")) {
                    menu = Bukkit.getServer().createInventory(null, 54, ChatColor.DARK_GRAY + target_info.getUsername());
                } else {
                    if (!user.hasPermission("commons.staff.disguise.bypass")) {
                        menu = Bukkit.getServer().createInventory(null, 54, ChatColor.DARK_GRAY + target_disguise_name);
                    } else {
                        menu = Bukkit.getServer().createInventory(null, 54, ChatColor.DARK_GRAY + target_info.getUsername());
                    }
                }
            }
            // -- Players itemstack defines -- //
            ItemStack separator = new ItemStack(Material.STAINED_GLASS_PANE, 1);
            ItemMeta separator_meta = separator.getItemMeta();
            separator_meta.setDisplayName(" ");
            separator.setItemMeta(separator_meta);
            ItemStack player_raw = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            ItemStack player_skull = NBTHandler.addString(player_raw, "own_profile", "own_skull");
            SkullMeta skull_meta = (SkullMeta) player_skull.getItemMeta();
            skull_meta.setOwner(user_info.getSkin());
            if (!user_info.getMain_group().getName().equals("usuario")) {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user_info.getMain_group().getSymbol() + " " + user.getName());
            } else {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user.getName());
            }
            ArrayList<String> skull_lore = new ArrayList<>();
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
            if (user_info.getDisguised()) {
                Group disguise_group = ProfileRequest.encodeDisguise(query, "own");
                String disguise_name = JSONDeserializer.deserialize(query).get("query_user").getAsJsonObject().getAsJsonPrimitive("disguised_actual").getAsString();
                if(!disguise_group.getName().equals("usuario")) {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_group.getSymbol() + " " + disguise_name);
                } else {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_name);
                }
            }
            skull_lore.add("");
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + user_status);
            skull_meta.setLore(skull_lore);
            player_skull.setItemMeta(skull_meta);
            // -------------------------------------------------------------- //
            ItemStack target_skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta target_skull_meta = (SkullMeta) target_skull.getItemMeta();
            target_skull_meta.setOwner(target_info.getSkin());

            if (!target_info.getDisguised()) {
                if(!target_info.getMain_group().getName().equals("usuario")) {
                    target_skull_meta.setDisplayName(GetAPIColor.getColor(target_info.getMain_group().getColor()) + target_info.getMain_group().getSymbol() + " " + target_info.getUsername());
                } else {
                    target_skull_meta.setDisplayName(GetAPIColor.getColor(target_info.getMain_group().getColor()) + target_info.getUsername());
                }
            } else {
                if (!ProfileRequest.queryOptions(query)[1].equals("true")) {
                    if(!target_info.getMain_group().getName().equals("usuario")) {
                        target_skull_meta.setDisplayName(GetAPIColor.getColor(target_info.getMain_group().getColor()) + target_info.getMain_group().getSymbol() + " " + target_info.getUsername());
                    } else {
                        target_skull_meta.setDisplayName(GetAPIColor.getColor(target_info.getMain_group().getColor()) + target_info.getUsername());
                    }
                } else {
                    if (!user.hasPermission("commons.staff.disguise.bypass")) {
                        if(!target_disguise.getName().equals("usuario")) {
                            target_skull_meta.setDisplayName(GetAPIColor.getColor(target_disguise.getColor()) + target_disguise.getSymbol() + " " + target_disguise_name);
                        } else {
                            target_skull_meta.setDisplayName(GetAPIColor.getColor(target_disguise.getColor()) + target_disguise_name);
                        }
                    } else {
                        if(!target_info.getMain_group().getName().equals("usuario")) {
                            target_skull_meta.setDisplayName(GetAPIColor.getColor(target_info.getMain_group().getColor()) + target_info.getMain_group().getSymbol() + " " + target_info.getUsername());
                        } else {
                            target_skull_meta.setDisplayName(GetAPIColor.getColor(target_info.getMain_group().getColor()) + target_info.getUsername());
                        }
                    }
                }
            }
            ArrayList<String> target_skull_lore = new ArrayList<>();
            target_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
            target_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
            if (target_info.getDisguised()) {
                if (user.hasPermission("commons.staff.disguise.bypass")) {
                    if(!target_disguise.getName().equals("usuario")) {
                        target_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(target_disguise.getColor()) + target_disguise.getSymbol() + " " + target_disguise_name);
                    } else {
                        target_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(target_disguise.getColor()) + target_disguise_name);
                    }
                }
            }
            target_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + target_status);
            target_skull_lore.add("");
            target_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_shows_of"));
            if(!target_info.getDisguised()) {
                target_skull_lore.add(ChatColor.GRAY + target_info.getUsername() + ".");
            } else {
                if (!ProfileRequest.queryOptions(query)[1].equals("true")) {
                    target_skull_lore.add(ChatColor.GRAY + target_info.getUsername() + ".");
                } else {
                    if (!user.hasPermission("commons.staff.disguise.bypass")) {
                        target_skull_lore.add(ChatColor.GRAY + target_disguise_name + ".");
                    } else {
                        target_skull_lore.add(ChatColor.GRAY + target_info.getUsername() + ".");
                    }
                }
            }
            target_skull_meta.setLore(target_skull_lore);
            target_skull.setItemMeta(target_skull_meta);
            // -- GUI itemstack defines -- //
            ItemStack friends_skull = CustomHeads.profileFriends();
            ItemMeta friends_meta = friends_skull.getItemMeta();
            friends_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_friends"));
            friends_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_friends_info"), "%s", ChatColor.GRAY));
            friends_skull.setItemMeta(friends_meta);
            ItemStack parties_skull = CustomHeads.profileParties();
            ItemMeta parties_meta = parties_skull.getItemMeta();
            parties_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_parties"));
            parties_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_parties_info"), "%s", ChatColor.GRAY));
            parties_skull.setItemMeta(parties_meta);
            ItemStack recent_skull = CustomHeads.profileRecent();
            ItemMeta recent_meta = recent_skull.getItemMeta();
            recent_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_recent"));
            recent_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_recent_info"), "%s", ChatColor.GRAY));
            recent_skull.setItemMeta(recent_meta);
            // -- Slot set -- //
            for (int slot = 9; slot <= 17; slot++) {
                menu.setItem(slot, separator);
            }
            menu.setItem(0, target_skull);
            menu.setItem(2, player_skull);
            menu.setItem(3, friends_skull);
            menu.setItem(4, parties_skull);
            menu.setItem(5, recent_skull);
            user.openInventory(menu);
        }
    }

    private void friends_gui(Player user, String page) {
        TranslatableComponent injector = new TranslatableComponent();
        String language = BukkitAPI.bukkit_users.get(user).getLanguage();
        Inventory menu = Bukkit.createInventory(null, 54, injector.getString(language, "profile_friends"));
        String query = ProfileRequest.request(user.getName(), null, BukkitAPI.bukkit_users.get(user).getToken());
        if (ProfileRequest.queryOptions(query)[0].equals("false")) {
            user.sendMessage(ChatColor.RED + injector.getString(BukkitAPI.bukkit_users.get(user).getLanguage(), JSONDeserializer.deserialize(query).get("message").getAsString()));
        } else {
            BukkitUser user_info = ProfileRequest.encodeUser(query, "own");
            String[] sort = GetSorted.request(BukkitAPI.bukkit_users.get(user).getToken());
            String user_status;
            if (user_info.getLast_seen().equals("conectado")) {
                user_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
            } else {
                user_status = ChatColor.AQUA + TimestampAgo.convert(user_info.getLast_seen(), language);
            }
            // -- Itemstack defines -- //
            ItemStack separator = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 2);
            ItemMeta separator_meta = separator.getItemMeta();
            separator_meta.setDisplayName(" ");
            separator.setItemMeta(separator_meta);
            ItemStack player_raw = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            ItemStack player_skull = NBTHandler.addString(player_raw, "own_profile", "own_skull");
            SkullMeta skull_meta = (SkullMeta) player_skull.getItemMeta();
            skull_meta.setOwner(user_info.getSkin());
            if (!user_info.getMain_group().getName().equals("usuario")) {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user_info.getMain_group().getSymbol() + " " + user.getName());
            } else {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user.getName());
            }
            ArrayList<String> skull_lore = new ArrayList<>();
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
            if (user_info.getDisguised()) {
                Group disguise_group = ProfileRequest.encodeDisguise(query, "own");
                String disguise_name = JSONDeserializer.deserialize(query).get("query_user").getAsJsonObject().getAsJsonPrimitive("disguised_actual").getAsString();
                if(!disguise_group.getName().equals("usuario")) {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_group.getSymbol() + " " + disguise_name);
                } else {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_name);
                }
            }
            skull_lore.add("");
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + user_status);
            skull_meta.setLore(skull_lore);
            player_skull.setItemMeta(skull_meta);
            ItemStack friends_skull = CustomSkull.getSkull("http://textures.minecraft.net/texture/11827e965da3f9d8ada413335e4aa6a0718f38725e74987ae3036cb925d6cc63");
            ItemMeta friends_meta = friends_skull.getItemMeta();
            friends_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_friends"));
            friends_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_friends_info"), "%s", ChatColor.GRAY));
            friends_skull.setItemMeta(friends_meta);
            ItemStack parties_skull = CustomHeads.profileParties();
            ItemMeta parties_meta = parties_skull.getItemMeta();
            parties_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_parties"));
            parties_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_parties_info"), "%s", ChatColor.GRAY));
            parties_skull.setItemMeta(parties_meta);
            ItemStack recent_skull = CustomHeads.profileRecent();
            ItemMeta recent_meta = recent_skull.getItemMeta();
            recent_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_recent"));
            recent_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_recent_info"), "%s", ChatColor.GRAY));
            recent_skull.setItemMeta(recent_meta);
            String friends_query = FriendsList.request(BukkitAPI.bukkit_users.get(user).getToken(), page, sort[0]);
            @Nullable JsonArray friends;
            try {
                friends = JSONDeserializer.deserialize(friends_query).get("friends").getAsJsonArray();
            } catch (Exception ex) {
                friends = null;
            }
            Integer total_pages = JSONDeserializer.deserialize(friends_query).get("total_pages").getAsInt();
            Integer actual_page = JSONDeserializer.deserialize(friends_query).get("actual_page").getAsInt();
            List<Friend> friendList = new ArrayList<>();
            if (friends != null) {
                for (JsonElement friend_element : friends) {
                    Gson gson = new Gson();
                    JsonObject friend_object = friend_element.getAsJsonObject();
                    String friend_username = friend_object.get("username").getAsString();
                    Integer friend_level = friend_object.get("level").getAsInt();
                    String friend_main_group_string = JSONDeserializer.jsonString(friend_object.get("real_group").getAsJsonObject());
                    Group friend_main_group = gson.fromJson(friend_main_group_string, Group.class);
                    String friend_last_online = friend_object.get("last_online").getAsString();
                    Boolean friend_disguised = friend_object.get("disguised").getAsBoolean();
                    String friend_skin = friend_object.get("skin").getAsString();
                    String friend_game = friend_object.get("last_game").getAsString().toLowerCase();
                    @Nullable String disguised_actual = null;
                    @Nullable Group disguised_group = null;
                    if (friend_disguised) {
                        disguised_actual = friend_object.get("disguised_actual").getAsString();
                        String disguised_group_string = JSONDeserializer.jsonString(friend_object.get("disguise_group").getAsJsonObject());
                        disguised_group = gson.fromJson(disguised_group_string, Group.class);
                    }
                    Friend friend  = new Friend(friend_username, friend_level, friend_main_group, friend_disguised, friend_skin, friend_last_online, friend_game, disguised_actual, disguised_group);
                    friendList.add(friend);
                }
                List<Friend> sorted_array = UserSort.friend_sort(friendList, sort[0], sort[1]);
                Friend[] friendArray = new Friend[sorted_array.size()];
                friendArray = sorted_array.toArray(friendArray);
                for (int i = 27; i < 27 + friendList.size(); i++) {
                    Friend friend = friendArray[i - 27];
                    ItemStack friend_skull_raw = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
                    ItemStack friend_skull = NBTHandler.addString(friend_skull_raw, "friend_profile", friend.getUsername());
                    SkullMeta friend_skull_meta = (SkullMeta) friend_skull.getItemMeta();
                    friend_skull_meta.setOwner(friend.getSkin());
                    ArrayList<String> friend_skull_lore = new ArrayList<>();
                    friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
                    friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
                    friend_skull_lore.add(" ");
                    if (friend.getDisguised()) {
                        if(!friend.getDisguise_group().getName().equals("usuario")) {
                            friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getGroup().getSymbol() + " " + friend.getUsername());
                            friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(friend.getDisguise_group().getColor()) + friend.getDisguise_group().getSymbol() + " " + friend.getDisguise_actual());
                        } else {
                            friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername());
                            friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(friend.getDisguise_group().getColor()) + friend.getDisguise_actual());
                        }
                    }
                    if(!friend.getGroup().getName().equals("usuario")) {
                        friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getGroup().getSymbol() + " " + friend.getUsername());
                    } else {
                        friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername());
                    }
                    String friend_status;
                    if (friend.getLast_online().equals("conectado")) {
                        friend_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
                    } else {
                        friend_status = ChatColor.AQUA + TimestampAgo.convert(friend.getLast_online(), language);
                    }
                    friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + friend_status);
                    friend_skull_meta.setLore(friend_skull_lore);
                    friend_skull.setItemMeta(friend_skull_meta);
                    menu.setItem(i, friend_skull);
                }
                if (total_pages != 1) {
                    if (actual_page != 1) {
                        ItemStack previous_page_raw = CustomHeads.prevPage();
                        ItemStack previous_page = NBTHandler.addString(previous_page_raw, "pagination_accessor", "" + (actual_page - 1));
                        ItemMeta previus_page_meta = previous_page.getItemMeta();
                        previus_page_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_page_previous").replace("[0]", ChatColor.AQUA + "" + (actual_page - 1)));
                        previous_page.setItemMeta(previus_page_meta);
                        menu.setItem(21, previous_page);
                    }
                    ItemStack page_actual = CustomHeads.actualPage();
                    ItemMeta page_actual_meta = page_actual.getItemMeta();
                    page_actual_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_page_actual").replace("[0]", ChatColor.AQUA + actual_page.toString()));
                    page_actual.setItemMeta(page_actual_meta);
                    menu.setItem(22, page_actual);
                    if (!actual_page.equals(total_pages)) {
                        ItemStack next_page_raw = CustomHeads.nextPage();
                        ItemStack next_page = NBTHandler.addString(next_page_raw, "pagination_accessor", "" + (actual_page + 1));
                        ItemMeta next_page_meta = next_page.getItemMeta();
                        next_page_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_page_next").replace("[0]", ChatColor.AQUA + "" + (actual_page + 1)));
                        next_page.setItemMeta(next_page_meta);
                        menu.setItem(23, next_page);
                    }
                }
                ItemStack change_sort_raw = new ItemStack(Material.HOPPER);
                ItemStack change_sort = NBTHandler.addString(change_sort_raw, "friends_sort", actual_page.toString());
                ItemMeta change_sort_meta = change_sort.getItemMeta();
                change_sort_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_friends_sort_change_item"));
                ArrayList<String> change_sort_lore = new ArrayList<>();
                if (sort[0].equals("3")) {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_current") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_last_online"));
                } else if (sort[0].equals("2")) {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_current") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_alphabetical"));
                } else {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_current") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_default"));
                }
                if (!sort[1].equals("true")) {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_reversion") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_normal"));
                } else {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_reversion") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_reversed"));
                }
                change_sort_lore.add("");
                List<String> default_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_default_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.AQUA + injector.getString(language, "commons_friends_sort_default") + ChatColor.GRAY + ": " + default_info.get(0));
                default_info.remove(0);
                for (String piece : default_info) {
                    change_sort_lore.add(piece);
                }
                List<String> alphabetical_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_alphabetical_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.AQUA + injector.getString(language, "commons_friends_sort_alphabetical") + ChatColor.GRAY + ": " + alphabetical_info.get(0));
                alphabetical_info.remove(0);
                for (String piece : alphabetical_info) {
                    change_sort_lore.add(piece);
                }
                List<String> last_online_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_last_online_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.AQUA + injector.getString(language, "commons_friends_sort_last_online") + ChatColor.GRAY + ": " + last_online_info.get(0));
                last_online_info.remove(0);
                for (String piece : last_online_info) {
                    change_sort_lore.add(piece);
                }
                change_sort_lore.add("");
                List<String> left_click_sort_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_left_click_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.YELLOW + injector.getString(language, "left_click_placeholder").toUpperCase() + " " + ChatColor.GRAY + left_click_sort_info.get(0).toLowerCase());
                left_click_sort_info.remove(0);
                for (String piece : left_click_sort_info) {
                    change_sort_lore.add(piece);
                }
                change_sort_lore.add("");
                List<String> right_click_sort_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_right_click_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.YELLOW + injector.getString(language, "right_click_placeholder").toUpperCase() + " " + ChatColor.GRAY + right_click_sort_info.get(0).toLowerCase());
                right_click_sort_info.remove(0);
                for (String piece : right_click_sort_info) {
                    change_sort_lore.add(piece);
                }
                change_sort_meta.setLore(change_sort_lore);
                change_sort.setItemMeta(change_sort_meta);
                menu.setItem(25, change_sort);
                ItemStack search_player_raw = new ItemStack(Material.SIGN);
                ItemStack search_player = NBTHandler.addString(search_player_raw, "accessor", "FRIENDS_SEARCH");
                ItemMeta search_player_meta = search_player.getItemMeta();
                search_player_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_friends_search_menu"));
                search_player.setItemMeta(search_player_meta);
                menu.setItem(26, search_player);
                ItemStack remove_player_raw = new ItemStack(Material.SIGN);
                ItemStack remove_player = NBTHandler.addString(remove_player_raw, "accessor", "FRIENDS_REMOVE");
                ItemMeta remove_player_meta = remove_player.getItemMeta();
                remove_player_meta.setDisplayName(ChatColor.RED + injector.getString(language, "commons_friends_remove_menu"));
                ArrayList<String> remove_player_lore = new ArrayList<>();
                List<String> remove_player_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_remove_info"), "%s", ChatColor.GRAY);
                for (String piece : remove_player_info) {
                    remove_player_lore.add(ChatColor.GRAY + piece);
                }
                remove_player_meta.setLore(remove_player_lore);
                remove_player.setItemMeta(remove_player_meta);
                menu.setItem(19, remove_player);
            } else {
                ItemStack empty = new ItemStack(Material.GLASS_BOTTLE);
                ItemMeta empty_meta = empty.getItemMeta();
                empty_meta.setDisplayName(ChatColor.RED + injector.getString(BukkitAPI.bukkit_users.get(user).getLanguage(), "profile_friends_empty"));
                empty.setItemMeta(empty_meta);
                menu.setItem(31, empty);
            }
            ItemStack add_player_raw = new ItemStack(Material.BOOK_AND_QUILL);
            ItemStack add_player = NBTHandler.addString(add_player_raw, "accessor", "FRIENDS_ADD");
            ItemMeta add_player_meta = add_player.getItemMeta();
            add_player_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_friends_add_menu"));
            ArrayList<String> add_player_lore = new ArrayList<>();
            List<String> add_player_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_add_info"), "%s", ChatColor.GRAY);
            for (String piece : add_player_info) {
                add_player_lore.add(ChatColor.GRAY + piece);
            }
            add_player_meta.setLore(add_player_lore);
            add_player.setItemMeta(add_player_meta);
            for (int slot = 9; slot <= 17; slot++) {
                menu.setItem(slot, separator);
            }
            menu.setItem(2, player_skull);
            menu.setItem(3, friends_skull);
            menu.setItem(4, parties_skull);
            menu.setItem(5, recent_skull);
            menu.setItem(18, add_player);
            user.openInventory(menu);
        }
    }

    public void friends_search(Player user, String page, String search) {
        TranslatableComponent injector = new TranslatableComponent();
        String language = BukkitAPI.bukkit_users.get(user).getLanguage();
        Inventory menu = Bukkit.createInventory(null, 54, injector.getString(language, "profile_friends"));
        String query = ProfileRequest.request(user.getName(), null, BukkitAPI.bukkit_users.get(user).getToken());
        if (ProfileRequest.queryOptions(query)[0].equals("false")) {
            user.sendMessage(ChatColor.RED + injector.getString(BukkitAPI.bukkit_users.get(user).getLanguage(), JSONDeserializer.deserialize(query).get("message").getAsString()));
        } else {
            BukkitUser user_info = ProfileRequest.encodeUser(query, "own");
            String[] sort = GetSorted.request(BukkitAPI.bukkit_users.get(user).getToken());
            String user_status;
            if (user_info.getLast_seen().equals("conectado")) {
                user_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
            } else {
                user_status = ChatColor.AQUA + TimestampAgo.convert(user_info.getLast_seen(), language);
            }
            // -- Itemstack defines -- //
            ItemStack separator = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 2);
            ItemMeta separator_meta = separator.getItemMeta();
            separator_meta.setDisplayName(" ");
            separator.setItemMeta(separator_meta);
            ItemStack player_raw = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            ItemStack player_skull = NBTHandler.addString(player_raw, "own_profile", "own_skull");
            SkullMeta skull_meta = (SkullMeta) player_skull.getItemMeta();
            skull_meta.setOwner(user_info.getSkin());
            if (!user_info.getMain_group().getName().equals("usuario")) {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user_info.getMain_group().getSymbol() + " " + user.getName());
            } else {
                skull_meta.setDisplayName(GetAPIColor.getColor(user_info.getMain_group().getColor()) + user.getName());
            }
            ArrayList<String> skull_lore = new ArrayList<>();
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
            if (user_info.getDisguised()) {
                Group disguise_group = ProfileRequest.encodeDisguise(query, "own");
                String disguise_name = JSONDeserializer.deserialize(query).get("query_user").getAsJsonObject().getAsJsonPrimitive("disguised_actual").getAsString();
                if(!disguise_group.getName().equals("usuario")) {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_group.getSymbol() + " " + disguise_name);
                } else {
                    skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_name);
                }
            }
            skull_lore.add("");
            skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + user_status);
            skull_meta.setLore(skull_lore);
            player_skull.setItemMeta(skull_meta);
            ItemStack friends_skull = CustomSkull.getSkull("http://textures.minecraft.net/texture/11827e965da3f9d8ada413335e4aa6a0718f38725e74987ae3036cb925d6cc63");
            ItemMeta friends_meta = friends_skull.getItemMeta();
            friends_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_friends"));
            friends_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_friends_info"), "%s", ChatColor.GRAY));
            friends_skull.setItemMeta(friends_meta);
            ItemStack parties_skull = CustomHeads.profileParties();
            ItemMeta parties_meta = parties_skull.getItemMeta();
            parties_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_parties"));
            parties_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_parties_info"), "%s", ChatColor.GRAY));
            parties_skull.setItemMeta(parties_meta);
            ItemStack recent_skull = CustomHeads.profileRecent();
            ItemMeta recent_meta = recent_skull.getItemMeta();
            recent_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "profile_recent"));
            recent_meta.setLore(ChatProcessor.loreProcessor(injector.getString(language, "profile_recent_info"), "%s", ChatColor.GRAY));
            recent_skull.setItemMeta(recent_meta);
            String friends_query = FriendsSearch.request(BukkitAPI.bukkit_users.get(user).getToken(), page, sort[0], search);
            if (!JSONDeserializer.deserialize(friends_query).get("has_results").getAsString().equals("true")) {
                user.playSound(user.getLocation(), Sound.NOTE_BASS, 1f, 1f);
                user.sendMessage(injector.getString(language, "commons_friends_not_found"));
            } else {
                JsonArray friends = JSONDeserializer.deserialize(friends_query).get("search").getAsJsonArray();
                Integer total_pages = JSONDeserializer.deserialize(friends_query).get("total_pages").getAsInt();
                Integer actual_page = JSONDeserializer.deserialize(friends_query).get("actual_page").getAsInt();
                List<Friend> friendList = new ArrayList<>();
                for (JsonElement friend_element : friends) {
                    Gson gson = new Gson();
                    JsonObject friend_object = friend_element.getAsJsonObject();
                    String friend_username = friend_object.get("username").getAsString();
                    Integer friend_level = friend_object.get("level").getAsInt();
                    String friend_main_group_string = JSONDeserializer.jsonString(friend_object.get("real_group").getAsJsonObject());
                    Group friend_main_group = gson.fromJson(friend_main_group_string, Group.class);
                    String friend_last_online = friend_object.get("last_online").getAsString();
                    Boolean friend_disguised = friend_object.get("disguised").getAsBoolean();
                    String friend_skin = friend_object.get("skin").getAsString();
                    String friend_game = friend_object.get("last_game").getAsString().toLowerCase();
                    @Nullable String disguised_actual = null;
                    @Nullable Group disguised_group = null;
                    if (friend_disguised) {
                        disguised_actual = friend_object.get("disguised_actual").getAsString();
                        String disguised_group_string = JSONDeserializer.jsonString(friend_object.get("disguise_group").getAsJsonObject());
                        disguised_group = gson.fromJson(disguised_group_string, Group.class);
                    }
                    Friend friend  = new Friend(friend_username, friend_level, friend_main_group, friend_disguised, friend_skin, friend_last_online, friend_game, disguised_actual, disguised_group);
                    friendList.add(friend);
                }
                List<Friend> sorted_array = UserSort.friend_sort(friendList, sort[0], sort[1]);
                Friend[] friendArray = new Friend[sorted_array.size()];
                friendArray = sorted_array.toArray(friendArray);
                for (int i = 27; i < 27 + friendList.size(); i++) {
                    Friend friend = friendArray[i - 27];
                    ItemStack friend_skull_raw = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
                    ItemStack friend_skull = NBTHandler.addString(friend_skull_raw, "friend_profile", friend.getUsername());
                    SkullMeta friend_skull_meta = (SkullMeta) friend_skull.getItemMeta();
                    friend_skull_meta.setOwner(friend.getSkin());
                    ArrayList<String> friend_skull_lore = new ArrayList<>();
                    friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_level") + ": " + ChatColor.GOLD + user_info.getLevel());
                    friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_points") + ": " + ChatColor.YELLOW + user_info.getLevel());
                    friend_skull_lore.add(" ");
                    if (friend.getDisguised()) {
                        if(!friend.getDisguise_group().getName().equals("usuario")) {
                            friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getGroup().getSymbol() + " " + friend.getUsername());
                            friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(friend.getDisguise_group().getColor()) + friend.getDisguise_group().getSymbol() + " " + friend.getDisguise_actual());
                        } else {
                            friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername());
                            friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_disguise") + ": " + GetAPIColor.getColor(friend.getDisguise_group().getColor()) + friend.getDisguise_actual());
                        }
                    }
                    if(!friend.getGroup().getName().equals("usuario")) {
                        friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getGroup().getSymbol() + " " + friend.getUsername());
                    } else {
                        friend_skull_meta.setDisplayName(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername());
                    }
                    String friend_status;
                    if (friend.getLast_online().equals("conectado")) {
                        friend_status = ChatColor.GREEN + injector.getString(language, "profile_connected");
                    } else {
                        friend_status = ChatColor.AQUA + TimestampAgo.convert(friend.getLast_online(), language);
                    }
                    friend_skull_lore.add(ChatColor.GRAY + injector.getString(language, "profile_last_seen") + ": " + friend_status);
                    friend_skull_meta.setLore(friend_skull_lore);
                    friend_skull.setItemMeta(friend_skull_meta);
                    menu.setItem(i, friend_skull);
                }
                if (total_pages != 1) {
                    if (actual_page != 1) {
                        ItemStack previous_page_rawed = CustomHeads.prevPage();
                        ItemStack previous_page_raw = NBTHandler.addString(previous_page_rawed, "pagination_search_accessor", "" + (actual_page - 1));
                        ItemStack previous_page = NBTHandler.addString(previous_page_raw, "pagination_search_result", search);
                        ItemMeta previus_page_meta = previous_page.getItemMeta();
                        previus_page_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_page_previous").replace("[0]", ChatColor.AQUA + "" + (actual_page - 1)));
                        previous_page.setItemMeta(previus_page_meta);
                        menu.setItem(21, previous_page);
                    }
                    ItemStack page_actual = CustomHeads.actualPage();
                    ItemMeta page_actual_meta = page_actual.getItemMeta();
                    page_actual_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_page_actual").replace("[0]", ChatColor.AQUA + actual_page.toString()));
                    page_actual.setItemMeta(page_actual_meta);
                    menu.setItem(22, page_actual);
                    if (!actual_page.equals(total_pages)) {
                        ItemStack next_page_rawed = CustomHeads.nextPage();
                        ItemStack next_page_raw = NBTHandler.addString(next_page_rawed, "pagination_search_accessor", "" + (actual_page + 1));
                        ItemStack next_page = NBTHandler.addString(next_page_raw, "pagination_search_result", search);
                        ItemMeta next_page_meta = next_page.getItemMeta();
                        next_page_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_page_next").replace("[0]", ChatColor.AQUA + "" + (actual_page + 1)));
                        next_page.setItemMeta(next_page_meta);
                        menu.setItem(23, next_page);
                    }
                }
                ItemStack add_player_raw = new ItemStack(Material.BOOK_AND_QUILL);
                ItemStack add_player = NBTHandler.addString(add_player_raw, "accessor", "FRIENDS_ADD");
                ItemMeta add_player_meta = add_player.getItemMeta();
                add_player_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_friends_add_menu"));
                ArrayList<String> add_player_lore = new ArrayList<>();
                List<String> add_player_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_add_info"), "%s", ChatColor.GRAY);
                for (String piece : add_player_info) {
                    add_player_lore.add(ChatColor.GRAY + piece);
                }
                add_player_meta.setLore(add_player_lore);
                add_player.setItemMeta(add_player_meta);
                ItemStack remove_player_raw = new ItemStack(Material.BARRIER);
                ItemStack remove_player = NBTHandler.addString(remove_player_raw, "accessor", "FRIENDS_REMOVE");
                ItemMeta remove_player_meta = remove_player.getItemMeta();
                remove_player_meta.setDisplayName(ChatColor.RED + injector.getString(language, "commons_friends_remove_menu"));
                ArrayList<String> remove_player_lore = new ArrayList<>();
                List<String> remove_player_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_remove_info"), "%s", ChatColor.GRAY);
                for (String piece : remove_player_info) {
                    remove_player_lore.add(ChatColor.GRAY + piece);
                }
                remove_player_meta.setLore(remove_player_lore);
                remove_player.setItemMeta(remove_player_meta);
                ItemStack clear_search_raw = new ItemStack(Material.BARRIER);
                ItemStack clear_search = NBTHandler.addString(clear_search_raw, "accessor", "FRIENDS_SEARCH_CLEAR");
                ItemMeta clear_search_meta = clear_search.getItemMeta();
                clear_search_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_friends_clear_search_menu"));
                clear_search.setItemMeta(clear_search_meta);
                ItemStack change_sort_rawed = new ItemStack(Material.HOPPER);
                ItemStack change_sort_raw = NBTHandler.addString(change_sort_rawed, "friends_sort", actual_page.toString());
                ItemStack change_sort = NBTHandler.addString(change_sort_raw, "pagination_search_result", search);
                ItemMeta change_sort_meta = change_sort.getItemMeta();
                change_sort_meta.setDisplayName(ChatColor.GREEN + injector.getString(language, "commons_friends_sort_change_item"));
                ArrayList<String> change_sort_lore = new ArrayList<>();
                if (sort[0].equals("3")) {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_current") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_last_online"));
                } else if (sort[0].equals("2")) {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_current") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_alphabetical"));
                } else {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_current") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_default"));
                }
                if (!sort[1].equals("true")) {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_reversion") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_normal"));
                } else {
                    change_sort_lore.add(ChatColor.GRAY + injector.getString(language, "commons_friends_sort_reversion") + ": " + ChatColor.AQUA + injector.getString(language, "commons_friends_sort_reversed"));
                }
                change_sort_lore.add("");
                List<String> default_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_default_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.AQUA + injector.getString(language, "commons_friends_sort_default") + ChatColor.GRAY + ": " + default_info.get(0));
                default_info.remove(0);
                for (String piece : default_info) {
                    change_sort_lore.add(piece);
                }
                List<String> alphabetical_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_alphabetical_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.AQUA + injector.getString(language, "commons_friends_sort_alphabetical") + ChatColor.GRAY + ": " + alphabetical_info.get(0));
                alphabetical_info.remove(0);
                for (String piece : alphabetical_info) {
                    change_sort_lore.add(piece);
                }
                List<String> last_online_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_last_online_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.AQUA + injector.getString(language, "commons_friends_sort_last_online") + ChatColor.GRAY + ": " + last_online_info.get(0));
                last_online_info.remove(0);
                for (String piece : last_online_info) {
                    change_sort_lore.add(piece);
                }
                change_sort_lore.add("");
                List<String> left_click_sort_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_left_click_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.YELLOW + injector.getString(language, "left_click_placeholder").toUpperCase() + " " + ChatColor.GRAY + left_click_sort_info.get(0).toLowerCase());
                left_click_sort_info.remove(0);
                for (String piece : left_click_sort_info) {
                    change_sort_lore.add(piece);
                }
                change_sort_lore.add("");
                List<String> right_click_sort_info = ChatProcessor.loreProcessor(injector.getString(language, "commons_friends_sort_right_click_info"), "%s", ChatColor.GRAY);
                change_sort_lore.add(ChatColor.YELLOW + injector.getString(language, "right_click_placeholder").toUpperCase() + " " + ChatColor.GRAY + right_click_sort_info.get(0).toLowerCase());
                right_click_sort_info.remove(0);
                for (String piece : right_click_sort_info) {
                    change_sort_lore.add(piece);
                }
                change_sort_meta.setLore(change_sort_lore);
                change_sort.setItemMeta(change_sort_meta);
                for (int slot = 9; slot <= 17; slot++) {
                    menu.setItem(slot, separator);
                }
                menu.setItem(2, player_skull);
                menu.setItem(3, friends_skull);
                menu.setItem(4, parties_skull);
                menu.setItem(5, recent_skull);
                menu.setItem(18, add_player);
                menu.setItem(19, remove_player);
                menu.setItem(25, change_sort);
                menu.setItem(26, clear_search);
                user.openInventory(menu);
            }
        }
    }

    @EventHandler
    public void inventoryClick(InventoryClickEvent event) {
        HumanEntity entity = event.getWhoClicked();
        if (event.getCurrentItem()== null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
            return;
        }
        if((entity instanceof Player)) {
            Player player = (Player)entity;
            String player_language = BukkitAPI.bukkit_users.get(player).getLanguage();
            TranslatableComponent injector = new TranslatableComponent();
            ItemStack clicked = event.getCurrentItem();
            event.setCancelled(true);
            if (NBTHandler.hasString(clicked, "own_profile")) {
                own_gui(player, player_language);
                return;
            }
            if(NBTHandler.hasString(clicked, "friend_profile")) {
                other_gui(player, NBTHandler.getString(clicked, "friend_profile"),player_language);
            }
            // ---- Hotbar Accessors ---- //
            switch(NBTHandler.getString(clicked, "accessor")) {
                case "FRIENDS_ACCESSOR": {
                    friends_gui(player, "1");
                    break;
                }
                case "FRIENDS_ADD": {
                    player.closeInventory();
                    SignGui signGui = new SignGui(player, "", "^^^^^^", injector.getString(player_language, "commons_friends_player_top_sign"), injector.getString(player_language, "commons_friends_add_player_bottom_sign"), "friends_add");
                    signGui.open();
                    break;
                }
                case "FRIENDS_REMOVE": {
                    player.closeInventory();
                    SignGui signGui = new SignGui(player, "", "^^^^^^", injector.getString(player_language, "commons_friends_player_top_sign"), injector.getString(player_language, "commons_friends_remove_player_bottom_sign"), "friends_remove");
                    signGui.open();
                    break;
                }
                case "FRIENDS_SEARCH": {
                    player.closeInventory();
                    SignGui signGui = new SignGui(player, "", "^^^^^^", injector.getString(player_language, "commons_friends_player_top_sign"), injector.getString(player_language, "commons_friends_search_player_bottom_sign"), "friends_search");
                    signGui.open();
                    break;
                }
                case "FRIENDS_SEARCH_CLEAR": {
                    friends_gui(player, "1");
                }
                default: {
                    break;
                }
            }
            if(NBTHandler.hasString(clicked, "friends_sort")) {
                String[] sort = GetSorted.request(BukkitAPI.bukkit_users.get(player).getToken());
                if (event.getClick() == ClickType.LEFT) {
                    ToggleSort.request(BukkitAPI.bukkit_users.get(player).getToken(), Integer.valueOf(sort[0]));
                    friends_gui(player, NBTHandler.getString(clicked, "friends_sort"));
                } else {
                    ToggleReverse.request(BukkitAPI.bukkit_users.get(player).getToken());
                    friends_gui(player, NBTHandler.getString(clicked, "friends_sort"));
                }
            }
            if(NBTHandler.hasString(clicked, "friends_search_sort")) {
                String[] sort = GetSorted.request(BukkitAPI.bukkit_users.get(player).getToken());
                if (event.getClick() == ClickType.LEFT) {
                    ToggleSort.request(BukkitAPI.bukkit_users.get(player).getToken(), Integer.valueOf(sort[0]));
                    friends_search(player, NBTHandler.getString(clicked, "friends_search_sort"), NBTHandler.getString(clicked, "pagination_search_result"));
                } else {
                    ToggleReverse.request(BukkitAPI.bukkit_users.get(player).getToken());
                    friends_search(player, NBTHandler.getString(clicked, "friends_sort"), NBTHandler.getString(clicked, "pagination_search_result"));
                }
            }
            // ---- Friends Pagination ---- //
            if(NBTHandler.hasString(clicked, "pagination_accessor")) {
                friends_gui(player, NBTHandler.getString(clicked, "pagination_accessor"));
            }
            if(NBTHandler.hasString(clicked, "pagination_search_accessor")) {
                friends_search(player, NBTHandler.getString(clicked, "pagination_search_accessor"), NBTHandler.getString(clicked, "pagination_search_result"));
            }
        }
    }
}