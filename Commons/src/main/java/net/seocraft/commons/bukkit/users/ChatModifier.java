package net.seocraft.commons.bukkit.users;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.commons.bukkit.CommonsBukkit;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatModifier implements Listener {

    private String server_type = BukkitAPI.getInstance().getConfig().getString("api.type");
    private CommonsBukkit plugin = CommonsBukkit.getPlugin(CommonsBukkit.class);

    @EventHandler
    public void loadPrefix(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();
        if (e.isCancelled()) { return; }
        e.setCancelled(true);

        Integer nivel = BukkitAPI.bukkit_users.get(player).getLevel();
        String msg = e.getMessage();

        if (!BukkitAPI.bukkit_users.get(player).getDisguised()) {
            String group = BukkitAPI.bukkit_users.get(player).getMain_group().getName();
            String group_fixed = group.substring(0, 1).toUpperCase() + group.substring(1);
            ChatColor color = GetAPIColor.getColor(BukkitAPI.bukkit_users.get(player).getMain_group().getColor());
            if (BukkitAPI.bukkit_users.get(player).getMain_group().getName().equals("usuario")) {
                TextComponent chat = new TextComponent(ChatColor.GRAY + player.getDisplayName());
                TextComponent message = new TextComponent(ChatColor.GRAY + ": " + ChatColor.GRAY + msg);
                if(server_type.equals("LOBBY")) {
                    chat.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(player.getDisplayName()).append("\n" + ChatColor.GRAY + "Rango: " + color + group_fixed).append("\n" + ChatColor.GRAY + "Nivel: " + ChatColor.BLUE + nivel).append("\n").append("\n" + ChatColor.YELLOW + "¡Click aqui para ver el perfil de " + color + player.getName() + ChatColor.YELLOW + "!").create()));
                    chat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + player.getName()));
                }
                message.setColor(net.md_5.bungee.api.ChatColor.GRAY);
                Bukkit.spigot().broadcast(chat, message);
                return;
            } else if (!BukkitAPI.bukkit_users.get(player).getMain_group().getName().equals("usuario")) {
                String processed_message = ChatProcessor.lobbyChatProcessor(msg);
                TextComponent message = new TextComponent(ChatColor.WHITE + ": " + ChatColor.WHITE + processed_message);
                TextComponent chat = new TextComponent(player.getDisplayName());
                if(server_type.equals("LOBBY")) {
                    chat.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(player.getDisplayName()).append("\n" + ChatColor.GRAY + "Rango: " + color + group_fixed).append("\n" + ChatColor.GRAY + "Nivel: " + ChatColor.BLUE + nivel).append("\n").append("\n" + ChatColor.YELLOW + "¡Click aqui para ver el perfil de " + color + player.getName() + ChatColor.YELLOW + "!").create()));
                    chat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + player.getName()));
                }
                message.setColor(net.md_5.bungee.api.ChatColor.WHITE);
                Bukkit.spigot().broadcast(chat, message);
                return;
            }
        } else {
            String group = plugin.disguise.get(player).getGroup().getName();
            String group_fixed = group.substring(0, 1).toUpperCase() + group.substring(1);
            String symbol = plugin.disguise.get(player).getGroup().getSymbol();
            ChatColor color = GetAPIColor.getColor(plugin.disguise.get(player).getGroup().getColor());
            String real_group = BukkitAPI.bukkit_users.get(player).getMain_group().getName();
            String real_group_fixed = real_group.substring(0, 1).toUpperCase() + real_group.substring(1);
            String real_symbol = BukkitAPI.bukkit_users.get(player).getMain_group().getSymbol();
            ChatColor real_color = GetAPIColor.getColor(BukkitAPI.bukkit_users.get(player).getMain_group().getColor());
            if (plugin.disguise.get(player).getGroup().getName().equals("usuario")) {
                TextComponent message = new TextComponent(ChatColor.GRAY + ": " + ChatColor.GRAY + msg);
                message.setColor(net.md_5.bungee.api.ChatColor.GRAY);
                for (Player players : Bukkit.getOnlinePlayers()) {
                    if (!players.hasPermission("commons.staff.disguise.bypass")) {
                        TextComponent chat = new TextComponent(player.getDisplayName());
                        if(server_type.equals("LOBBY")) {
                            chat.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(color + plugin.disguise.get(player).getDisguise()).append("\n" + ChatColor.GRAY + "Rango: " + color + group_fixed).append("\n" + ChatColor.GRAY + "Nivel: " + ChatColor.BLUE + nivel).append("\n").append("\n" + ChatColor.YELLOW + "¡Click aqui para ver el perfil de " + color + plugin.disguise.get(player).getDisguise() + ChatColor.YELLOW + "!").create()));
                            chat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + player.getName()));
                        }
                        players.spigot().sendMessage(chat, message);
                    } else {
                        TextComponent chat = new TextComponent(ChatColor.GRAY + "[" + player.getName() + "] " + player.getDisplayName());
                        if(server_type.equals("LOBBY")) {
                            chat.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(real_color + real_symbol + " " + player.getName()).append("\n" + ChatColor.GRAY + "Rango: " + real_color + real_group_fixed).append("\n" + ChatColor.GRAY + "Nivel: " + ChatColor.BLUE + nivel).append("\n" + ChatColor.GRAY + "Disfráz: " + color + symbol + " " + plugin.disguise.get(player).getDisguise()).append("\n").append("\n" + ChatColor.YELLOW + "¡Click aqui para ver el perfil de " + real_color + player.getName() + ChatColor.YELLOW + "!").create()));
                            chat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + player.getName()));
                        }
                        players.spigot().sendMessage(chat, message);
                    }
                }
            } else if (!plugin.disguise.get(player).getGroup().getName().equals("usuario")) {
                TextComponent message = new TextComponent(ChatColor.WHITE + ": " + ChatColor.WHITE + msg);
                for (Player players : Bukkit.getOnlinePlayers()) {
                    if (!players.hasPermission("commons.staff.disguise.bypass")) {
                        TextComponent chat = new TextComponent(player.getDisplayName());
                        if(server_type.equals("LOBBY")) {
                            chat.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(color + symbol + " " + plugin.disguise.get(player).getDisguise()).append("\n" + ChatColor.GRAY + "Rango: " + color + group_fixed).append("\n" + ChatColor.GRAY + "Nivel: " + ChatColor.BLUE + nivel).append("\n").append("\n" + ChatColor.YELLOW + "¡Click aqui para ver el perfil de " + color + plugin.disguise.get(player).getDisguise() + ChatColor.YELLOW + "!").create()));
                            chat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + player.getName()));
                        }
                        players.spigot().sendMessage(chat, message);
                    } else {
                        TextComponent chat = new TextComponent(ChatColor.GRAY + "[" + player.getName() + "] " + player.getDisplayName());
                        if(server_type.equals("LOBBY")) {
                            chat.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(real_color + real_symbol + " " + player.getName()).append("\n" + ChatColor.GRAY + "Rango: " + real_color + real_group_fixed).append("\n" + ChatColor.GRAY + "Nivel: " + ChatColor.BLUE + nivel).append("\n" + ChatColor.GRAY + "Disfráz: " + color + symbol + " " + plugin.disguise.get(player).getDisguise()).append("\n").append("\n" + ChatColor.YELLOW + "¡Click aqui para ver el perfil de " + real_color + player.getName() + ChatColor.YELLOW + "!").create()));
                            chat.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + player.getName()));
                        }
                        players.spigot().sendMessage(chat, message);
                    }
                }
            }
        }
    }
}