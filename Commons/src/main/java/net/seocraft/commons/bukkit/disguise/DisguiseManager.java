package net.seocraft.commons.bukkit.disguise;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_8_R3.*;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.Disguise;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.api.bukkit.extended_events.DisguiseUpdateEvent;
import net.seocraft.commons.bukkit.CommonsBukkit;
import net.seocraft.commons.bukkit.users.NametagChanger;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import java.lang.reflect.Field;

public class DisguiseManager {

    private Disguise disguise;
    private Player receiver;

    public DisguiseManager(Disguise disguise, Player receiver) {
        this.disguise = disguise;
        this.receiver = receiver;
    }

    private void setGP(EntityLiving living, GameProfile profile) {
        try {
            Field gp2 = living.getClass().getSuperclass().getDeclaredField("bH");
            gp2.setAccessible(true);
            gp2.set(living, profile);
            gp2.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public void disguisePlayer() {
        Bukkit.getPluginManager().callEvent(new DisguiseUpdateEvent(disguise.getPlayer(), BukkitAPI.bukkit_users.get(disguise.getPlayer()).getMain_group()));
        if (!CommonsBukkit.getInstance().disguise_list.containsKey(disguise.getDisguise())) {
            CommonsBukkit.getInstance().disguise_list.put(disguise.getDisguise(), disguise);
        }

        CraftPlayer craftPlayer = ((CraftPlayer) disguise.getPlayer());

        GameProfile playerprofile = new GameProfile(disguise.getPlayer().getUniqueId(), disguise.getPlayer().getName());
        GameProfile profile = new GameProfile(disguise.getPlayer().getUniqueId(), disguise.getDisguise());
        EntityLiving entityLiving = craftPlayer.getHandle();

        if (disguise.getGroup().getName().equals("usuario")) {
            disguise.getPlayer().setPlayerListName((GetAPIColor.getColor(disguise.getGroup().getColor())) + disguise.getGroup().getSymbol() + disguise.getDisguise());
            disguise.getPlayer().setDisplayName((GetAPIColor.getColor(disguise.getGroup().getColor())) + disguise.getGroup().getSymbol() + disguise.getDisguise());
        } else {
            disguise.getPlayer().setPlayerListName((GetAPIColor.getColor(disguise.getGroup().getColor())) + disguise.getGroup().getSymbol() + " " + disguise.getDisguise());
            disguise.getPlayer().setDisplayName((GetAPIColor.getColor(disguise.getGroup().getColor())) + disguise.getGroup().getSymbol() + " " + disguise.getDisguise());
        }

        craftPlayer.getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, craftPlayer.getHandle()));

        craftPlayer.getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, craftPlayer.getHandle()));

        if (!receiver.getUniqueId().equals(disguise.getPlayer().getUniqueId())) {
            ((CraftPlayer) receiver).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(disguise.getPlayer().getEntityId()));
            ((CraftPlayer) receiver).getHandle().playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(craftPlayer.getHandle()));
            Bukkit.getServer().getScheduler().runTask(CommonsBukkit.getInstance(), new Runnable() {
                @Override
                public void run() {
                    setGP(entityLiving, profile);
                    NametagChanger.changePlayerName(disguise.getPlayer(), "",NametagChanger.TeamAction.CREATE, disguise.getGroup());
                    receiver.hidePlayer(disguise.getPlayer());
                }
            });
            Bukkit.getServer().getScheduler().runTaskLater(CommonsBukkit.getInstance(), new Runnable() {
                @Override
                public void run() {
                    receiver.showPlayer(disguise.getPlayer());
                }
            }, 2);
            Bukkit.getServer().getScheduler().runTaskLater(CommonsBukkit.getInstance(), new Runnable() {
                @Override
                public void run() {
                    setGP(entityLiving, playerprofile);
                }
            }, 3);
        }
    }
}