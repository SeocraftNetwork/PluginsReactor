package net.seocraft.commons.bukkit.users;

import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.commons.bukkit.CommonsBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Team;

public class NametagChanger {

    private static CommonsBukkit plugin = CommonsBukkit.getPlugin(CommonsBukkit.class);

    public static void changePlayerName(Player player, String suffix, TeamAction action, Group group) {
        String priority_added = group.getPriority().toString().split("0")[0];
        String priority_fixed = "0000" + priority_added + player.getName();
        if (Bukkit.getScoreboardManager().getMainScoreboard().getTeam(priority_fixed) == null) {
            Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(priority_fixed);
        }
        Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(priority_fixed);
        if (group.getName().equals("usuario")) {
            team.setPrefix(GetAPIColor.getColor(group.getColor()) + group.getSymbol());
        } else {
            team.setPrefix( GetAPIColor.getColor(group.getColor()) + group.getSymbol() + " ");
        }
        team.setSuffix(suffix);
        team.setNameTagVisibility(NameTagVisibility.ALWAYS);

        switch (action) {
            case CREATE:
                team.addPlayer(player);
                break;
            case UPDATE:
                team.unregister();
                plugin.board.registerNewTeam(group.getPriority() + player.getName());
                team = plugin.board.getTeam(group.getPriority() + player.getName());
                if (group.getName().equals("usuario")) {
                    team.setPrefix(GetAPIColor.getColor(group.getColor()) + group.getSymbol());
                } else {
                    team.setPrefix(" " + GetAPIColor.getColor(group.getColor()) + group.getSymbol());
                }
                team.setSuffix(suffix);
                team.setNameTagVisibility(NameTagVisibility.ALWAYS);
                team.addPlayer(player);
                break;
            case DESTROY:
                team.unregister();
                break;
        }
    }

    public enum TeamAction {
        CREATE, DESTROY, UPDATE
    }
}