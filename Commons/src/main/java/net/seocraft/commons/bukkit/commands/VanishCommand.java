package net.seocraft.commons.bukkit.commands;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.util.RedisConnection;
import net.seocraft.commons.bukkit.command_framework.Command;
import net.seocraft.commons.bukkit.command_framework.CommandArgs;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class VanishCommand {

    @Command(name = "vanish", aliases = {"vc", "v", "vanishearme", "banicharme"}, description = "Comando para esconderte de los demás usuarios", usage = "/vanish", permission = "util.staff.vanish", inGameOnly = true)
    public void onCommand(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if(!RedisConnection.hasString(args.getPlayer().getName() + ".vanished")) {
            if (!RedisConnection.addString(args.getPlayer().getName() + ".vanished", "true")) {
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                args.getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "command_vanish_error"));
            } else {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!player.hasPermission("util.staff.vanish.bypass")) {
                        player.hidePlayer(args.getPlayer());
                    }
                }
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.ENDERMAN_TELEPORT, 1f, 1f);
                args.getPlayer().sendMessage(ChatColor.GREEN + injector.getString(player_language, "command_vanish_enabled"));
            }
        } else {
            if (RedisConnection.deleteString(args.getPlayer().getName() + ".vanished").equals("true")) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.showPlayer(args.getPlayer());
                }
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.ENDERMAN_TELEPORT, 1f, 1f);
                args.getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "command_vanish_disabled"));
            } else if (!RedisConnection.deleteString(args.getPlayer().getName() + ".vanished").equals("error")) {
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                args.getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "command_vanish_error"));
            }
        }
    }
}