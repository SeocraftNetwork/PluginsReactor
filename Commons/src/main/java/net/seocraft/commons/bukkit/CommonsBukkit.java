package net.seocraft.commons.bukkit;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.Disguise;
import net.seocraft.commons.bukkit.command_framework.CommandFramework;
import net.seocraft.commons.bukkit.commands.*;
import net.seocraft.commons.bukkit.listeners.GroupUpdateListener;
import net.seocraft.commons.bukkit.listeners.JoinDataListener;
import net.seocraft.commons.bukkit.listeners.SignChangeListener;
import net.seocraft.commons.bukkit.users.ChatModifier;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;

public class CommonsBukkit extends JavaPlugin {

    public HashMap<Player, Disguise> disguise = new HashMap<>();
    public HashMap<Player, String> disguise_name = new HashMap<>();
    public HashMap<Player, String> disguise_group = new HashMap<>();
    public HashMap<String, Disguise> disguise_list = new HashMap<>();

    private static CommonsBukkit instance;
    public Scoreboard board;

    @Override
    public void onEnable() {
        System.out.println(ChatColor.RED + BukkitAPI.getInstance().getConfig().getString("api.type"));
        getServer().getPluginManager().registerEvents(new JoinDataListener(), this);
        getServer().getPluginManager().registerEvents(new GroupUpdateListener(), this);
        getServer().getPluginManager().registerEvents(new ChatModifier(), this);
        getServer().getPluginManager().registerEvents(new SignChangeListener(), this);
        getServer().getPluginManager().registerEvents(new ProfileCommand(), this);
        instance = this;
        this.registerCommands();
    }

    @Override
    public void onDisable() {
    }

    public static CommonsBukkit getInstance(){
        return instance;
    }

    private void loadConfig(){
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    private void registerCommands() {
        CommandFramework commandFramework = new CommandFramework(this);
        commandFramework.registerCommands(new AddGroupCommand());
        commandFramework.registerCommands(new RemoveGroupCommand());
        commandFramework.registerCommands(new VanishCommand());
        commandFramework.registerCommands(new DisguiseCommand());
        commandFramework.registerCommands(new ProfileCommand());
    }
}