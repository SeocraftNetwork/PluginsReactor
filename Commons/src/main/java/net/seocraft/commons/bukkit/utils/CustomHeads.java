package net.seocraft.commons.bukkit.utils;

import net.seocraft.api.bukkit.packets.CustomSkull;
import net.seocraft.api.bukkit.packets.NBTHandler;
import org.bukkit.inventory.ItemStack;

public class CustomHeads {
    public static ItemStack profileFriends() {
        ItemStack raw_friends = CustomSkull.getSkull("http://textures.minecraft.net/texture/11827e965da3f9d8ada413335e4aa6a0718f38725e74987ae3036cb925d6cc63");
        ItemStack friends = NBTHandler.addString(raw_friends, "accessor", "FRIENDS_ACCESSOR");
        return friends;
    }

    public static ItemStack profileParties() {
        ItemStack raw_parties = CustomSkull.getSkull("http://textures.minecraft.net/texture/df323ec6d848c24950fc7aa51b6caca05a22fd7068d12504dbb97213f1f20980");
        ItemStack parties = NBTHandler.addString(raw_parties, "accessor", "PARTIES_ACCESSOR");
        return parties;
    }

    public static ItemStack profileGangs() {
        ItemStack raw_gangs = CustomSkull.getSkull("http://textures.minecraft.net/texture/43a2b69fcae48256be37706706c6748576e1860b1f5c9634b5c2207d5e1c70e9");
        ItemStack gangs = NBTHandler.addString(raw_gangs, "accessor", "GANGS_ACCESSOR");
        return gangs;
    }

    public static ItemStack profileRecent() {
        ItemStack raw_recent = CustomSkull.getSkull("http://textures.minecraft.net/texture/826a2c7db4f098bcc1a4549289a09cc12d3acd416ce733ef9587695377829900");
        ItemStack recent = NBTHandler.addString(raw_recent, "accessor", "RECENT_ACCESSOR");
        return recent;
    }

    public static ItemStack prevPage() {
        ItemStack prev_page = CustomSkull.getSkull("http://textures.minecraft.net/texture/32ff8aaa4b2ec30bc5541d41c8782199baa25ae6d854cda651f1599e654cfc79");
        return prev_page;
    }

    public static ItemStack actualPage() {
        ItemStack actual_page = CustomSkull.getSkull("http://textures.minecraft.net/texture/dd2acd9f2dfc2e05f69d941fe9970e8c3f05527a02a9381157891c8ddb8cf3");
        return actual_page;
    }

    public static ItemStack nextPage() {
        ItemStack next_page = CustomSkull.getSkull("http://textures.minecraft.net/texture/aab95a8751aeaa3c671a8e90b83de76a0204f1be65752ac31be2f98feb64bf7f");
        return next_page;
    }
}