package net.seocraft.commons.bukkit.users;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.requests.user.GetPermissions;
import net.seocraft.commons.bukkit.CommonsBukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

public class PermissionsManifest {

    private static CommonsBukkit plugin = CommonsBukkit.getPlugin(CommonsBukkit.class);

    public static void setupPermissions(Player player) {
        PermissionAttachment attachment = player.addAttachment(plugin);
        for (String permisos : GetPermissions.request(player.getName())) {
            attachment.unsetPermission(permisos);
            if (permisos.contains("*")) {
                player.setOp(true);
            } else {
                attachment.setPermission(permisos, true);
            }
        }
    }
}