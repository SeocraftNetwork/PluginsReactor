package net.seocraft.commons.bukkit.commands;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.extended_events.GroupsUpdateEvent;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.bukkit.requests.user.AddGroup;
import net.seocraft.api.bukkit.requests.user.JoinData;
import net.seocraft.commons.bukkit.command_framework.Command;
import net.seocraft.commons.bukkit.command_framework.CommandArgs;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;


public class AddGroupCommand {

    @Command(name = "addgroup", aliases = {"ag", "addg"}, description = "Comando para agregar un grupo al usuario", usage = "/addgroup <usuario> <grupo>", permission = "util.admin.addgroup", inGameOnly = true)
    public void onCommand(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (args.length() == 2) {
            String[] change_event = AddGroup.request(args.getArgs()[0], args.getArgs()[1], BukkitAPI.bukkit_users.get(args.getPlayer()).getToken());
            if (change_event[0].equals("true")) {
                Player player = Bukkit.getPlayer(args.getArgs()[0]);
                args.getSender().sendMessage(ChatColor.GREEN + injector.getString(player_language, "command_group_modify_add")
                        .replace("[0]", ChatColor.AQUA + args.getArgs()[1] + ChatColor.GREEN)
                        .replace("[1]", ChatColor.AQUA + args.getArgs()[0]) + ChatColor.GREEN);
                if (player == null) return;
                BukkitUser user = JoinData.getPlayerUserObject(player.getName(), player.getAddress().toString().split(":")[0].replace("/", ""));
                GroupsUpdateEvent event = new GroupsUpdateEvent(player, user.getMain_group(), user.getSecondary_groups());
                Bukkit.getServer().getPluginManager().callEvent(event);
                BukkitAPI.bukkit_users.put(player, user);
            } else if(change_event[0].equals("false")) {
                args.getSender().sendMessage(ChatColor.RED + change_event[1]);
            }
        } else {
            args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
            args.getSender().sendMessage(ChatColor.RED + injector.getString(player_language, "correct_usage") + ": /addgroup <" + injector.getString(player_language, "user_placeholder") + "> <" + injector.getString(player_language, "group_placecholder") + ">");
        }
    }
}