package net.seocraft.commons.bukkit.commands;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_8_R3.*;
import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.bukkit.models.Disguise;
import net.seocraft.api.bukkit.requests.disguises.DisguiseNameChecker;
import net.seocraft.api.bukkit.requests.disguises.DisguiseRemove;
import net.seocraft.api.bukkit.requests.disguises.DisguiseUser;
import net.seocraft.api.shared.models.Group;
import net.seocraft.commons.bukkit.command_framework.Command;
import net.seocraft.commons.bukkit.command_framework.CommandArgs;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.api.bukkit.packets.SignGui;
import net.seocraft.api.bukkit.requests.user.GetGroupPlaceholder;
import net.seocraft.commons.bukkit.CommonsBukkit;
import net.seocraft.commons.bukkit.disguise.DisguiseManager;
import net.seocraft.commons.bukkit.disguise.DisguiseRandom;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftMetaBook;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.List;
import java.util.Random;

public class DisguiseCommand {

    @Command(name = "disguise", aliases = {"disfraz", "deguisement", "dis"}, description = "Comando para esconder tu nickname y tu rango ante otros jugadores", usage = "/disguise", permission = "util.disguise", inGameOnly = true)
    public void onDisguiseCommand(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (!CommonsBukkit.getInstance().disguise.containsKey(args.getPlayer())) {
            ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
            BookMeta bookMeta = (BookMeta) book.getItemMeta();
            List<IChatBaseComponent> pages;
            try {
                pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
            } catch (ReflectiveOperationException ex) {
                ex.printStackTrace();
                return;
            }
            TextComponent text = new TextComponent(ChatColor.RED + "\u25BA" + ChatColor.DARK_AQUA + "Cambia tu " + ChatColor.DARK_RED + ChatColor.BOLD + "NICKNAME" + ChatColor.RED + "\u25C4\n" + ChatColor.BLACK + "Los nicknames te permiten jugar con un usuario diferente al que ya conoces.\n" +  "Todas las reglas aún aplican. Aún puedes ser reportado y tu historial de nombres es guardado.\n\n");
            TextComponent text2 = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.BLACK + "De acuerdo, elige mi nombre.");
            text2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename undefined"));
            text2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escojer tu nombre!").create()));
            IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(new BaseComponent[]{text, text2}));
            pages.add(page);
            bookMeta.setTitle(ChatColor.YELLOW + "Disguises");
            bookMeta.setAuthor(ChatColor.AQUA + "Insanely");
            book.setItemMeta(bookMeta);
            openBook(book, args.getPlayer());
        } else {
            BukkitUser user = BukkitAPI.bukkit_users.get(args.getPlayer());
            Disguise disguise = new Disguise(args.getPlayer(), args.getPlayer().getName(), user.getMain_group());
            String[] delete_disguise = DisguiseRemove.request(BukkitAPI.bukkit_users.get(args.getPlayer()).getToken());
            if (delete_disguise[0].equals("true")) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    DisguiseManager disguiseManager = new DisguiseManager(disguise, player);
                    disguiseManager.disguisePlayer();
                }
                CommonsBukkit.getInstance().disguise_group.remove(args.getPlayer());
                CommonsBukkit.getInstance().disguise.remove(args.getPlayer());
                CommonsBukkit.getInstance().disguise_name.remove(args.getPlayer());
                BukkitAPI.bukkit_users.get(args.getPlayer()).setDisguised(false);
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_PLING, 1f, 1f);
                args.getPlayer().sendMessage(ChatColor.GREEN + injector.getString(player_language, delete_disguise[1]));
            } else if (delete_disguise[0].equals("false")) {
                args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                args.getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, delete_disguise[1]));
            }

        }
    }

    @Command(name = "disguiserank", permission = "util.disguise", inGameOnly = true)
    public void onDisguiseCommand2(CommandArgs args) {
        if (!CommonsBukkit.getInstance().disguise_name.containsKey(args.getPlayer())) return;
        if (!CommonsBukkit.getInstance().disguise.containsKey(BukkitAPI.bukkit_users.get(args.getPlayer()))) {
            if (args.getArgs()[0].equalsIgnoreCase("choose")) {
                if (args.length() == 2) {
                    String rank = args.getArgs()[1];
                    if (!rank.equals("usuario") && !rank.equals("warlord") && !rank.equals("ghost") && !rank.equals("shallow") && !rank.equals("zeno")) {
                        args.getPlayer().sendMessage(ChatColor.RED + "No puedes escojer ese rango!");
                        return;
                    }
                    CommonsBukkit.getInstance().disguise_group.put(args.getPlayer(), args.getArgs()[1]);
                    args.getPlayer().performCommand("disguiseconfirm");
                }
            }
            if (args.length() == 1) {
                if (args.getArgs()[0].equalsIgnoreCase("undefined")) {
                    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
                    BookMeta bookMeta = (BookMeta) book.getItemMeta();
                    List<IChatBaseComponent> pages;
                    try {
                        pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
                    } catch (ReflectiveOperationException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    TextComponent espacio = new TextComponent("\n");
                    TextComponent texto = new TextComponent("\u00A1Ahora, escogamos un " + ChatColor.BOLD
                            + "RANGO" + ChatColor.BLACK + "!\n\n");
                    TextComponent text = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.GRAY + "USUARIO");
                    TextComponent text2 = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.YELLOW + "WARLORD");
                    TextComponent text3 = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.GREEN + "GHOST");
                    TextComponent text4 = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.BLUE + "SHALLOW");
                    TextComponent text5 = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.LIGHT_PURPLE + "ZENO");
                    text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para disfrazarte como " + ChatColor.GRAY + "USUARIO" + ChatColor.YELLOW + "!").create()));
                    text2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para disfrazarte como " + ChatColor.YELLOW + "WARLORD" + ChatColor.YELLOW + "!").create()));
                    text3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para disfrazarte como " + ChatColor.GREEN + "GHOST" + ChatColor.YELLOW + "!").create()));
                    text4.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para disfrazarte como " + ChatColor.BLUE + "SHALLOW" + ChatColor.YELLOW + "!").create()));
                    text5.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para disfrazarte como " + ChatColor.LIGHT_PURPLE + "ZENO" + ChatColor.YELLOW + "!").create()));
                    text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguiserank choose usuario"));
                    text2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguiserank choose warlord"));
                    text3.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguiserank choose ghost"));
                    text4.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguiserank choose shallow"));
                    text5.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguiserank choose zeno"));
                    IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(new BaseComponent[]{texto, text, espacio, text2, espacio, text3, espacio, text4, espacio, text5}));
                    pages.add(page);
                    bookMeta.setTitle(ChatColor.YELLOW + "Disfraces");
                    bookMeta.setAuthor(ChatColor.AQUA + "Insanely_");
                    book.setItemMeta(bookMeta);
                    openBook(book, args.getPlayer());
                }
            }
        }
    }

    @Command(name = "disguisename", permission = "util.disguise", inGameOnly = true)
    public void onDisguiseCommand3(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (!CommonsBukkit.getInstance().disguise.containsKey(BukkitAPI.bukkit_users.get(args.getPlayer()))) {
            if (args.getArgs()[0].equalsIgnoreCase("choose")) {
                if (args.length() == 2) {
                    if (StringUtils.isAlphanumeric(args.getArgs()[1])) {
                        if (args.getArgs()[1].length() >= 3) {
                            CommonsBukkit.getInstance().disguise_name.put(args.getPlayer(), args.getArgs()[1]);
                            args.getPlayer().performCommand("disguiserank undefined");
                        }
                    }
                }
            }
            if (args.length() == 1) {
                if (args.getArgs()[0].equalsIgnoreCase("customconfirm")) {
                    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
                    BookMeta bookMeta = (BookMeta) book.getItemMeta();
                    List<IChatBaseComponent> pages;
                    try {
                        pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
                    } catch (ReflectiveOperationException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    TextComponent text = new TextComponent(ChatColor.GRAY + "¿Estás seguro de que quieres usar este nombre?");
                    TextComponent text2 = new TextComponent(ChatColor.GREEN + "" + ChatColor.BOLD + "USAR NOMBRE");
                    TextComponent text3 = new TextComponent(ChatColor.RED + "" + ChatColor.BOLD + "CAMBIAR NOMBRE");
                    text2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename choose " + CommonsBukkit.getInstance().disguise_name));
                    text2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escoger este nombre!").create()));
                    text3.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename custom"));
                    text3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para volver a elegir un nombre!").create()));

                    IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(new BaseComponent[]{text, text2, text3}));
                    pages.add(page);
                    bookMeta.setTitle(ChatColor.YELLOW + "Disguise");
                    bookMeta.setAuthor(ChatColor.AQUA + "Insanely_");
                    book.setItemMeta(bookMeta);
                    openBook(book, args.getPlayer());
                }
                if (args.getArgs()[0].equalsIgnoreCase("custom")) {
                    SignGui gui = new SignGui(args.getPlayer(), "", "^^^^^^", injector.getString(player_language, "command_disguise_sign_top"), injector.getString(player_language, "command_disguise_sign_bottom"), "disguise_custom");
                    gui.open();
                }
                if (args.getArgs()[0].equalsIgnoreCase("random")) {
                    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
                    BookMeta bookMeta = (BookMeta) book.getItemMeta();
                    List<IChatBaseComponent> pages;
                    try {
                        pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
                    } catch (ReflectiveOperationException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    List<String> list = DisguiseRandom.getList();
                    Random random = new Random();
                    final String name = list.get(random.nextInt(list.size()));
                    TextComponent text = new TextComponent(ChatColor.BLACK + "Hemos generado un nick random para ti: " + ChatColor.BOLD + name + "\n\n");
                    TextComponent text2 = new TextComponent(ChatColor.GREEN + "" + ChatColor.BOLD + "USAR NOMBRE");
                    TextComponent text3 = new TextComponent(ChatColor.RED + "" + ChatColor.BOLD + "REINTENTAR");
                    TextComponent text4 = new TextComponent("\n\n");
                    TextComponent text5 = new TextComponent(ChatColor.BLACK + "" + ChatColor.UNDERLINE + "O escoje un nombre\n" + ChatColor.BLACK + "" + ChatColor.UNDERLINE + "personalizado");
                    text2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename choose " + name));
                    text5.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename custom"));
                    text5.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escoger un nombre personalizado!").create()));
                    text3.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename random"));
                    text3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para reintentar con otro nombre!").create()));
                    text2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escoger este nombre!").create()));
                    IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(new BaseComponent[]{text, text2, text3, text4, text5}));
                    pages.add(page);
                    bookMeta.setTitle(ChatColor.YELLOW + "Disguise");
                    bookMeta.setAuthor(ChatColor.AQUA + "Insanely_");
                    book.setItemMeta(bookMeta);
                    openBook(book, args.getPlayer());
                }

                if (args.getArgs()[0].equalsIgnoreCase("undefined")){
                    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
                    BookMeta bookMeta = (BookMeta) book.getItemMeta();
                    List<IChatBaseComponent> pages;
                    try {
                        pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
                    } catch (ReflectiveOperationException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    TextComponent espacio = new TextComponent("\n");
                    TextComponent texto = new TextComponent(ChatColor.BLACK + "\u00A1De acuerdo, escojamos un " + ChatColor.BOLD
                            + "NICK" + ChatColor.BLACK + "!\n\n");
                    TextComponent text = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.BLACK + "Escoger un nick random");
                    TextComponent text2 = new TextComponent(ChatColor.RED + "\u279C" + ChatColor.BLACK + "Escoger mi propio nick");
                    TextComponent text3 = new TextComponent(ChatColor.BLACK + "\n\nPara volver a ser tu mismo usa: " + ChatColor.BOLD + "/disfraz");
                    text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escoger un nickname random!").create()));
                    text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename random"));
                    text2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguisename custom"));
                    text2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escoger tu nickname!").create()));

                    IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(new BaseComponent[]{texto, text, espacio, text2, text3}));
                    pages.add(page);

                    bookMeta.setTitle(ChatColor.YELLOW + "Disguise");
                    bookMeta.setAuthor(ChatColor.AQUA + "Insanely_");
                    book.setItemMeta(bookMeta);
                    openBook(book, args.getPlayer());
                }
            }
        }
    }

    @Command(name = "disguiseconfirm", permission = "util.disguise", inGameOnly = true)
    public void onDisguiseCommand4(CommandArgs args) {
        String player_language = BukkitAPI.bukkit_users.get(args.getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (!CommonsBukkit.getInstance().disguise_group.containsKey(args.getPlayer())) return;
        if (args.length() == 1) {
            if (args.getArgs()[0].equals("confirm")) {
                Group group = GetGroupPlaceholder.request(CommonsBukkit.getInstance().disguise_group.get(args.getPlayer()), BukkitAPI.getInstance().getConfig().getString("realm"));
                Disguise disguise = new Disguise(args.getPlayer(), CommonsBukkit.getInstance().disguise_name.get(args.getPlayer()), group);
                CommonsBukkit.getInstance().disguise.put(args.getPlayer(), disguise);
                String[] check_disguise = DisguiseNameChecker.request(CommonsBukkit.getInstance().disguise_name.get(args.getPlayer()));
                if(check_disguise[0].equals("false")) {
                    args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                    args.getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, check_disguise[1]));
                    return;
                }
                String[] disguised = DisguiseUser.request(BukkitAPI.bukkit_users.get(args.getPlayer()).getToken(), CommonsBukkit.getInstance().disguise_name.get(args.getPlayer()), group.getName().toLowerCase());
                if(disguised[0].equals("false")) {
                    args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                    args.getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, disguised[1]));
                    return;
                } else if (disguised[0].equals("true")) {
                    BukkitAPI.bukkit_users.get(args.getPlayer()).setDisguised(true);
                    args.getPlayer().playSound(args.getPlayer().getLocation(), Sound.LEVEL_UP, 1f, 1f);
                    args.getPlayer().sendMessage(ChatColor.GREEN + injector.getString(player_language, disguised[1]));
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        DisguiseManager manager = new DisguiseManager(disguise, player);
                        manager.disguisePlayer();
                    }
                }
                return;
            }
        }
        ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) book.getItemMeta();
        List<IChatBaseComponent> pages;
        try {
            pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
        } catch (ReflectiveOperationException ex) {
            ex.printStackTrace();
            return;
        }
        Group group = GetGroupPlaceholder.request(CommonsBukkit.getInstance().disguise_group.get(args.getPlayer()), BukkitAPI.getInstance().getConfig().getString("realm"));
        TextComponent text;
        if (CommonsBukkit.getInstance().disguise_group.get(args.getPlayer()).equals("usuario")) {
            text = new TextComponent(ChatColor.BLACK + "¿Estás seguro de que quieres disfrazarte como " + GetAPIColor.getColor(group.getColor()) + group.getSymbol() + CommonsBukkit.getInstance().disguise_name.get(args.getPlayer()) + ChatColor.BLACK + "? \n\n");
        } else {
            text = new TextComponent(ChatColor.BLACK + "¿Estás seguro de que quieres disfrazarte como " + GetAPIColor.getColor(group.getColor()) + group.getSymbol() + " " + CommonsBukkit.getInstance().disguise_name.get(args.getPlayer()) + ChatColor.BLACK + "? \n\n");
        }
        TextComponent text2 = new TextComponent(ChatColor.GREEN + "" + ChatColor.BOLD + "USAR DISFRAZ\n");
        TextComponent text3 = new TextComponent(ChatColor.RED + "" + ChatColor.BOLD + "VOLVER A ELEGIR");
        text2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguiseconfirm confirm"));
        text2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para escoger este disfráz!").create()));
        text3.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/disguise"));
        text3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + "\u00A1Haz click para volver a elegir un disfráz!").create()));

        IChatBaseComponent page = IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(new BaseComponent[]{text, text2, text3}));
        pages.add(page);
        bookMeta.setTitle(ChatColor.YELLOW + "Disguise");
        bookMeta.setAuthor(ChatColor.AQUA + "Insanely_");
        book.setItemMeta(bookMeta);
        openBook(book, args.getPlayer());
    }

    public void openBook(ItemStack book, Player p) {
        int slot = p.getInventory().getHeldItemSlot();
        ItemStack old = p.getInventory().getItem(slot);
        p.getInventory().setItem(slot, book);

        ByteBuf buf = Unpooled.buffer(256);
        buf.setByte(0, (byte)0);
        buf.writerIndex(1);

        PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer(buf));
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
        p.getInventory().setItem(slot, old);
    }
}