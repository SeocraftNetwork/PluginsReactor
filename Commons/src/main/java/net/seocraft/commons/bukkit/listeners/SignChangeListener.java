package net.seocraft.commons.bukkit.listeners;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.extended_events.PlayerInputEvent;
import net.seocraft.api.bukkit.packets.SignInputHandler;
import net.seocraft.api.bukkit.requests.disguises.DisguiseNameChecker;
import net.seocraft.api.shared.utils.BungeeMessaging;
import net.seocraft.commons.bukkit.CommonsBukkit;
import net.seocraft.commons.bukkit.commands.ProfileCommand;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SignChangeListener implements Listener {

    @EventHandler
    public void onSignInput(PlayerInputEvent event) {
        String player_language = BukkitAPI.bukkit_users.get(event.getGui().getPlayer()).getLanguage();
        TranslatableComponent injector = new TranslatableComponent();
        if (event.getGui().getT4().equals(injector.getString(player_language, "command_disguise_sign_bottom"))) {
            CommonsBukkit.getInstance().disguise_name.put(event.getGui().getPlayer(), event.getGui().getT1());
            if (event.getGui().getT1().length() < 3) {
                event.getGui().getPlayer().playSound(event.getGui().getPlayer().getLocation(), Sound.NOTE_BASS, 1, 2);
                event.getGui().getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "command_disguise_length"));
                event.getGui().getPlayer().performCommand("disguisename undefined");
                return;
            }
            if (!StringUtils.isAlphanumeric(event.getGui().getT1())) {
                event.getGui().getPlayer().playSound(event.getGui().getPlayer().getLocation(), Sound.NOTE_BASS, 1, 2);
                event.getGui().getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "command_disguise_alphanumeric"));
                event.getGui().getPlayer().performCommand("disguisename undefined");
                return;
            }
            String[] disguised = DisguiseNameChecker.request(event.getGui().getT1());
            if (disguised[0].equals("false")) {
                event.getGui().getPlayer().playSound(event.getGui().getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                event.getGui().getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, disguised[1]));
                event.getGui().getPlayer().performCommand("disguisename undefined");
                return;
            }
        }
        if (event.getGui().getT4().equals(injector.getString(player_language, "commons_friends_add_player_bottom_sign"))) {
            String name = event.getGui().getT1();
            if (!event.getGui().getT1().isEmpty()) {
                BungeeMessaging.executeBungeeCommand(event.getGui().getPlayer(), "friends " + injector.getString(player_language, "commons_friends_add_command") + " " + name);
            } else {
                event.getGui().getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "commons_friends_empty_sign"));
            }
        }
        if (event.getGui().getT4().equals(injector.getString(player_language, "commons_friends_remove_player_bottom_sign"))) {
            String name = event.getGui().getT1();
            if (!event.getGui().getT1().isEmpty()) {
                BungeeMessaging.executeBungeeCommand(event.getGui().getPlayer(), "friends " + injector.getString(player_language, "commons_friends_remove_command") + " " + name);
            } else {
                event.getGui().getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "commons_friends_empty_sign"));
            }
        }
        if (event.getGui().getT4().equals(injector.getString(player_language, "commons_friends_search_player_bottom_sign"))) {
            String name = event.getGui().getT1();
            if (!event.getGui().getT1().isEmpty()) {
                ProfileCommand profileCommand = new ProfileCommand();
                profileCommand.friends_search(event.getGui().getPlayer(), "1", name);
            } else {
                event.getGui().getPlayer().sendMessage(ChatColor.RED + injector.getString(player_language, "commons_friends_empty_sign"));
            }
        }
    }
}