package net.seocraft.commons.bukkit.listeners;

import net.seocraft.api.bukkit.BukkitAPI;
import net.seocraft.api.bukkit.models.BukkitUser;
import net.seocraft.api.bukkit.models.Disguise;
import net.seocraft.api.bukkit.requests.disguises.DisguiseJoin;
import net.seocraft.api.shared.utils.GetAPIColor;
import net.seocraft.api.bukkit.util.RedisConnection;
import net.seocraft.api.bukkit.requests.user.JoinData;
import net.seocraft.cloud.api.CloudAPI;
import net.seocraft.commons.bukkit.CommonsBukkit;
import net.seocraft.commons.bukkit.disguise.DisguiseManager;
import net.seocraft.commons.bukkit.users.NametagChanger;
import net.seocraft.commons.bukkit.users.PermissionsManifest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinDataListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        BukkitUser user = JoinData.getPlayerUserObject(event.getPlayer().getName(), event.getPlayer().getAddress().toString().split(":")[0].replace("/", ""));
        if (user.getLogged().equals("false")) event.getPlayer().kickPlayer(ChatColor.RED + "Error while connecting, please try again");
        if (user.getLogged().equals("authenticating")) return;
        PermissionsManifest.setupPermissions(event.getPlayer());
        user.startToken();
        BukkitAPI.bukkit_users.put(event.getPlayer(), user);
        NametagChanger.changePlayerName(event.getPlayer(), "", NametagChanger.TeamAction.CREATE, user.getMain_group());
        if (user.getMain_group().getName().equals("usuario")) {
            event.getPlayer().setDisplayName(GetAPIColor.getColor(user.getMain_group().getColor()) + user.getMain_group().getSymbol() + event.getPlayer().getName());
        } else {
            event.getPlayer().setDisplayName(GetAPIColor.getColor(user.getMain_group().getColor()) + user.getMain_group().getSymbol() + " " + event.getPlayer().getName());
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (RedisConnection.hasString(event.getPlayer().getName() + ".vanished")) {
                if (!player.hasPermission("util.staff.vanish.bypass")) {
                    player.hidePlayer(event.getPlayer());
                }
            }
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (RedisConnection.hasString(player.getName() + ".vanished")) {
                if (!event.getPlayer().hasPermission("util.staff.vanish.bypass")) {
                    event.getPlayer().hidePlayer(player);
                }
            }
        }
        if (user.getDisguised()) {
            Disguise disguise = DisguiseJoin.request(event.getPlayer().getName(), BukkitAPI.bukkit_users.get(event.getPlayer()).getToken());
            for (Player player : Bukkit.getOnlinePlayers()) {
                DisguiseManager disguiseManager = new DisguiseManager(disguise, player);
                disguiseManager.disguisePlayer();
                CommonsBukkit.getInstance().disguise.put(event.getPlayer(), disguise);
            }
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            BukkitUser player_user = BukkitAPI.bukkit_users.get(player);
            if (player_user.getDisguised()) {
                Disguise disguise = CommonsBukkit.getInstance().disguise.get(player);
                DisguiseManager disguiseManager = new DisguiseManager(disguise, event.getPlayer());
                disguiseManager.disguisePlayer();
            }
        }
    }
}