package net.seocraft.commons.bungee.commands;

import com.google.gson.Gson;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.requests.punishments.PunishmentRequest;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.api.shared.utils.BukkitMessaging;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.commons.bungee.admin_feed.FeedUtils;

public class BanCommand extends Command {

    public BanCommand(){
        super("ban", "", "pb");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        TranslatableComponent injector = new TranslatableComponent();
        String language = BungeeAPI.bungee_users.get(player).getLanguage();
        if (sender instanceof ProxiedPlayer) {
            if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.ban")) {
                BukkitMessaging.sendSound(player, "bass");
                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "no_permission")));
            } else {
                if (args.length >= 2) {
                    if (CommonsBungee.getInstance().getProxy().getPlayer(args[0]) == null) {
                        player.sendMessage(ChatColor.RED + injector.getString(language, "commons_admin_punishments_not_connected") + " " + ChatColor.YELLOW + "https://www.seocraft.net" + ChatColor.RED + ".");
                    } else if (args[0].toLowerCase().equals(player.getName().toLowerCase())) {
                        player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishments_own") + "."));
                    } else {
                        ProxiedPlayer punished = CommonsBungee.getInstance().getProxy().getPlayer(args[0]);
                        if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(punished)).contains("commons.staff.basics")) {
                            String reason = "";
                            for (int i = 1; i < args.length; i++) {
                                if (i != args.length - 1) {
                                    reason += args[i] + " ";
                                } else {
                                    reason += args[i];
                                }
                            }
                            String punishment = PunishmentRequest.request(sender.getName(), args[0], "ban", reason, player.getServer().getInfo().getName(), player.getAddress().toString().split(":")[0].replace("/", ""));
                            if (!JSONDeserializer.deserialize(punishment).get("query_success").getAsString().equals("true")) {
                                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString( language, JSONDeserializer.errorMessage(punishment))));
                            } else {
                                String punished_language = BungeeAPI.bungee_users.get(punished).getLanguage();
                                Gson gson = new Gson();
                                Group punisher_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(punishment).get("query_user").getAsJsonObject().get("real_group").getAsJsonObject()), Group.class);
                                Group punished_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(punishment).get("query_target").getAsJsonObject().get("real_group").getAsJsonObject()), Group.class);
                                punished.disconnect(new TextComponent(ChatColor.RED + injector.getString(punished_language, "commons_admin_punishments_permanent_ban") + " - " + ChatColor.valueOf(punisher_group.getColor().toUpperCase()) + punisher_group.getSymbol() + " " + player.getName()
                                        + "\n\n" + ChatColor.AQUA + "\u00BB " + reason + "\n\n" +
                                        ChatColor.YELLOW + injector.getString(punished_language, "commons_admin_punishment_disconnect_mistake") + ChatColor.GOLD + " seocraft.net/apelar"
                                ));
                                BukkitMessaging.sendPermaBanSuccess(punisher_group.name, player, punished.getName(), punished_group.getColor(), punished_group.getSymbol(), language, reason);
                                FeedUtils.punishmentAlert("ban", player, punished, reason, punished.getServer().getInfo().getName().split("-")[0]);
                            }
                        } else {
                            player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishments_bypass") + "."));
                        }
                    }
                } else {
                    sender.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "correct_usage") + ": " + "/ban <" + injector.getString(language, "user_placeholder") + "> <" + injector.getString(language, "reason_placeholder") + ">"));
                }
            }
        }
    }
}