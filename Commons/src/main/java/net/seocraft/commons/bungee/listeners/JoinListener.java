package net.seocraft.commons.bungee.listeners;

import com.google.gson.Gson;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.models.BungeeUser;
import net.seocraft.api.bungee.requests.user.GetPermissions;
import net.seocraft.api.bungee.requests.user.JoinData;
import net.seocraft.api.bungee.requests.user.PreLogin;
import net.seocraft.api.bungee.requests.user.QuitRequest;
import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.api.shared.i18n.TranslatableComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JoinListener implements Listener {

    @EventHandler
    public void preLogin(PreLoginEvent event) {
        String response = PreLogin.request(event.getConnection().getName());
        if(!JSONDeserializer.deserialize(response).get("can_join").getAsBoolean()) {
            if (!JSONDeserializer.deserialize(response).get("banned").getAsBoolean()) {
                event.getConnection().disconnect(new TextComponent(ChatColor.RED + "Internal Seocraft API error... Please try again \n\n" + ChatColor.RED + "or contact an administrator..."));
            } else {
                TranslatableComponent injector = new TranslatableComponent();
                String reason = JSONDeserializer.deserialize(response).get("punishment_info").getAsJsonObject().get("reason").getAsString();
                String punisher_name = JSONDeserializer.deserialize(response).get("punishment_info").getAsJsonObject().get("punisher_name").getAsString();
                Gson gson = new Gson();
                Group punisher_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(response).get("punishment_info").getAsJsonObject().get("punisher_group").getAsJsonObject()), Group.class);
                String language = JSONDeserializer.deserialize(response).get("language").getAsString();
                event.getConnection().disconnect(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishments_permanent_ban") + " - " + ChatColor.valueOf(punisher_group.getColor().toUpperCase()) + punisher_group.getSymbol() + " " + punisher_name
                        + "\n\n" + ChatColor.AQUA + "\u00BB " + reason + "\n\n" +
                        ChatColor.YELLOW + injector.getString(language, "commons_admin_punishment_disconnect_mistake") + ChatColor.GOLD + " seocraft.net/apelar"
                ));
            }
        } else {
            BungeeAPI.not_redirected.add(event.getConnection().getName());
        }
    }

    @EventHandler
    public void onServerJoin(ServerConnectEvent event) {
        if (BungeeAPI.not_redirected.contains(event.getPlayer().getName())) {
            BungeeUser user = JoinData.getPlayerUserObject(event.getPlayer().getName());
            CommonsBungee.player_permissions.put(event.getPlayer(), GetPermissions.request(user.getUsername()));
            BungeeAPI.bungee_users.put(event.getPlayer(), user);
            List<String> servers = new ArrayList<>();
            for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
                if (server.getName().split("-")[0].equals("authentication")) {
                    servers.add(server.getName());
                }
            }
            Random random = new Random();
            ServerInfo auth_server = ProxyServer.getInstance().getServerInfo(servers.get(random.nextInt(servers.size())));
            if (!auth_server.getName().equals("authentication-1")) {
                event.getPlayer().connect(auth_server);
            }
            BungeeAPI.not_redirected.remove(event.getPlayer().getName());
        } else {
            BungeeAPI.bungee_users.get(event.getPlayer()).startToken();
            if (event.getPlayer().getServer().getInfo().getName().contains("lobby")) {
                if (RedisConnection.hasString(event.getPlayer().getName() + ".new_friendship_requests")) {
                    String language = BungeeAPI.bungee_users.get(event.getPlayer()).getLanguage();
                    TranslatableComponent injector = new TranslatableComponent();
                    TextComponent message = new TextComponent(ChatColor.AQUA + injector.getString(language, "commons_friends_new_requests"));
                    message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_new_requests_hover")).create()));
                    message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends " + injector.getString(language, "commons_friends_no_friends")));
                    event.getPlayer().sendMessage(message);
                }
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerDisconnectEvent event) {
        QuitRequest.request(BungeeAPI.bungee_users.get(event.getPlayer()).getToken());
    }
}