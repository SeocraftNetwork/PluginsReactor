package net.seocraft.commons.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.models.BungeeUser;
import net.seocraft.api.shared.i18n.TranslatableComponent;

public class PartiesCommand extends Command {

    public PartiesCommand() {
        super("party", "", "parties", "grupo", "grupos", "groupe", "groupes");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            BungeeUser user = BungeeAPI.bungee_users.get(player);
            TranslatableComponent injector = new TranslatableComponent();
            String language = BungeeAPI.bungee_users.get(player).getLanguage();
            if (args.length == 0) {
                sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                sender.sendMessage(ChatColor.GOLD + injector.getString(language, "commons_friends_help_title"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " <" + injector.getString(language, "user_placeholder") + ">" + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_parties_main_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_parties_invite_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_add_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_parties_accept_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_accept_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_parties_disband_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_deny_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_parties_kick_offline_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_list_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_parties_promote_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_remove_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_parties_leave_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_requests_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_friends_toggle_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_toggle_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_parties_command") + " " + injector.getString(language, "commons_friends_removeall_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_removeall_about"));
                sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                return;
            }
        }
    }
}