package net.seocraft.commons.bungee.commands;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.models.BungeeUser;
import net.seocraft.api.bungee.requests.friends.*;
import net.seocraft.api.bungee.utils.GetAPIColor;
import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.shared.commons.UserSort;
import net.seocraft.api.shared.models.Friend;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import net.seocraft.api.shared.processor.FontEnumeration;
import net.seocraft.api.shared.processor.FriendListChatProcessor;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class FriendsCommand extends Command {

    public FriendsCommand(){
        super("friend", "", "friends", "amigo", "amigos", "ami", "amis");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            BungeeUser user = BungeeAPI.bungee_users.get(player);
            TranslatableComponent injector = new TranslatableComponent();
            String language = BungeeAPI.bungee_users.get(player).getLanguage();
            if (args.length == 0) {
                sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                sender.sendMessage(ChatColor.GOLD + injector.getString(language, "commons_friends_help_title"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_help_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_help_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_add_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_add_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_accept_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_accept_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_deny_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_deny_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_list_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_list_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_remove_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_remove_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_requests_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_requests_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_toggle_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_toggle_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_removeall_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_removeall_about"));
                sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                return;
            }
            if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_add_command"))) {
                if (args.length == 2) {
                    if (!args[1].toLowerCase().equals(sender.getName().toLowerCase())) {
                        String[] query = AddFriend.request(BungeeAPI.bungee_users.get(player).getToken(), args[1], ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("util.friends.add.bypass"));
                        if (!query[0].equals("true")) {
                            player.sendMessage(ChatColor.RED + injector.getString(language, query[1]) + ".");
                            return;
                        } else {
                            Gson gson = new Gson();
                            String real_name = JSONDeserializer.deserialize(query[1]).get("real_name").getAsString();
                            Boolean disguised = JSONDeserializer.deserialize(query[1]).get("disguised").getAsBoolean();
                            String main_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(query[1]).get("real_group").getAsJsonObject());
                            Group main_group = gson.fromJson(main_group_string, Group.class);
                            @Nullable String disguised_name = null;
                            @Nullable Group disguise_group = null;
                            if(disguised) {
                                disguised_name = JSONDeserializer.deserialize(query[1]).get("disguised_name").getAsString();
                                String disguise_group_string = JSONDeserializer.jsonString(JSONDeserializer.deserialize(query[1]).get("disguise_group").getAsJsonObject());
                                disguise_group = gson.fromJson(disguise_group_string, Group.class);
                            }
                            String name_placeholder;
                            if (!disguised) {
                                if (main_group.getName().equals("usuario")) {
                                    name_placeholder = GetAPIColor.getColor(main_group.getColor()) + real_name;
                                } else {
                                    name_placeholder = GetAPIColor.getColor(main_group.getColor()) + main_group.getSymbol() + " " + real_name;
                                }
                            } else {
                                if (args[1].toLowerCase().equals(disguised_name.toLowerCase())) {
                                    if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("util.staff.disguise.bypass")) {
                                        if (!main_group.getName().equals("usuario")) {
                                            name_placeholder = GetAPIColor.getColor(disguise_group.getColor()) + disguise_group.getSymbol() + " " + disguised_name;
                                        } else {
                                            name_placeholder = GetAPIColor.getColor(disguise_group.getColor()) + disguised_name;
                                        }
                                    } else {
                                        if (!main_group.getName().equals("usuario")) {
                                            name_placeholder = ChatColor.GRAY + "[" + real_name + "] " + GetAPIColor.getColor(disguise_group.getColor()) + disguise_group.getSymbol() + " " + disguised_name;
                                        } else {
                                            name_placeholder = ChatColor.GRAY + "[" + real_name + "] " + GetAPIColor.getColor(disguise_group.getColor()) + disguised_name;
                                        }
                                    }
                                } else {
                                    if (main_group.getName().equals("usuario")) {
                                        name_placeholder = GetAPIColor.getColor(main_group.getColor()) + real_name;
                                    } else {
                                        name_placeholder = GetAPIColor.getColor(main_group.getColor()) + main_group.getSymbol() + " " + real_name;
                                    }
                                }
                            }
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            sender.sendMessage(ChatColor.GREEN + injector.getString(language, "commons_friends_add_success").replace("[0]", name_placeholder + ChatColor.GREEN + "."));
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            if (CommonsBungee.getInstance().getProxy().getPlayer(real_name) != null) {
                                TextComponent accept = new TextComponent(ChatColor.GREEN + "" + ChatColor.BOLD + "[" + injector.getString(language, "commons_friends_accept_placeholder").toUpperCase() + "]");
                                TextComponent separator = new TextComponent(ChatColor.DARK_GRAY + " - ");
                                TextComponent deny = new TextComponent(ChatColor.RED + "" + ChatColor.BOLD + "[" + injector.getString(language, "commons_friends_deny_placeholder").toUpperCase() + "]");
                                accept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_accept_hover").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + player.getName())).create()));
                                accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends " + injector.getString(language, "commons_friends_accept_command") + " " + sender.getName()));
                                deny.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_deny_hover").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + player.getName())).create()));
                                deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends " + injector.getString(language, "commons_friends_deny_command") + " " + sender.getName()));
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                                if (user.getMain_group().getName().equals("usuario")) {
                                    CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + injector.getString(language, "commons_friends_friend_request_placeholder").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + user.getMain_group().getSymbol() + player.getName()));
                                } else {
                                    CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + injector.getString(language, "commons_friends_friend_request_placeholder").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + user.getMain_group().getSymbol() + " " + player.getName()));
                                }
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(accept, separator, deny);
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            } else if (!RedisConnection.hasString(real_name + ".new_friendship_requests")) {
                                RedisConnection.addString(real_name + ".new_friendship_requests", "true");
                            }
                        }
                        return;
                    } else {
                        sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_request_own"));
                        return;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "command_invalid_usage") + ": /" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_add_command") + " <" + injector.getString(language, "user_placeholder") + ">");
                    return;
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_accept_command"))) {
                if (args.length == 2) {
                    if (!args[1].toLowerCase().equals(sender.getName().toLowerCase())) {
                        String[] query = AcceptFriend.request(BungeeAPI.bungee_users.get(player).getToken(), args[1]);
                        if (!query[0].equals("true")) {
                            player.sendMessage(ChatColor.RED + injector.getString(language, query[1]) + ".");
                            return;
                        } else {
                            Gson gson = new Gson();
                            String real_name = JSONDeserializer.deserialize(query[1]).get("real_name").getAsString();
                            Group main_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(query[1]).get("real_group").getAsJsonObject()), Group.class);
                            String accepted_placeholder;
                            if (!main_group.getName().equals("usuario")) {
                                accepted_placeholder = ChatColor.YELLOW + injector.getString(language, "commons_friends_accept_success").replace("[0]", GetAPIColor.getColor(main_group.color) + main_group.symbol + " " + real_name + ChatColor.YELLOW);
                            } else {
                                accepted_placeholder = ChatColor.YELLOW + injector.getString(language, "commons_friends_accept_success").replace("[0]",  GetAPIColor.getColor(main_group.color) + real_name + ChatColor.YELLOW);
                            }
                            String accepted_target;
                            if (!main_group.getName().equals("usuario")) {
                                accepted_target = ChatColor.YELLOW + injector.getString(language, "commons_friends_accept_target").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + user.getMain_group().getSymbol() + " " + sender.getName() + ChatColor.YELLOW);
                            } else {
                                accepted_target = ChatColor.YELLOW + injector.getString(language, "commons_friends_accept_target").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + sender.getName() + ChatColor.YELLOW);
                            }
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            TextComponent message = new TextComponent(accepted_placeholder);
                            message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(main_group.getColor()) + real_name)).create()));
                            message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + real_name));
                            sender.sendMessage(message);
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            if (CommonsBungee.getInstance().getProxy().getPlayer(real_name) != null) {
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                                TextComponent message_target = new TextComponent(accepted_target);
                                message_target.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + player.getName())).create()));
                                message_target.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + user.getUsername()));
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(message_target);
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            }
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_request_own"));
                        return;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "command_invalid_usage") + ": /" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_accept_command") + " " + "<" + injector.getString(language, "user_placeholder") + ">");
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_deny_command"))) {
                if (args.length == 2) {
                    if (!args[1].toLowerCase().equals(sender.getName().toLowerCase())) {
                        String[] query = RejectFriend.request(BungeeAPI.bungee_users.get(player).getToken(), args[1]);
                        if (!query[0].equals("true")) {
                            player.sendMessage(ChatColor.RED + injector.getString(language, query[1]) + ".");
                            return;
                        } else {
                            Gson gson = new Gson();
                            String real_name = JSONDeserializer.deserialize(query[1]).get("real_name").getAsString();
                            Group main_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(query[1]).get("real_group").getAsJsonObject()), Group.class);
                            String rejected_placeholder;
                            if (!main_group.getName().equals("usuario")) {
                                rejected_placeholder = ChatColor.RED + injector.getString(language, "commons_friends_reject_success").replace("[0]", GetAPIColor.getColor(main_group.color) + main_group.symbol + " " + real_name + ChatColor.RED);
                            } else {
                                rejected_placeholder = ChatColor.RED + injector.getString(language, "commons_friends_reject_success").replace("[0]", GetAPIColor.getColor(main_group.color) + real_name + ChatColor.RED);
                            }
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            sender.sendMessage(rejected_placeholder);
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_request_own"));
                        return;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "command_invalid_usage") + ": /" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_reject_command") + " " + "<" + injector.getString(language, "user_placeholder") + ">");
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_remove_command"))) {
                if (args.length == 2) {
                    if (!args[1].toLowerCase().equals(sender.getName().toLowerCase())) {
                        String[] query = RemoveFriend.request(BungeeAPI.bungee_users.get(player).getToken(), args[1]);
                        if (!query[0].equals("true")) {
                            player.sendMessage(ChatColor.RED + injector.getString(language, query[1]) + ".");
                            return;
                        } else {
                            Gson gson = new Gson();
                            String real_name = JSONDeserializer.deserialize(query[1]).get("real_name").getAsString();
                            Group main_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(query[1]).get("real_group").getAsJsonObject()), Group.class);
                            String removed_placeholder;
                            if (!main_group.getName().equals("usuario")) {
                                removed_placeholder = ChatColor.YELLOW + injector.getString(language, "commons_friends_remove_success").replace("[0]", GetAPIColor.getColor(main_group.color) + main_group.symbol + " " + real_name + ChatColor.YELLOW);
                            } else {
                                removed_placeholder = ChatColor.YELLOW + injector.getString(language, "commons_friends_remove_success").replace("[0]", GetAPIColor.getColor(main_group.color) + real_name + ChatColor.YELLOW);
                            }
                            String removed_target;
                            if (!main_group.getName().equals("usuario")) {
                                removed_target = ChatColor.RED + injector.getString(language, "commons_friends_remove_target").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) +user.getMain_group().getSymbol() + " " + sender.getName() + ChatColor.RED);
                            } else {
                                removed_target = ChatColor.RED + injector.getString(language, "commons_friends_remove_target").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + sender.getName() + ChatColor.RED);
                            }
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            TextComponent message = new TextComponent(removed_placeholder);
                            message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(main_group.getColor()) + real_name)).create()));
                            message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + real_name));
                            sender.sendMessage(message);
                            sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            if (CommonsBungee.getInstance().getProxy().getPlayer(real_name) != null) {
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                                TextComponent message_target = new TextComponent(removed_target);
                                message_target.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(user.getMain_group().getColor()) + player.getName())).create()));
                                message_target.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + user.getUsername()));
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(message_target);
                                CommonsBungee.getInstance().getProxy().getPlayer(real_name).sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                            }
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_request_own"));
                        return;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "command_invalid_usage") + ": /" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_remove_command") + " " + "<" + injector.getString(language, "user_placeholder") + ">");
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_toggle_command"))) {
                String[] query = ToggleFriends.request(BungeeAPI.bungee_users.get(sender).getToken());
                if (!query[0].equals("true")) {
                    player.sendMessage(ChatColor.RED + injector.getString(language, query[1]) + ".");
                    return;
                } else {
                    if (!query[1].equals("true")) {
                        sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                        sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_toggle_off"));
                        sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------"); } else {
                        sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                        sender.sendMessage(ChatColor.GREEN + injector.getString(language, "commons_friends_toggle_on"));
                        sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                    }
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_removeall_command"))) {
                String query = RemoveAllFriends.request(BungeeAPI.bungee_users.get(player).getToken());
                if (!query.equals("true")) {
                    player.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_error") + ".");
                    return;
                } else {
                    sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_deleted_all"));
                    sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_list_command"))) {
                String page;
                if (args.length == 2) {
                    page = args[1];
                } else {
                    page = "1";
                }
                String friends_query = FriendsList.request(BungeeAPI.bungee_users.get(player).getToken(), page, "2");
                JsonArray friends = JSONDeserializer.deserialize(friends_query).get("friends").getAsJsonArray();
                Integer total_pages = JSONDeserializer.deserialize(friends_query).get("total_pages").getAsInt();
                Integer actual_page = JSONDeserializer.deserialize(friends_query).get("actual_page").getAsInt();
                List<Friend> friendList = new ArrayList<>();
                for (JsonElement friend_element : friends) {
                    Gson gson = new Gson();
                    JsonObject friend_object = friend_element.getAsJsonObject();
                    String friend_username = friend_object.get("username").getAsString();
                    Integer friend_level = friend_object.get("level").getAsInt();
                    String friend_main_group_string = JSONDeserializer.jsonString(friend_object.get("real_group").getAsJsonObject());
                    Group friend_main_group = gson.fromJson(friend_main_group_string, Group.class);
                    String friend_last_online = friend_object.get("last_online").getAsString();
                    Boolean friend_disguised = friend_object.get("disguised").getAsBoolean();
                    String friend_skin = friend_object.get("skin").getAsString();
                    String friend_game = friend_object.get("last_game").getAsString().toLowerCase();
                    @Nullable String disguised_actual = null;
                    @Nullable Group disguised_group = null;
                    if (friend_disguised) {
                        disguised_actual = friend_object.get("disguised_actual").getAsString();
                        String disguised_group_string = JSONDeserializer.jsonString(friend_object.get("disguise_group").getAsJsonObject());
                        disguised_group = gson.fromJson(disguised_group_string, Group.class);
                    }
                    Friend friend  = new Friend(friend_username, friend_level, friend_main_group, friend_disguised, friend_skin, friend_last_online, friend_game, disguised_actual, disguised_group);
                    friendList.add(friend);
                }
                List<Friend> sorted_array = UserSort.friend_sort(friendList, "1","false");
                Friend[] friendArray = new Friend[sorted_array.size()];
                friendArray = sorted_array.toArray(friendArray);

                if (actual_page > total_pages) {
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_no_friends"));
                } else {
                    sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                    sender.sendMessage(ChatColor.GOLD + FontEnumeration.sendCenteredMessage(injector.getString(language, "commons_friends_pagination").replace("[0]", actual_page.toString()).replace("[1]", total_pages.toString())));

                    for(int i = 8; i < 8 + friendList.size(); i++) {
                        Friend friend = friendArray[i - 8];
                        TextComponent name_placeholder = new TextComponent(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername());
                        TextComponent online_at = new TextComponent("" + ChatColor.YELLOW + FriendListChatProcessor.processor(friend.getLast_game(), friend.getLast_online(), language).replace("%red%", "" + ChatColor.RED).replace("%yellow%", "" + ChatColor.YELLOW));
                        name_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_view_profile").replace("[0]", GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername())).create()));
                        name_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/perfil " + friend.getUsername()));
                        sender.sendMessage(name_placeholder, online_at);
                    }
                    sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                }

            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_requests_command"))) {
                String page;
                if (args.length == 2) {
                    page = args[1];
                } else {
                    page = "1";
                }
                String friends_query = RequestsList.request(BungeeAPI.bungee_users.get(player).getToken(), page, "2");
                JsonArray friends = JSONDeserializer.deserialize(friends_query).get("requests").getAsJsonArray();
                Integer total_pages = JSONDeserializer.deserialize(friends_query).get("total_pages").getAsInt();
                Integer actual_page = JSONDeserializer.deserialize(friends_query).get("actual_page").getAsInt();
                List<Friend> friendList = new ArrayList<>();
                for (JsonElement friend_element : friends) {
                    Gson gson = new Gson();
                    JsonObject friend_object = friend_element.getAsJsonObject();
                    String friend_username = friend_object.get("username").getAsString();
                    Integer friend_level = friend_object.get("level").getAsInt();
                    String friend_main_group_string = JSONDeserializer.jsonString(friend_object.get("real_group").getAsJsonObject());
                    Group friend_main_group = gson.fromJson(friend_main_group_string, Group.class);
                    String friend_last_online = friend_object.get("last_online").getAsString();
                    Boolean friend_disguised = friend_object.get("disguised").getAsBoolean();
                    String friend_skin = friend_object.get("skin").getAsString();
                    String friend_game = friend_object.get("last_game").getAsString().toLowerCase();
                    @Nullable String disguised_actual = null;
                    @Nullable Group disguised_group = null;
                    if (friend_disguised) {
                        disguised_actual = friend_object.get("disguised_actual").getAsString();
                        String disguised_group_string = JSONDeserializer.jsonString(friend_object.get("disguise_group").getAsJsonObject());
                        disguised_group = gson.fromJson(disguised_group_string, Group.class);
                    }
                    Friend friend  = new Friend(friend_username, friend_level, friend_main_group, friend_disguised, friend_skin, friend_last_online, friend_game, disguised_actual, disguised_group);
                    friendList.add(friend);
                }
                List<Friend> sorted_array = UserSort.friend_sort(friendList, "1","false");
                Friend[] friendArray = new Friend[sorted_array.size()];
                friendArray = sorted_array.toArray(friendArray);

                if (actual_page > total_pages) {
                    sender.sendMessage(ChatColor.RED + injector.getString(language, "commons_friends_no_friends"));
                } else {
                    sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                    sender.sendMessage(ChatColor.GOLD + FontEnumeration.sendCenteredMessage(injector.getString(language, "commons_requests_pagination").replace("[0]", actual_page.toString()).replace("[1]", total_pages.toString())));

                    for(int i = 8; i < 8 + friendList.size(); i++) {
                        Friend friend = friendArray[i - 8];
                        TextComponent name_placeholder = new TextComponent(GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername() + "" + ChatColor.YELLOW + FriendListChatProcessor.processor(friend.getLast_game(), friend.getLast_online(), language).replace("%red%", "" + ChatColor.RED).replace("%yellow%", "" + ChatColor.YELLOW));
                        name_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_friends_accept_hover").replace("[0]", GetAPIColor.getColor(friend.getGroup().getColor()) + friend.getUsername())).create()));
                        name_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends " + injector.getString(language, "commons_friends_accept_command") + " " + friend.getUsername()));
                        sender.sendMessage(name_placeholder);
                    }
                    sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");

                    if (RedisConnection.hasString(sender.getName() + ".new_friendship_requests")) {
                        RedisConnection.deleteString(sender.getName() + ".new_friendship_requests");
                    }
                }
            } else if (args[0].equalsIgnoreCase(injector.getString(language, "commons_friends_help_command"))) {
                sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
                sender.sendMessage(ChatColor.GOLD + injector.getString(language, "commons_friends_help_title"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_help_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_help_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_add_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_add_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_accept_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_accept_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_deny_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_deny_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_list_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_list_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_remove_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_remove_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_requests_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_requests_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_toggle_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_toggle_about"));
                sender.sendMessage(ChatColor.YELLOW + "/" + injector.getString(language, "commons_friends_command") + " " + injector.getString(language, "commons_friends_removeall_command") + ChatColor.GRAY + " - " + ChatColor.AQUA + injector.getString(language, "commons_friends_removeall_about"));
                sender.sendMessage(ChatColor.AQUA + "-----------------------------------------------------");
            }
        }
    }
}