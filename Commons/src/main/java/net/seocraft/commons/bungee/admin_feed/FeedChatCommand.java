package net.seocraft.commons.bungee.admin_feed;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.api.shared.utils.BukkitMessaging;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.api.shared.i18n.TranslatableComponent;

public class FeedChatCommand extends Command {

    public FeedChatCommand(){
        super("feedchat", "", "fc", "ac", "a");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            TranslatableComponent injector = new TranslatableComponent();
            String language = BungeeAPI.bungee_users.get(player).getLanguage();

            if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.read")) {
                BukkitMessaging.sendSound(player, "bass");
                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "no_permission")));
            } else {
                if(!RedisConnection.hasString(player.getName() + ".feed_activated")) {
                    BukkitMessaging.sendSound(player, "bass");
                    TextComponent feed_disabled = new TextComponent(ChatColor.AQUA + "" + ChatColor.BOLD + injector.getString(language, "info_placeholder") + ": " + ChatColor.GRAY + injector.getString(language, "commons_admin_feed_disabled_join") + "... ");
                    feed_disabled.setColor(ChatColor.GRAY);
                    TextComponent enable_click = new TextComponent(injector.getString(language, "commons_admin_feed_disabled_activate"));
                    enable_click.setColor(ChatColor.YELLOW);
                    enable_click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_admin_feed_disabled_hover").replace("%red%", ChatColor.RED + "").replace("%yellow%", ChatColor.YELLOW + "")).create()));
                    enable_click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/feed estado"));
                    player.sendMessage(feed_disabled, enable_click);
                } else {
                    if (args[0].equalsIgnoreCase("-i")) {
                        if (args.length > 1) {
                            if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.important")) {
                                BukkitMessaging.sendSound(player, "bass");
                                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "no_permission")));
                            } else {
                                TextComponent sender_placeholder = FeedUtils.getUserPlaceholder(language, player);
                                String message = "";
                                for (int i = 1; i < args.length; i++) {
                                    if (i != args.length - 1) {
                                        message += args[i] + " ";
                                    } else {
                                        message += args[i];
                                    }
                                }
                                TextComponent message_fixed = new TextComponent(": " + message);
                                message_fixed.setColor(ChatColor.WHITE);

                                for(ProxiedPlayer online_team : CommonsBungee.getInstance().getProxy().getPlayers()) {
                                    if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(online_team)).contains("commons.staff.feed.read")) {
                                        String user_language = BungeeAPI.bungee_users.get(online_team).getLanguage();
                                        TextComponent prefix = FeedUtils.getFeedImportant(user_language);
                                        BukkitMessaging.sendSound(player, "harp");
                                        online_team.sendMessage(prefix, sender_placeholder, message_fixed);
                                    }
                                }
                            }
                        } else {
                            BukkitMessaging.sendSound(player, "bass");
                            player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "correct_usage") + ": " + "/a [-i] " + "<" + injector.getString(language, "message_placeholder") + ">"));
                        }
                    } else {
                        TextComponent sender_placeholder = FeedUtils.getUserPlaceholder(language, player);
                        String message = "";
                        for (int i = 0; i < args.length; i++) {
                            if (i != args.length - 1) {
                                message += args[i] + " ";
                            } else {
                                message += args[i];
                            }
                        }
                        TextComponent message_fixed = new TextComponent(": " + message);
                        message_fixed.setColor(ChatColor.WHITE);
                        for(ProxiedPlayer online_team : CommonsBungee.getInstance().getProxy().getPlayers()) {
                            if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(online_team)).contains("commons.staff.feed.read") && RedisConnection.hasString(online_team.getName() + ".feed_activated")) {
                                String user_language = BungeeAPI.bungee_users.get(online_team).getLanguage();
                                TextComponent prefix = FeedUtils.getFeedPrefix(user_language);
                                online_team.sendMessage(prefix, sender_placeholder, message_fixed);
                            }
                        }
                    }
                }
            }
        }
    }
}