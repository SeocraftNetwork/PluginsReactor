package net.seocraft.commons.bungee;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.seocraft.commons.bungee.admin_feed.FeedChatCommand;
import net.seocraft.commons.bungee.admin_feed.FeedStatusCommand;
import net.seocraft.commons.bungee.admin_feed.LoadFeedListener;
import net.seocraft.commons.bungee.commands.BanCommand;
import net.seocraft.commons.bungee.commands.FriendsCommand;
import net.seocraft.commons.bungee.commands.KickCommand;
import net.seocraft.commons.bungee.commands.WarnCommand;
import net.seocraft.commons.bungee.listeners.JoinListener;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommonsBungee extends Plugin {

    private static CommonsBungee instance;
    public static HashMap<ProxiedPlayer, String[]> player_permissions = new HashMap<>();
    public static List<ProxiedPlayer> logged_staff = new ArrayList<>();

    public static CommonsBungee getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        getProxy().getPluginManager().registerCommand(this, new FriendsCommand());
        getProxy().getPluginManager().registerCommand(this, new WarnCommand());
        getProxy().getPluginManager().registerCommand(this, new KickCommand());
        getProxy().getPluginManager().registerCommand(this, new BanCommand());
        getProxy().getPluginManager().registerCommand(this, new FeedChatCommand());
        getProxy().getPluginManager().registerCommand(this, new FeedStatusCommand());
        getProxy().getPluginManager().registerListener(this, new LoadFeedListener());
        getProxy().getPluginManager().registerListener(this, new JoinListener());
    }
}