package net.seocraft.commons.bungee.admin_feed;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.api.shared.utils.BukkitMessaging;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.api.shared.i18n.TranslatableComponent;

public class FeedStatusCommand extends Command {

    public FeedStatusCommand(){
        super("feed", "", "fs");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            TranslatableComponent injector = new TranslatableComponent();
            String language = BungeeAPI.bungee_users.get(player).getLanguage();

            if(!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.disable") && ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.read")) {
                BukkitMessaging.sendSound(player, "bass");
                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "no_permission")));
            } else {
                if(args.length > 0 && args[0].equals("estado")) {
                    if (!RedisConnection.hasString(player.getName() + ".feed_activated")) {
                        RedisConnection.addString(player.getName() + ".feed_activated", "true");
                        BukkitMessaging.sendSound(player, "pling");
                        TextComponent message = new TextComponent(injector.getString(language, "commons_admin_feed_enabled"));
                        message.setColor(ChatColor.GREEN);
                        player.sendMessage(message);
                    } else {
                        RedisConnection.deleteString(player.getName() + ".feed_activated");
                        BukkitMessaging.sendSound(player, "bass");
                        TextComponent message = new TextComponent(injector.getString(language, "commons_admin_feed_disabled"));
                        message.setColor(ChatColor.RED);
                        player.sendMessage(message);
                    }
                } else {
                    BukkitMessaging.sendSound(player, "bass");
                    player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "correct_usage") + ": " + "/feed [estado]"));
                }
            }
        }
    }
}