package net.seocraft.commons.bungee.admin_feed;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import net.seocraft.api.shared.processor.GameOnlineProcessor;


public class FeedUtils {
    public static TextComponent getFeedPrefix(String language) {
        TranslatableComponent injector = new TranslatableComponent();
        TextComponent staff_prefix = new TextComponent(ChatColor.AQUA + "[STAFF] ");
        staff_prefix.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_admin_feed_status_hover").replace("%red%", ChatColor.RED + "").replace("%yellow%", ChatColor.YELLOW + "")).create()));
        staff_prefix.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/feed estado"));
        return staff_prefix;
    }

    public static TextComponent getUserPlaceholder(String language, ProxiedPlayer player) {
        TranslatableComponent injector = new TranslatableComponent();
        Group group = BungeeAPI.bungee_users.get(player).getMain_group();
        TextComponent user_placeholder;
        if (!group.getName().equalsIgnoreCase("usuario")) {
            user_placeholder = new TextComponent(group.getSymbol() + " " + player.getName());
            user_placeholder.setColor(ChatColor.valueOf(group.getColor().toUpperCase()));
        } else {
            user_placeholder = new TextComponent(player.getName());
            user_placeholder.setColor(ChatColor.valueOf(group.getColor().toUpperCase()));
        }
        user_placeholder.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder (ChatColor.YELLOW + injector.getString(language, "commons_admin_feed_visit_server").replace("[0]", ChatColor.valueOf(group.getColor().toUpperCase()) + player.getName() + ChatColor.YELLOW)).create()));
        user_placeholder.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/visit " + player.getName().toLowerCase()));
        return user_placeholder;
    }

    public static TextComponent getFeedImportant(String language) {
        TranslatableComponent injector = new TranslatableComponent();
        TextComponent important_prefix = new TextComponent(ChatColor.DARK_RED + "[STAFF] ");
        important_prefix.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_admin_feed_important_hover").replace("%red%", ChatColor.RED + "").replace("%yellow%", ChatColor.YELLOW + "")).create()));
        important_prefix.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/a -i "));
        return important_prefix;
    }

    public static void punishmentAlert(String type, ProxiedPlayer punisher, ProxiedPlayer punished, String reason, String gamemode) {
        TranslatableComponent injector = new TranslatableComponent();
        TextComponent gamemode_message = new TextComponent("(" + GameOnlineProcessor.processor(gamemode) + ") ");
        gamemode_message.setColor(ChatColor.GRAY);
        for (ProxiedPlayer online_team : CommonsBungee.getInstance().getProxy().getPlayers()) {
            if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(online_team)).contains("commons.staff.feed.read") && RedisConnection.hasString(online_team + ".feed_activated") && !online_team.getName().equalsIgnoreCase(punisher.getName()) || ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(punisher)).contains("commons.staff.feed.important") && ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(online_team)).contains("commons.staff.feed.read") && !online_team.getName().equalsIgnoreCase(punisher.getName())) {
                TextComponent staff_placeholder = getFeedPrefix(BungeeAPI.bungee_users.get(online_team).getLanguage());
                TextComponent punisher_placeholder = getUserPlaceholder(BungeeAPI.bungee_users.get(online_team).getLanguage(), punisher);
                TextComponent punished_placeholder = getUserPlaceholder(BungeeAPI.bungee_users.get(online_team).getLanguage(), punished);
                TextComponent separator = new TextComponent(" \u00bb ");
                separator.setColor(ChatColor.GRAY);
                TextComponent type_placeholder = null;
                switch (type) {
                    case "warn": {
                        String warn = injector.getString(BungeeAPI.bungee_users.get(online_team).getLanguage(), "commons_admin_punishments_warn");
                        type_placeholder = new TextComponent(Character.toUpperCase(warn.charAt(0)) + warn.substring(1));
                        type_placeholder.setColor(ChatColor.GREEN);
                        break;
                    }
                    case "kick": {
                        String kick = injector.getString(BungeeAPI.bungee_users.get(online_team).getLanguage(), "commons_admin_punishments_kick");
                        type_placeholder = new TextComponent(Character.toUpperCase(kick.charAt(0)) + kick.substring(1));
                        type_placeholder.setColor(ChatColor.YELLOW);
                        break;
                    }
                    case "temp-ban": {
                        String ban = injector.getString(BungeeAPI.bungee_users.get(online_team).getLanguage(), "commons_admin_punishments_temporal_ban");
                        type_placeholder = new TextComponent(Character.toUpperCase(ban.charAt(0)) + ban.substring(1));
                        type_placeholder.setColor(ChatColor.RED);
                        break;
                    }
                    case "ban": {
                        String ban = injector.getString(BungeeAPI.bungee_users.get(online_team).getLanguage(), "commons_admin_punishments_permanent_ban");
                        type_placeholder = new TextComponent(Character.toUpperCase(ban.charAt(0)) + ban.substring(1));
                        type_placeholder.setColor(ChatColor.RED);
                        break;
                    }
                    default: {
                        type_placeholder = new TextComponent("Unknown Punishment");
                        break;
                    }
                }
                TextComponent reason_placeholder = new TextComponent(Character.toUpperCase(reason.charAt(0)) + reason.substring(1));
                reason_placeholder.setColor(ChatColor.WHITE);
                online_team.sendMessage(staff_placeholder, gamemode_message, punisher_placeholder, separator, type_placeholder, separator, punished_placeholder, separator, reason_placeholder);
            }
        }

    }
}