package net.seocraft.commons.bungee.commands;

import com.google.gson.Gson;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.requests.punishments.TempPunishmentRequest;
import net.seocraft.api.shared.models.Group;
import net.seocraft.api.shared.commons.JSONDeserializer;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.api.shared.utils.BukkitMessaging;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.commons.bungee.admin_feed.FeedUtils;
import net.seocraft.api.shared.i18n.TranslatableComponent;
import net.seocraft.api.shared.processor.TimeUnit;

public class TempBanCommand extends Command {

    public TempBanCommand(){
        super("tempban", "", "tb", "temporalban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        TranslatableComponent injector = new TranslatableComponent();
        String language = BungeeAPI.bungee_users.get(player).getLanguage();
        if (sender instanceof ProxiedPlayer) {
            if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.ban")) {
                BukkitMessaging.sendSound(player, "bass");
                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "no_permission")));
            } else {
                if (args.length >= 3) {
                    if (CommonsBungee.getInstance().getProxy().getPlayer(args[0]) == null) {
                        player.sendMessage(ChatColor.RED + injector.getString(language, "commons_admin_punishments_not_connected") + " " + ChatColor.YELLOW + "https://www.seocraft.net" + ChatColor.RED + ".");
                    } else if (args[0].toLowerCase().equals(player.getName().toLowerCase())) {
                        player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishments_own") + "."));
                    } else {
                        ProxiedPlayer punished = CommonsBungee.getInstance().getProxy().getPlayer(args[0]);
                        if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(punished)).contains("commons.staff.basics")) {
                            String reason = "";
                            for (int i = 2; i < args.length; i++) {
                                if (i != args.length - 2) {
                                    reason += args[i] + " ";
                                } else {
                                    reason += args[i];
                                }
                            }
                            int expire_date = 0;
                            try {
                                expire_date = Integer.parseInt(args[2].split(":")[0]);
                            } catch(NumberFormatException e){
                                BukkitMessaging.sendSound(player, "bass");
                                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishment_temporal_wrong")));
                                return;
                            }
                            if (!TimeUnit.existFromShortcut(args[2].split(":")[1])) {
                                BukkitMessaging.sendSound(player, "bass");
                                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishment_temporal_wrong")));
                                return;
                            }
                            TimeUnit unit = TimeUnit.getFromShortcut(args[1].split(":")[1]);
                            long banTime = unit.getToSecond() * expire_date;
                            String punishment = TempPunishmentRequest.request(sender.getName(), args[0], "temp-ban", reason, player.getServer().getInfo().getName(), player.getAddress().toString().split(":")[0].replace("/", ""), "");
                            if (!JSONDeserializer.deserialize(punishment).get("query_success").getAsString().equals("true")) {
                                player.sendMessage(new TextComponent(ChatColor.RED + injector.getString( language, JSONDeserializer.errorMessage(punishment))));
                            } else {
                                String punished_language = BungeeAPI.bungee_users.get(punished).getLanguage();
                                Gson gson = new Gson();
                                Group punisher_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(punishment).get("query_user").getAsJsonObject().get("real_group").getAsJsonObject()), Group.class);
                                Group punished_group = gson.fromJson(JSONDeserializer.jsonString(JSONDeserializer.deserialize(punishment).get("query_target").getAsJsonObject().get("real_group").getAsJsonObject()), Group.class);
                                punished.disconnect(new TextComponent(ChatColor.RED + injector.getString(punished_language, "commons_admin_punishments_temporal_ban") + " - " + ChatColor.valueOf(punisher_group.getColor().toUpperCase()) + punisher_group.getSymbol() + " " + player.getName()
                                        + "\n\n" + ChatColor.AQUA + "\u00BB " + reason + "(" + "elvetadelafecha" + ")"+ "\n\n" +
                                        ChatColor.YELLOW + injector.getString(punished_language, "commons_admin_punishment_disconnect_mistake") + ChatColor.GOLD + " seocraft.net/apelar"
                                ));
                                BukkitMessaging.sendTempBanSuccess(punisher_group.name, player, punished.getName(), punished_group.getColor(), punished_group.getSymbol(), language, reason);
                                FeedUtils.punishmentAlert("temp-ban", player, punished, reason, punished.getServer().getInfo().getName().split("-")[0]);
                            }
                        } else {
                            player.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "commons_admin_punishments_bypass") + "."));
                        }
                    }
                } else {
                    sender.sendMessage(new TextComponent(ChatColor.RED + injector.getString(language, "correct_usage") + ": " + "/tempban <" + injector.getString(language, "user_placeholder") + "> <" + injector.getString(language, "delay_placeholder") + "> <" + injector.getString(language, "reason_placeholder") + ">"));
                }
            }
        }
    }
}