package net.seocraft.commons.bungee.admin_feed;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.seocraft.api.bungee.BungeeAPI;
import net.seocraft.api.bungee.utils.RedisConnection;
import net.seocraft.api.shared.utils.ChatProcessor;
import net.seocraft.api.shared.utils.BukkitMessaging;
import net.seocraft.commons.bungee.CommonsBungee;
import net.seocraft.api.shared.i18n.TranslatableComponent;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class LoadFeedListener implements Listener {

    @EventHandler
    public void loadFeed(PluginMessageEvent event) {
        if (event.getTag().equalsIgnoreCase("BungeeCord")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(event.getData()));
            try {
                String channel = in.readUTF();
                if(channel.equals("InternalAPI")){
                    ProxiedPlayer player = BungeeAPI.getInstance().getProxy().getPlayer(event.getReceiver().toString());
                    String input = in.readUTF();
                    if (input.equals("StaffRequest")) {
                        TranslatableComponent injector = new TranslatableComponent();
                        String language = BungeeAPI.bungee_users.get(player).getLanguage();
                        if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.read")) {
                            if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.disable")) {
                                if (!RedisConnection.hasString(player.getName() + ".feed_activated")) {
                                    RedisConnection.addString(player.getName() + ".feed_activated", "true");
                                }
                            } else {
                                if (!RedisConnection.hasString(player.getName() + ".feed_activated")) {
                                    BukkitMessaging.sendSound(player, "bass");
                                    TextComponent feed_disabled = new TextComponent(ChatColor.AQUA + "" + ChatColor.BOLD + injector.getString(language, "info_placeholder") + ": " + ChatColor.GRAY + injector.getString(language, "commons_admin_feed_disabled_join") + "... ");
                                    feed_disabled.setColor(ChatColor.GRAY);
                                    TextComponent enable_click = new TextComponent(injector.getString(language, "commons_admin_feed_disabled_activate"));
                                    enable_click.setColor(ChatColor.YELLOW);
                                    enable_click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.YELLOW + injector.getString(language, "commons_admin_feed_disabled_hover").replace("%red%", ChatColor.RED + "").replace("%yellow%", ChatColor.YELLOW + "")).create()));
                                    enable_click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/feed estado"));
                                    player.sendMessage(feed_disabled, enable_click);
                                }
                            }
                            if(!CommonsBungee.logged_staff.contains(player)) {
                                CommonsBungee.logged_staff.add(player);
                                for (ProxiedPlayer online : CommonsBungee.getInstance().getProxy().getPlayers()) {
                                    if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(online)).contains("commons.staff.feed.read") && RedisConnection.hasString(online + ".feed_activated") && !online.getName().equalsIgnoreCase(player.getName()) || ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.important") && ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(online)).contains("commons.staff.feed.read") && !online.getName().equalsIgnoreCase(player.getName())) {
                                        TextComponent staff_placeholder = FeedUtils.getFeedPrefix(BungeeAPI.bungee_users.get(online).getLanguage());
                                        TextComponent player_placeholder = FeedUtils.getUserPlaceholder(BungeeAPI.bungee_users.get(online).getLanguage(), player);
                                        TextComponent message = new TextComponent(ChatColor.YELLOW + " " + injector.getString(BungeeAPI.bungee_users.get(online).getLanguage(), "commons_friends_joined").toLowerCase() + ".");
                                        online.sendMessage(staff_placeholder, player_placeholder, message);

                                    }
                                }
                            }
                        }
                    }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @EventHandler
    public void unloadFeed(PlayerDisconnectEvent event) {
        if (CommonsBungee.logged_staff.contains(event.getPlayer())) {
            CommonsBungee.logged_staff.remove(event.getPlayer());
            if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(event.getPlayer())).contains("commons.staff.feed.read")) {
                TranslatableComponent injector = new TranslatableComponent();
                if (!ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(event.getPlayer())).contains("commons.staff.feed.disable")) {
                    if (RedisConnection.hasString(event.getPlayer().getName() + ".feed_activated")) {
                        RedisConnection.deleteString(event.getPlayer().getName() + ".feed_activated");
                    }
                }
                for (ProxiedPlayer player : CommonsBungee.getInstance().getProxy().getPlayers()) {
                    if (ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.read") && RedisConnection.hasString(player + ".feed_activated") && !player.getName().equalsIgnoreCase(event.getPlayer().getName()) || ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(event.getPlayer())).contains("commons.staff.feed.important") && ChatProcessor.convertStringArray(CommonsBungee.player_permissions.get(player)).contains("commons.staff.feed.read") && !player.getName().equalsIgnoreCase(event.getPlayer().getName())) {
                        TextComponent staff_placeholder = FeedUtils.getFeedPrefix(BungeeAPI.bungee_users.get(player).getLanguage());
                        TextComponent player_placeholder = FeedUtils.getUserPlaceholder(BungeeAPI.bungee_users.get(player).getLanguage(), event.getPlayer());
                        TextComponent message = new TextComponent(ChatColor.YELLOW + " " + injector.getString(BungeeAPI.bungee_users.get(player).getLanguage(), "commons_friends_left").toLowerCase() + ".");
                        player.sendMessage(staff_placeholder, player_placeholder, message);
                    }
                }
            }
        }
    }
}